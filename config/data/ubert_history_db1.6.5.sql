/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : ubert_history_db

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-07-29 19:59:15
Author: ChengJili
版本说明：
@20150729：原始版本定稿
*/
CREATE DATABASE IF NOT EXISTS ubert_history_db DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
USE ubert_history_db;
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for history_clearing_currencyrates
-- ----------------------------
DROP TABLE IF EXISTS `history_clearing_currencyrates`;
CREATE TABLE `history_clearing_currencyrates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ClearingDate` date NOT NULL,
  `AskCurrency` char(3) NOT NULL,
  `BidCurrency` char(3) NOT NULL,
  `RatePrice` float DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ClearingDate`,`AskCurrency`,`BidCurrency`) USING BTREE,
  KEY `index1` (`AskCurrency`,`BidCurrency`),
  KEY `index2` (`BidCurrency`,`AskCurrency`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for history_iopv_account
-- ----------------------------
DROP TABLE IF EXISTS `history_iopv_account`;
CREATE TABLE `history_iopv_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Date` date NOT NULL,
  `ProductID` int(11) NOT NULL,
  `UnitID` int(11) NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `Net` float DEFAULT '0',
  `IOPV` float DEFAULT '0',
  `RealIOPV` float DEFAULT '0',
  `CurrentLot` bigint(11) DEFAULT '0',
  `DeltLot` int(11) DEFAULT '0',
  `FeeOfDay` float DEFAULT '0',
  `ClearFeeaAcumulation` float DEFAULT '0',
  `TotalAssetValue` float DEFAULT '0',
  `PosRate` float DEFAULT '0',
  `DebtAssetValue` float DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`Date`,`ProductID`,`UnitID`,`AccountCode`) USING BTREE,
  KEY `index1` (`AccountCode`),
  KEY `index2` (`UnitID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for history_iopv_product
-- ----------------------------
DROP TABLE IF EXISTS `history_iopv_product`;
CREATE TABLE `history_iopv_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Date` date NOT NULL,
  `ProductID` int(11) NOT NULL,
  `Net` float DEFAULT '0',
  `IOPV` float DEFAULT '0',
  `RealIOPV` float DEFAULT '0',
  `CurrentLot` bigint(11) DEFAULT '0',
  `DeltLot` int(11) DEFAULT '0',
  `FeeOfDay` float DEFAULT '0',
  `ClearFeeaAcumulation` float DEFAULT '0',
  `TotalAssetValue` float DEFAULT '0',
  `PosRate` float DEFAULT '0',
  `DebtAssetValue` float DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`Date`,`ProductID`) USING BTREE,
  KEY `index1` (`ProductID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for history_iopv_productunit
-- ----------------------------
DROP TABLE IF EXISTS `history_iopv_productunit`;
CREATE TABLE `history_iopv_productunit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Date` date NOT NULL,
  `ProductID` int(11) NOT NULL,
  `UnitID` int(11) NOT NULL,
  `Net` float DEFAULT '0',
  `IOPV` float DEFAULT '0',
  `RealIOPV` float DEFAULT '0',
  `CurrentLot` bigint(11) DEFAULT '0',
  `DeltLot` int(11) DEFAULT '0',
  `FeeOfDay` float DEFAULT '0',
  `ClearFeeaAcumulation` float DEFAULT '0',
  `TotalAssetValue` float DEFAULT '0',
  `PosRate` float DEFAULT '0',
  `DebtAssetValue` float DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`Date`,`ProductID`,`UnitID`) USING BTREE,
  KEY `index1` (`Date`),
  KEY `index2` (`UnitID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for history_log_logininfo
-- ----------------------------
DROP TABLE IF EXISTS `history_log_logininfo`;
CREATE TABLE `history_log_logininfo` (
  `id` int(11) NOT NULL,
  `UserID` varchar(12) NOT NULL,
  `LoginType` char(3) NOT NULL,
  `LogoutType` char(3) DEFAULT NULL,
  `LoginDateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `LogoutDateTime` datetime DEFAULT NULL,
  `Mac` varchar(17) NOT NULL,
  `IP4` int(11) NOT NULL,
  `IP6` varbinary(16) DEFAULT NULL,
  `Address` varchar(40) NOT NULL,
  `ClientType` char(3) DEFAULT NULL,
  `OSType` char(3) DEFAULT NULL,
  `ClientVersion` char(3) DEFAULT NULL,
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`UserID`,`LoginDateTime`,`IP4`) USING BTREE,
  KEY `index1` (`LoginDateTime`),
  KEY `index2` (`IP4`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for history_obj_exrightinfo
-- ----------------------------
DROP TABLE IF EXISTS `history_obj_exrightinfo`;
CREATE TABLE `history_obj_exrightinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `RegionID` char(3) NOT NULL,
  `Obj` varchar(20) NOT NULL,
  `MarketCode` char(3) NOT NULL,
  `ClassCode` char(3) NOT NULL,
  `ExRightDate` date NOT NULL,
  `RecordeDate` date NOT NULL,
  `CheckInRightDate` date NOT NULL,
  `CheckInProfitsDate` date NOT NULL,
  `Denominator` int(11) DEFAULT '0',
  `MemberA` int(11) DEFAULT '0',
  `MemberB` int(11) DEFAULT '0',
  `Profits` float DEFAULT '0',
  `ReMark` varchar(60) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`Obj`,`MarketCode`,`ClassCode`,`ExRightDate`) USING BTREE,
  KEY `index1` (`ExRightDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for history_tradeset_calendar
-- ----------------------------
DROP TABLE IF EXISTS `history_tradeset_calendar`;
CREATE TABLE `history_tradeset_calendar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `RegionID` char(3) NOT NULL,
  `Date` date NOT NULL,
  `IsTrade` char(3) NOT NULL,
  `ReMark` varchar(60) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`RegionID`,`Date`) USING BTREE,
  KEY `index1` (`IsTrade`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trade_fundtransfer
-- ----------------------------
DROP TABLE IF EXISTS `history_trade_fundtransfer`;
CREATE TABLE `history_trade_fundtransfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `RegionID` char(3) DEFAULT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `Direct` char(3) NOT NULL,
  `Amount` double(10,6) NOT NULL,
  `Date` date NOT NULL,
  `Time` time DEFAULT NULL,
  `DateTime` datetime DEFAULT NULL,
  `Currency` char(3) DEFAULT NULL,
  `Checker` varchar(36) DEFAULT NULL,
  `ReMark` varchar(256) DEFAULT NULL,
  `IsEffectived` char(3) NOT NULL,
  `ReplyMsg` varchar(256) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `index1` (`AccountCode`),
  KEY `index2` (`DateTime`),
  KEY `index3` (`Direct`),
  KEY `index4` (`IsEffectived`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
