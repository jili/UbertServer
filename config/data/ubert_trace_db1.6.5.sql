/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : ubert_trace_db

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-07-29 19:59:15
Author: ChengJili
版本说明：
@20150729：原始版本定稿
*/
CREATE DATABASE IF NOT EXISTS ubert_trace_db DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
USE ubert_trace_db;
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for trace_account_bail
-- ----------------------------
DROP TABLE IF EXISTS `trace_account_bail`;
CREATE TABLE `trace_account_bail` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `id` int(11) NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `TempID` int(11) DEFAULT '0',
  `MarketCode` char(3) DEFAULT 'all',
  `ClassCode` char(3) DEFAULT 'all',
  `BS` char(3) DEFAULT 'all',
  `UnderlyingCode` varchar(20) DEFAULT 'all',
  `Obj` varchar(20) DEFAULT 'all',
  `Bail0` float DEFAULT '0',
  `Bail1` float DEFAULT '0',
  `Bail2` float DEFAULT '0',
  `Bail0Rate` float DEFAULT '0',
  `Bail1Rate` float DEFAULT '0',
  `Bail2Rate` float DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`TempID`),
  KEY `index2` (`AccountCode`),
  KEY `index3` (`TraceType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_account_fare
-- ----------------------------
DROP TABLE IF EXISTS `trace_account_fare`;
CREATE TABLE `trace_account_fare` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `id` int(11) NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `TempID` int(11) DEFAULT '0',
  `MarketCode` char(3) DEFAULT 'all',
  `ClassCode` char(3) DEFAULT 'all',
  `UnderlyingCode` varchar(20) DEFAULT 'all',
  `Obj` varchar(20) DEFAULT 'all',
  `BS` char(3) DEFAULT 'all',
  `OpenClose` char(3) DEFAULT 'all',
  `Commission0` float DEFAULT '0',
  `Commission1` float DEFAULT '0',
  `MinCommission0` float DEFAULT '0',
  `MinCommission1` float DEFAULT '0',
  `DeliverFare0` float DEFAULT '0',
  `DeliverFare1` float DEFAULT '0',
  `Tax0` float DEFAULT '0',
  `Tax1` float DEFAULT '0',
  `Etax1` float DEFAULT '0',
  `Etax0` float DEFAULT '0',
  `Cost0` float DEFAULT '0',
  `Cost1` float DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`TempID`),
  KEY `index2` (`AccountCode`),
  KEY `index3` (`TraceType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_account_icode
-- ----------------------------
DROP TABLE IF EXISTS `trace_account_icode`;
CREATE TABLE `trace_account_icode` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `id` int(11) NOT NULL,
  `iid` int(11) NOT NULL,
  `InvestID` varchar(20) NOT NULL,
  `Type` char(3) NOT NULL,
  `Code` varchar(20) NOT NULL,
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`InvestID`),
  KEY `index2` (`TraceType`),
  KEY `index3` (`Type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_account_info
-- ----------------------------
DROP TABLE IF EXISTS `trace_account_info`;
CREATE TABLE `trace_account_info` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `id` int(11) NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `AccountName` varchar(30) DEFAULT NULL,
  `PipID` int(11) NOT NULL,
  `Pwd` varchar(48) DEFAULT NULL,
  `IsRisk` char(3) DEFAULT '0',
  `iid` int(11) NOT NULL,
  `Type` char(3) DEFAULT NULL,
  `InvestID` varchar(12) NOT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`PipID`),
  KEY `index2` (`IsRisk`),
  KEY `index3` (`iid`),
  KEY `index4` (`TraceType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_account_invest
-- ----------------------------
DROP TABLE IF EXISTS `trace_account_invest`;
CREATE TABLE `trace_account_invest` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `id` int(11) NOT NULL,
  `InvestID` varchar(12) NOT NULL,
  `RegionID` char(3) DEFAULT 'C',
  `InvestName` varchar(48) DEFAULT '',
  `BrokerID` varchar(10) DEFAULT NULL,
  `Pwd` varchar(48) NOT NULL,
  `Type` char(3) NOT NULL,
  `OwnUserID` varchar(12) NOT NULL,
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`RegionID`),
  KEY `index2` (`InvestID`),
  KEY `index3` (`TraceType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_bail_temp
-- ----------------------------
DROP TABLE IF EXISTS `trace_bail_temp`;
CREATE TABLE `trace_bail_temp` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `TempID` int(11) NOT NULL,
  `TempName` varchar(30) NOT NULL,
  `ByOwnGroupID` int(11) DEFAULT '0',
  `ReMark` varchar(60) DEFAULT '',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`TempID`),
  KEY `index2` (`TraceType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_bail_tempobj
-- ----------------------------
DROP TABLE IF EXISTS `trace_bail_tempobj`;
CREATE TABLE `trace_bail_tempobj` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `id` int(11) NOT NULL,
  `TempID` int(11) NOT NULL,
  `MarketCode` char(3) DEFAULT 'all',
  `ClassCode` char(3) DEFAULT 'all',
  `BS` char(3) DEFAULT 'all',
  `UnderlyingCode` varchar(20) DEFAULT 'all',
  `Obj` varchar(20) DEFAULT 'all',
  `Bail0` float DEFAULT '0',
  `Bail1` float DEFAULT '0',
  `Bail2` float DEFAULT '0',
  `Bail0Rate` float DEFAULT '0',
  `Bail1Rate` float DEFAULT '0',
  `Bail2Rate` float DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`UnderlyingCode`),
  KEY `index2` (`ClassCode`),
  KEY `index3` (`TraceType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_fare_temp
-- ----------------------------
DROP TABLE IF EXISTS `trace_fare_temp`;
CREATE TABLE `trace_fare_temp` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `TempID` int(11) NOT NULL,
  `TempName` varchar(30) DEFAULT NULL,
  `ByOwnGroupID` int(11) DEFAULT NULL,
  `ReMark` varchar(60) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`TempID`),
  KEY `index2` (`TraceType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_fare_tempeobj
-- ----------------------------
DROP TABLE IF EXISTS `trace_fare_tempeobj`;
CREATE TABLE `trace_fare_tempeobj` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `id` int(11) NOT NULL,
  `TempID` int(11) NOT NULL,
  `MarketCode` char(3) DEFAULT 'all',
  `ClassCode` char(3) DEFAULT 'all',
  `UnderlyingCode` varchar(20) DEFAULT 'all',
  `Obj` varchar(20) DEFAULT 'all',
  `BS` char(3) DEFAULT 'all',
  `OpenClose` char(3) DEFAULT 'all',
  `Commission0` float DEFAULT '0',
  `Commission1` float DEFAULT '0',
  `MinCommission0` float DEFAULT '0',
  `MinCommission1` float DEFAULT '0',
  `DeliverFare0` float DEFAULT '0',
  `DeliverFare1` float DEFAULT '0',
  `Tax0` float DEFAULT '0',
  `Tax1` float DEFAULT '0',
  `Etax1` float DEFAULT '0',
  `Etax0` float DEFAULT '0',
  `Cost0` float DEFAULT '0',
  `Cost1` float DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`UnderlyingCode`),
  KEY `index2` (`TraceType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_obj_info
-- ----------------------------
DROP TABLE IF EXISTS `trace_obj_info`;
CREATE TABLE `trace_obj_info` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `ObjID` varchar(24) NOT NULL,
  `RegionID` char(3) NOT NULL,
  `Obj` varchar(20) NOT NULL,
  `MarketCode` char(3) NOT NULL,
  `ClassCode` char(3) NOT NULL,
  `Name` varchar(60) NOT NULL,
  `EName` varchar(20) NOT NULL,
  `LongName` varchar(40) DEFAULT '',
  `ListingDate` date DEFAULT NULL,
  `FaceValue` int(11) DEFAULT NULL,
  `TradeUnit` int(11) DEFAULT NULL,
  `Currency` char(3) DEFAULT 'RMB',
  `ContractSize` int(11) DEFAULT NULL,
  `TradeType` char(3) DEFAULT '1',
  `ClearType` char(3) DEFAULT '1',
  `ContractMonth` char(6) DEFAULT NULL,
  `ExpirationDate` date DEFAULT NULL,
  `UnderlyingCode` varchar(20) DEFAULT NULL,
  `Underlyingobj` varchar(20) DEFAULT NULL,
  `First` varchar(20) DEFAULT NULL,
  `Secend` varchar(20) DEFAULT NULL,
  `Third` varchar(20) DEFAULT NULL,
  `PutCall` char(3) DEFAULT NULL,
  `StrikePrice` float DEFAULT NULL,
  `OptionRate` float DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index5` (`ObjID`),
  KEY `index1` (`Obj`),
  KEY `index2` (`ClassCode`),
  KEY `index3` (`ListingDate`),
  KEY `index4` (`TraceType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_obj_protfolio
-- ----------------------------
DROP TABLE IF EXISTS `trace_obj_protfolio`;
CREATE TABLE `trace_obj_protfolio` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `ProtfolioID` int(11) NOT NULL,
  `Name` varchar(24) DEFAULT NULL,
  `IsPublic` char(3) DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`OwnUserID`),
  KEY `index2` (`ProtfolioID`),
  KEY `index3` (`TraceType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_obj_protfoliolist
-- ----------------------------
DROP TABLE IF EXISTS `trace_obj_protfoliolist`;
CREATE TABLE `trace_obj_protfoliolist` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `id` int(11) NOT NULL,
  `ProtfolioID` int(11) NOT NULL,
  `Obj` varchar(20) NOT NULL,
  `MarketCode` char(3) NOT NULL,
  `ClassCode` char(3) NOT NULL,
  `RegionID` char(3) DEFAULT 'C',
  `Qty` int(11) DEFAULT '0',
  `Rate` float DEFAULT '0',
  `ReMark` varchar(60) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`ProtfolioID`),
  KEY `index2` (`TraceType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_obj_underlying
-- ----------------------------
DROP TABLE IF EXISTS `trace_obj_underlying`;
CREATE TABLE `trace_obj_underlying` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `id` int(11) NOT NULL,
  `UnderlyingCode` varchar(20) NOT NULL,
  `RegionID` char(3) NOT NULL,
  `MarketCode` char(3) NOT NULL,
  `ClassCode` char(3) NOT NULL,
  `UnderlyingName` varchar(30) DEFAULT NULL,
  `Currency` char(3) DEFAULT NULL,
  `Tick` float DEFAULT NULL,
  `TradeUnit` int(11) NOT NULL,
  `ContractSize` int(11) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`UnderlyingCode`),
  KEY `index2` (`TraceType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_product_account
-- ----------------------------
DROP TABLE IF EXISTS `trace_product_account`;
CREATE TABLE `trace_product_account` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `id` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `UnitID` int(11) NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `Net` float DEFAULT NULL,
  `Share` bigint(20) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `BelongGroupID` int(11) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`StartDate`),
  KEY `index2` (`ProductID`),
  KEY `index3` (`BelongGroupID`),
  KEY `index4` (`TraceType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_product_info
-- ----------------------------
DROP TABLE IF EXISTS `trace_product_info`;
CREATE TABLE `trace_product_info` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `ProductName` varchar(30) NOT NULL,
  `Net` float DEFAULT NULL,
  `Share` bigint(20) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `BelongGroupID` int(11) DEFAULT NULL,
  `Type` char(3) DEFAULT NULL,
  `ReMark` varchar(60) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`StartDate`),
  KEY `index2` (`ProductID`),
  KEY `index3` (`BelongGroupID`),
  KEY `index4` (`TraceType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_product_unit
-- ----------------------------
DROP TABLE IF EXISTS `trace_product_unit`;
CREATE TABLE `trace_product_unit` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `UnitID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `UnitName` varchar(30) NOT NULL,
  `Net` float DEFAULT NULL,
  `Share` bigint(20) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `BelongGroupID` int(11) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`StartDate`),
  KEY `index2` (`UnitID`),
  KEY `index3` (`TraceType`),
  KEY `index4` (`ProductID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_risk_ctrlassets
-- ----------------------------
DROP TABLE IF EXISTS `trace_risk_ctrlassets`;
CREATE TABLE `trace_risk_ctrlassets` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `id` int(11) NOT NULL,
  `CtrlAssetsCode` varchar(20) NOT NULL,
  `Obj` varchar(20) NOT NULL,
  `IsSingleObj` char(3) NOT NULL,
  `CalcDirect` char(3) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`TraceType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_risk_sets
-- ----------------------------
DROP TABLE IF EXISTS `trace_risk_sets`;
CREATE TABLE `trace_risk_sets` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `id` int(11) NOT NULL,
  `RiskSetCode` varchar(35) NOT NULL,
  `IndexCode` varchar(20) DEFAULT NULL,
  `CtrlLevel` char(3) DEFAULT NULL,
  `CtrlObject` varchar(20) DEFAULT NULL,
  `CtrlAssetsCode` varchar(20) DEFAULT NULL,
  `OwnGroupID` int(11) DEFAULT NULL,
  `CtrlScene` char(3) DEFAULT NULL,
  `IsForceIntercept` char(3) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `GateValueA` float DEFAULT NULL,
  `GateValueB` float DEFAULT NULL,
  `GateValueC` float DEFAULT NULL,
  `GateDirectionA` char(3) DEFAULT NULL,
  `GateDirectionB` char(3) DEFAULT NULL,
  `GateDirectionC` char(3) DEFAULT NULL,
  `GateDirectionN` varchar(200) DEFAULT NULL,
  `GateValueN` varchar(200) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`RiskSetCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_safe_forbidlogin
-- ----------------------------
DROP TABLE IF EXISTS `trace_safe_forbidlogin`;
CREATE TABLE `trace_safe_forbidlogin` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `id` int(11) NOT NULL,
  `Mac` varchar(17) NOT NULL,
  `IP4` int(11) NOT NULL,
  `UserID` varchar(12) NOT NULL,
  `ReMark` varchar(60) NOT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_safe_otheraccess
-- ----------------------------
DROP TABLE IF EXISTS `trace_safe_otheraccess`;
CREATE TABLE `trace_safe_otheraccess` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `UserID` varchar(12) NOT NULL,
  `MaxOnline` int(6) DEFAULT '5',
  `LevelPrice` char(3) DEFAULT '1',
  `Dev` char(3) DEFAULT '0',
  `Reseach` char(3) DEFAULT '0',
  `ReBack` char(3) DEFAULT '0',
  `Trade` char(3) DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_tradeset_exchangelimit
-- ----------------------------
DROP TABLE IF EXISTS `trace_tradeset_exchangelimit`;
CREATE TABLE `trace_tradeset_exchangelimit` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `id` int(11) NOT NULL,
  `MarketCode` char(3) NOT NULL,
  `UnderlyingObj` varchar(20) DEFAULT '',
  `CancelLimit` int(11) NOT NULL,
  `OrderLimit` int(11) NOT NULL,
  `OnceMaxQty` int(11) NOT NULL,
  `ReMark` varchar(60) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`UnderlyingObj`),
  KEY `index2` (`TraceType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_user_access
-- ----------------------------
DROP TABLE IF EXISTS `trace_user_access`;
CREATE TABLE `trace_user_access` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `id` int(11) NOT NULL,
  `UserID` varchar(12) NOT NULL,
  `AccessUserID` varchar(12) NOT NULL,
  `AccessID` char(3) DEFAULT '1',
  `ReMark` varchar(60) DEFAULT NULL,
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`AccessUserID`),
  KEY `index2` (`UserID`),
  KEY `index3` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_user_account
-- ----------------------------
DROP TABLE IF EXISTS `trace_user_account`;
CREATE TABLE `trace_user_account` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `id` int(11) NOT NULL,
  `UserID` varchar(12) NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `AccessID` char(3) DEFAULT '1',
  `ReMark` varchar(60) DEFAULT NULL,
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`AccountCode`),
  KEY `index2` (`UserID`),
  KEY `index3` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_user_info
-- ----------------------------
DROP TABLE IF EXISTS `trace_user_info`;
CREATE TABLE `trace_user_info` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `id` int(11) NOT NULL,
  `UserID` varchar(12) NOT NULL,
  `UserName` varchar(30) NOT NULL,
  `Pwd` varchar(48) NOT NULL,
  `Stat` char(3) DEFAULT '1',
  `Type` char(3) DEFAULT '1',
  `LoginStat` char(3) DEFAULT '0',
  `CorrectNum` int(11) DEFAULT '0',
  `IsUSBKey` char(3) DEFAULT '0',
  `RoleID` char(3) DEFAULT '1',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`UserID`),
  KEY `index2` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_user_privateinfo
-- ----------------------------
DROP TABLE IF EXISTS `trace_user_privateinfo`;
CREATE TABLE `trace_user_privateinfo` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `id` int(11) NOT NULL,
  `UserID` varchar(12) NOT NULL,
  `UserName` varchar(30) NOT NULL,
  `SfID` varchar(48) NOT NULL,
  `AlterUserID` varchar(12) DEFAULT NULL,
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`UserID`),
  KEY `index2` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_work_flowpath
-- ----------------------------
DROP TABLE IF EXISTS `trace_work_flowpath`;
CREATE TABLE `trace_work_flowpath` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `id` int(11) NOT NULL,
  `GroupID` int(11) NOT NULL,
  `AnchorCode` char(3) NOT NULL,
  `FlowSeqNo` int(11) NOT NULL,
  `UserID` varchar(12) NOT NULL,
  `RoleID` char(3) NOT NULL,
  `ReMark` varchar(60) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`AnchorCode`,`FlowSeqNo`,`UserID`),
  KEY `index2` (`UserID`,`FlowSeqNo`,`AnchorCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_work_group
-- ----------------------------
DROP TABLE IF EXISTS `trace_work_group`;
CREATE TABLE `trace_work_group` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `GroupID` int(11) NOT NULL,
  `GroupName` varchar(48) DEFAULT NULL,
  `ManagerUserID` varchar(12) DEFAULT NULL,
  `Type` char(3) DEFAULT NULL,
  `UpGroupID` int(11) DEFAULT NULL,
  `Flag` char(3) DEFAULT '1',
  `Role` char(3) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`ManagerUserID`),
  KEY `index2` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_work_groupuser
-- ----------------------------
DROP TABLE IF EXISTS `trace_work_groupuser`;
CREATE TABLE `trace_work_groupuser` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `id` int(11) NOT NULL,
  `GroupID` int(11) NOT NULL,
  `UserID` varchar(12) NOT NULL,
  `RoleID` char(3) NOT NULL,
  `ReMark` varchar(60) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  UNIQUE KEY `unique_index` (`GroupID`,`UserID`) USING BTREE,
  KEY `index1` (`UserID`),
  KEY `index2` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trace_work_productflow
-- ----------------------------
DROP TABLE IF EXISTS `trace_work_productflow`;
CREATE TABLE `trace_work_productflow` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `TraceType` char(3) NOT NULL,
  `id` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `AnchorCode` char(3) NOT NULL,
  `FlowSeqNo` int(11) NOT NULL,
  `UserID` varchar(12) NOT NULL,
  `RoleID` char(3) NOT NULL,
  `BlongGroupID` int(11) NOT NULL,
  `ReMark` varchar(60) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sid`),
  KEY `index1` (`AnchorCode`,`FlowSeqNo`,`UserID`),
  KEY `index2` (`UserID`,`FlowSeqNo`,`AnchorCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
