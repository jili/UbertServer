/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : ubert_recent_db

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-07-29 19:59:15
Author: ChengJili
版本说明：
@20150729：原始版本定稿
*/
CREATE DATABASE IF NOT EXISTS ubert_recent_db DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
USE ubert_recent_db;
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for recent_clearing_accountbail
-- ----------------------------
DROP TABLE IF EXISTS `recent_clearing_accountbail`;
CREATE TABLE `recent_clearing_accountbail` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `ClearingDate` date NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `TempID` int(11) DEFAULT NULL,
  `MarketCode` char(3) NOT NULL,
  `ClassCode` char(3) NOT NULL,
  `BS` char(3) NOT NULL,
  `UnderlyingCode` varchar(20) NOT NULL,
  `Obj` varchar(20) NOT NULL,
  `Bail0` float DEFAULT NULL,
  `Bail1` float DEFAULT NULL,
  `Bail2` float DEFAULT NULL,
  `Bail0Rate` float DEFAULT NULL,
  `Bail1Rate` float DEFAULT NULL,
  `Bail2Rate` float DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ClearingDate`,`AccountCode`,`TempID`,`MarketCode`,`ClassCode`,`UnderlyingCode`,`Obj`,`BS`) USING BTREE,
  KEY `index1` (`TempID`),
  KEY `index2` (`AccountCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for recent_clearing_accountfare
-- ----------------------------
DROP TABLE IF EXISTS `recent_clearing_accountfare`;
CREATE TABLE `recent_clearing_accountfare` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `ClearingDate` date NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `TempID` int(11) DEFAULT '0',
  `MarketCode` char(3) DEFAULT '',
  `ClassCode` char(3) DEFAULT '',
  `UnderlyingCode` varchar(20) DEFAULT '',
  `Obj` varchar(20) DEFAULT '',
  `BS` char(3) DEFAULT '',
  `OpenClose` char(3) DEFAULT '',
  `Commission0` float DEFAULT NULL,
  `Commission1` float DEFAULT NULL,
  `MinCommission0` float DEFAULT NULL,
  `MinCommission1` float DEFAULT NULL,
  `DeliverFare0` float DEFAULT NULL,
  `DeliverFare1` float DEFAULT NULL,
  `Tax0` float DEFAULT NULL,
  `Tax1` float DEFAULT NULL,
  `Etax1` float DEFAULT NULL,
  `Etax0` float DEFAULT NULL,
  `Cost0` float DEFAULT NULL,
  `Cost1` float DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ClearingDate`,`AccountCode`,`TempID`,`MarketCode`,`ClassCode`,`UnderlyingCode`,`Obj`,`BS`,`OpenClose`) USING BTREE,
  KEY `index1` (`TempID`),
  KEY `index2` (`AccountCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for recent_clearing_daybook
-- ----------------------------
DROP TABLE IF EXISTS `recent_clearing_daybook`;
CREATE TABLE `recent_clearing_daybook` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `dayid` bigint(11) NOT NULL,
  `BookDate` date NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `BookID` int(11) NOT NULL,
  `ExeTime` time NOT NULL,
  `TradeDate` date NOT NULL,
  `AccountTitle` char(3) NOT NULL,
  `AmountReceived` double(10,6) DEFAULT '0.000000',
  `AmountPayed` double(10,6) DEFAULT '0.000000',
  `Balance` double(10,6) NOT NULL,
  `MarketCode` char(3) DEFAULT NULL,
  `ClassCode` char(3) DEFAULT NULL,
  `Obj` varchar(20) DEFAULT NULL,
  `Mac` varchar(17) DEFAULT NULL,
  `IP4` int(11) DEFAULT NULL,
  `OrderTypeEmit` char(3) NOT NULL,
  `TradeOperate` int(11) NOT NULL,
  `CloseType` char(3) DEFAULT NULL,
  `BS` char(3) DEFAULT NULL,
  `OpenClose` char(3) DEFAULT NULL,
  `HedgeFlag` char(3) DEFAULT NULL,
  `PosID` varchar(35) DEFAULT NULL,
  `ExeSequenceNo` int(11) DEFAULT NULL,
  `OutsideExeNo` varchar(35) DEFAULT NULL,
  `ExeQty` int(11) DEFAULT NULL,
  `ExePrice` float DEFAULT NULL,
  `Currency` char(3) DEFAULT NULL,
  `ExeAmount` float DEFAULT NULL,
  `TotalFare` float DEFAULT NULL,
  `CommissionFare` float DEFAULT NULL,
  `DeliverFare` float DEFAULT NULL,
  `Tax` float DEFAULT NULL,
  `OtherCost` float DEFAULT NULL,
  `EsCheckinAmount` double DEFAULT NULL,
  `InvestID` varchar(12) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`dayid`,`BookDate`) USING BTREE,
  KEY `index1` (`AccountCode`),
  KEY `index2` (`ExeTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for recent_clearing_exright
-- ----------------------------
DROP TABLE IF EXISTS `recent_clearing_exright`;
CREATE TABLE `recent_clearing_exright` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `AccountCode` varchar(16) NOT NULL,
  `ClearingDate` date NOT NULL,
  `Obj` varchar(20) NOT NULL,
  `MarketCode` char(3) NOT NULL,
  `ClassCode` char(3) NOT NULL,
  `RecordeDate` date NOT NULL,
  `PosQty` int(11) DEFAULT '0',
  `ProfitsAmount` float DEFAULT '0',
  `ShareA` int(11) DEFAULT '0',
  `ShareB` int(11) DEFAULT '0',
  `Tax` float DEFAULT '0',
  `NetProfitsAmount` float DEFAULT '0',
  `ReMark` varchar(60) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ClearingDate`,`AccountCode`,`Obj`,`MarketCode`,`ClassCode`) USING BTREE,
  KEY `index1` (`AccountCode`),
  KEY `index2` (`Obj`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for recent_clearing_fundstate
-- ----------------------------
DROP TABLE IF EXISTS `recent_clearing_fundstate`;
CREATE TABLE `recent_clearing_fundstate` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `ClearingDate` date DEFAULT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `FundTotal` float DEFAULT '0',
  `FundAvl` float DEFAULT '0',
  `FundFroze` float DEFAULT '0',
  `AssetTotal` float DEFAULT '0',
  `AssetNet` float DEFAULT '0',
  `AssetCredit` float DEFAULT '0',
  `PosTotalValue` float DEFAULT '0',
  `PosNetValue` float DEFAULT '0',
  `PosCreditValue` float DEFAULT '0',
  `Currency` char(3) DEFAULT 'RMB',
  `Rate` float DEFAULT '0',
  `EqualFundAvl` float DEFAULT '0',
  `BefAssetTotal` float DEFAULT '0',
  `BefFundAvl` float DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ClearingDate`,`AccountCode`) USING BTREE,
  KEY `index1` (`AccountCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for recent_clearing_posdetail
-- ----------------------------
DROP TABLE IF EXISTS `recent_clearing_posdetail`;
CREATE TABLE `recent_clearing_posdetail` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `ClearingDate` date NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `PosID` varchar(35) DEFAULT '',
  `Obj` varchar(20) NOT NULL,
  `MarketCode` char(3) NOT NULL,
  `ClassCode` char(3) NOT NULL,
  `BS` char(3) NOT NULL,
  `Qty` int(11) NOT NULL,
  `AvlQty` int(11) NOT NULL,
  `FrozeQty` int(11) DEFAULT '0',
  `ClosePrice` float DEFAULT '0',
  `CostPriceA` float DEFAULT '0',
  `CostPriceB` float DEFAULT '0',
  `opencostprice` float DEFAULT '0',
  `AvgCostPrice` float DEFAULT '0',
  `HedgeFlag` char(3) DEFAULT '0',
  `PosType` char(3) DEFAULT '0',
  `PutCall` char(3) DEFAULT '0',
  `Currency` char(3) DEFAULT 'RMB',
  `PL` float DEFAULT '0',
  `CostPL` float DEFAULT '0',
  `TotalPL` float DEFAULT '0',
  `Margin` float DEFAULT NULL,
  `TotalFare` float DEFAULT NULL,
  `InvestID` varchar(12) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT NULL,
  `AlterUserID` varchar(12) DEFAULT NULL,
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ClearingDate`,`AccountCode`,`PosID`,`MarketCode`,`ClassCode`,`Obj`,`BS`) USING BTREE,
  KEY `index1` (`AccountCode`),
  KEY `index2` (`PosID`),
  KEY `index3` (`Obj`),
  KEY `index4` (`BS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for recent_clearing_price
-- ----------------------------
DROP TABLE IF EXISTS `recent_clearing_price`;
CREATE TABLE `recent_clearing_price` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `ClearingDate` date DEFAULT NULL,
  `RegionID` char(3) NOT NULL,
  `Obj` varchar(20) NOT NULL,
  `ClassCode` char(3) NOT NULL,
  `MarketCode` char(3) NOT NULL,
  `ClosePrice` float NOT NULL,
  `clearprice` float DEFAULT NULL,
  `ALCPrice` float NOT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ClearingDate`,`Obj`,`ClassCode`,`MarketCode`) USING BTREE,
  KEY `index1` (`Obj`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for recent_clearing_protfoliopos
-- ----------------------------
DROP TABLE IF EXISTS `recent_clearing_protfoliopos`;
CREATE TABLE `recent_clearing_protfoliopos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ClearingDate` date NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `PosID` varchar(35) DEFAULT '',
  `Qty` int(11) NOT NULL,
  `AvlQty` int(11) NOT NULL,
  `FrozeQty` int(11) DEFAULT '0',
  `ClosePrice` float DEFAULT '0',
  `CostPriceA` float DEFAULT '0',
  `CostPriceB` float DEFAULT '0',
  `opencostprice` float DEFAULT '0',
  `HedgeFlag` char(3) DEFAULT '0',
  `Currency` char(3) DEFAULT 'RMB',
  `PL` float DEFAULT '0',
  `CostPL` float DEFAULT '0',
  `TotalPL` float DEFAULT '0',
  `Margin` float DEFAULT NULL,
  `TotalFare` float DEFAULT NULL,
  `InvestID` varchar(12) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT NULL,
  `AlterUserID` varchar(12) DEFAULT NULL,
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ClearingDate`,`AccountCode`,`PosID`) USING BTREE,
  KEY `index1` (`AccountCode`),
  KEY `index2` (`PosID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for recent_obj_etf
-- ----------------------------
DROP TABLE IF EXISTS `recent_obj_etf`;
CREATE TABLE `recent_obj_etf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ListingDate` date NOT NULL,
  `ETFID` varchar(20) NOT NULL,
  `Obj` varchar(20) NOT NULL,
  `PrimaryObj` varchar(20) DEFAULT NULL,
  `CashObj` varchar(20) DEFAULT NULL,
  `Name` varchar(30) DEFAULT NULL,
  `MarketCode` char(3) DEFAULT NULL,
  `FundManagementCompany` varchar(30) DEFAULT NULL,
  `UnderlyingIndex` varchar(20) DEFAULT NULL,
  `TradeUnit` int(11) DEFAULT '1',
  `EstimateCashCompoment` float DEFAULT '0',
  `MaxCashRatio` float DEFAULT '0',
  `Publish` char(3) DEFAULT NULL,
  `Creation` char(3) DEFAULT NULL,
  `Redemption` char(3) DEFAULT NULL,
  `RecordeNum` int(11) DEFAULT NULL,
  `CashComponent` float DEFAULT '0',
  `NavPercu` float DEFAULT '0',
  `Nav` float DEFAULT '0',
  `DividendPercu` float DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ListingDate`,`ETFID`,`MarketCode`) USING BTREE,
  KEY `index1` (`Obj`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for recent_obj_etflist
-- ----------------------------
DROP TABLE IF EXISTS `recent_obj_etflist`;
CREATE TABLE `recent_obj_etflist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ETFID` varchar(20) NOT NULL,
  `ListingDate` date NOT NULL,
  `Obj` varchar(20) NOT NULL,
  `MarketCode` char(3) NOT NULL,
  `ClassCode` char(3) NOT NULL,
  `Qty` int(11) DEFAULT NULL,
  `CanCashRepl` char(3) DEFAULT '1',
  `CashBuffer` float DEFAULT '0',
  `CashAmount` float DEFAULT '0',
  `ReMark` varchar(60) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ListingDate`,`ETFID`,`Obj`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for recent_obj_index
-- ----------------------------
DROP TABLE IF EXISTS `recent_obj_index`;
CREATE TABLE `recent_obj_index` (
  `IndexID` int(11) NOT NULL AUTO_INCREMENT,
  `ListingDate` date NOT NULL,
  `IndexObj` varchar(20) NOT NULL,
  `IndexName` varchar(30) NOT NULL,
  `RegionID` char(3) DEFAULT 'C',
  `MarketCode` char(3) NOT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`IndexID`),
  UNIQUE KEY `unique_index` (`ListingDate`,`IndexObj`,`MarketCode`) USING BTREE,
  KEY `index1` (`MarketCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for recent_obj_indexlist
-- ----------------------------
DROP TABLE IF EXISTS `recent_obj_indexlist`;
CREATE TABLE `recent_obj_indexlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IndexID` int(11) NOT NULL,
  `IndexObj` varchar(20) NOT NULL,
  `Obj` varchar(20) NOT NULL,
  `RegionID` char(3) DEFAULT 'C',
  `MarketCode` char(3) NOT NULL,
  `ClassCode` char(3) NOT NULL,
  `Qty` int(11) NOT NULL,
  `Rate` float NOT NULL,
  `ListingDate` date NOT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ListingDate`,`IndexID`,`Obj`,`MarketCode`,`ClassCode`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for recent_trade_alterorder
-- ----------------------------
DROP TABLE IF EXISTS `recent_trade_alterorder`;
CREATE TABLE `recent_trade_alterorder` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `OrderID` int(11) NOT NULL,
  `DealerUserID` varchar(12) NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `Time` time NOT NULL,
  `TradeDate` date NOT NULL,
  `DateTime` datetime NOT NULL,
  `Mac` varchar(17) DEFAULT NULL,
  `IP4` int(11) DEFAULT NULL,
  `OrderTypeEmit` char(3) NOT NULL,
  `TradeOperate` int(11) NOT NULL,
  `EntrustNo` int(11) DEFAULT NULL,
  `Entrustkey` varchar(35) DEFAULT NULL,
  `AlterQty` int(11) DEFAULT NULL,
  `AlterPrice` float DEFAULT NULL,
  `WorkStationID` char(3) DEFAULT '',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`TradeDate`,`OrderID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for recent_trade_cancelorder
-- ----------------------------
DROP TABLE IF EXISTS `recent_trade_cancelorder`;
CREATE TABLE `recent_trade_cancelorder` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `OrderID` int(11) NOT NULL,
  `DealerUserID` varchar(12) NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `Time` time NOT NULL,
  `TradeDate` date NOT NULL,
  `DateTime` datetime NOT NULL,
  `Mac` varchar(17) DEFAULT NULL,
  `IP4` int(11) DEFAULT NULL,
  `OrderTypeEmit` char(3) NOT NULL,
  `TradeOperate` int(11) NOT NULL,
  `EntrustNo` int(11) DEFAULT NULL,
  `Entrustkey` varchar(35) DEFAULT NULL,
  `CancelQty` int(11) DEFAULT NULL,
  `WorkStationID` char(3) DEFAULT '',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`TradeDate`,`OrderID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for recent_trade_executeorder
-- ----------------------------
DROP TABLE IF EXISTS `recent_trade_executeorder`;
CREATE TABLE `recent_trade_executeorder` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `OrderID` int(11) NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `DealerUserID` varchar(12) NOT NULL,
  `SubmitTime` time NOT NULL,
  `TradeDate` date NOT NULL,
  `DateTime` datetime NOT NULL,
  `Mac` varchar(17) DEFAULT NULL,
  `IP4` int(11) DEFAULT NULL,
  `OrderTypeEmit` char(3) NOT NULL,
  `TradeOperate` int(11) NOT NULL,
  `MarketCode` char(3) DEFAULT NULL,
  `ClassCode` char(3) DEFAULT NULL,
  `Obj` varchar(20) DEFAULT NULL,
  `BS` char(3) DEFAULT NULL,
  `OpenClose` char(3) DEFAULT NULL,
  `HedgeFlag` char(3) DEFAULT NULL,
  `PosID` varchar(35) DEFAULT NULL,
  `CustomCode` varchar(35) DEFAULT NULL,
  `CloseType` char(3) DEFAULT NULL,
  `BasketID` int(11) DEFAULT '-1',
  `BasketTatol` int(11) DEFAULT NULL,
  `SeqNum` int(11) DEFAULT NULL,
  `ExeTime` time DEFAULT NULL,
  `ExeSequenceNo` int(11) NOT NULL,
  `OutsideExeNo` varchar(35) DEFAULT NULL,
  `ExeQty` int(11) DEFAULT NULL,
  `ExePrice` float DEFAULT NULL,
  `Currency` char(3) DEFAULT NULL,
  `ExeAmount` float DEFAULT NULL,
  `CancelQty` int(11) DEFAULT NULL,
  `OrderQty` int(11) DEFAULT NULL,
  `WorkingQty` int(11) DEFAULT NULL,
  `CompleteQty` int(11) DEFAULT NULL,
  `OrderStatus` char(3) DEFAULT NULL,
  `TotalFare` float DEFAULT '0',
  `CommissionFare` float DEFAULT '0',
  `DeliverFare` float DEFAULT '0',
  `Tax` float DEFAULT '0',
  `OtherCost` float DEFAULT '0',
  `EsCheckinAmount` double DEFAULT '0',
  `ReMark` varchar(200) DEFAULT NULL,
  `InvestID` varchar(12) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`TradeDate`,`OrderID`) USING BTREE,
  KEY `index1` (`AccountCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for recent_trade_neworder
-- ----------------------------
DROP TABLE IF EXISTS `recent_trade_neworder`;
CREATE TABLE `recent_trade_neworder` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `OrderID` int(11) NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `DealerUserID` varchar(12) NOT NULL,
  `SubmitTime` time NOT NULL,
  `TradeDate` date NOT NULL,
  `DateTime` datetime NOT NULL,
  `Mac` varchar(17) DEFAULT NULL,
  `IP4` int(11) DEFAULT NULL,
  `OrderTypeEmit` char(3) NOT NULL,
  `TradeOperate` int(11) NOT NULL,
  `Obj` varchar(20) NOT NULL,
  `MarketCode` char(3) DEFAULT NULL,
  `ClassCode` char(3) DEFAULT NULL,
  `BS` char(3) DEFAULT NULL,
  `OpenClose` char(3) DEFAULT NULL,
  `PriceType` char(3) DEFAULT NULL,
  `OrderPrice` float DEFAULT NULL,
  `OrderQty` int(11) DEFAULT NULL,
  `HedgeFlag` char(3) DEFAULT NULL,
  `PosID` varchar(35) DEFAULT NULL,
  `CustomCode` varchar(35) DEFAULT NULL,
  `CloseType` char(3) DEFAULT NULL,
  `BasketID` int(11) DEFAULT '-1',
  `BasketTatol` int(11) DEFAULT NULL,
  `SeqNum` int(11) DEFAULT NULL,
  `WorkStationID` char(3) DEFAULT '',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`TradeDate`,`OrderID`) USING BTREE,
  KEY `index1` (`AccountCode`),
  KEY `index2` (`BasketID`),
  KEY `index3` (`Obj`),
  KEY `index4` (`PosID`),
  KEY `index5` (`SubmitTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for recent_trade_reportdetail
-- ----------------------------
DROP TABLE IF EXISTS `recent_trade_reportdetail`;
CREATE TABLE `recent_trade_reportdetail` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `MsgType` char(2) DEFAULT NULL,
  `OrderID` int(11) NOT NULL,
  `ReportNo` int(11) NOT NULL,
  `ReportContent` text,
  `EntrustNo` int(11) DEFAULT NULL,
  `Entrustkey` varchar(35) DEFAULT NULL,
  `WorkingQty` int(11) DEFAULT NULL,
  `UnWorkingQty` int(11) DEFAULT NULL,
  `CancelQty` int(11) DEFAULT NULL,
  `ExeQtyTatol` int(11) DEFAULT NULL,
  `ExePriceAvg` float DEFAULT NULL,
  `ExeAmountTatol` double DEFAULT NULL,
  `Price` float DEFAULT '0',
  `ExexSeq` int(11) NOT NULL,
  `ExeSequenceNo` int(11) NOT NULL,
  `OutsideExeNo` varchar(35) DEFAULT NULL,
  `ExeQty` int(11) DEFAULT NULL,
  `ExePrice` float DEFAULT NULL,
  `Currency` char(3) DEFAULT NULL,
  `ExeAmount` float DEFAULT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `MarketCode` char(3) DEFAULT NULL,
  `ClassCode` char(3) DEFAULT NULL,
  `Obj` varchar(20) DEFAULT NULL,
  `BS` char(3) DEFAULT NULL,
  `OpenClose` char(3) DEFAULT NULL,
  `HedgeFlag` char(3) DEFAULT NULL,
  `CloseType` char(3) DEFAULT NULL,
  `ExeTime` time DEFAULT NULL,
  `EntrustTime` time DEFAULT NULL,
  `SubmitTime` time DEFAULT NULL,
  `TradeDate` date NOT NULL,
  `InvestID` varchar(12) DEFAULT NULL,
  `ReMark` varchar(200) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`TradeDate`,`MsgType`,`OrderID`,`exexseq`,`ReportNo`) USING BTREE,
  KEY `index1` (`AccountCode`),
  KEY `index2` (`OrderID`),
  KEY `index3` (`Obj`),
  KEY `index4` (`ExeTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
