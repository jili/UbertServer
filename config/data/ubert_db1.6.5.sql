/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : ubert_db

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-07-29 19:59:15
Author: ChengJili
版本说明：
@20150729：原始版本定稿
*/
CREATE DATABASE IF NOT EXISTS ubert_db DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
USE ubert_db;
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for account_bail
-- ----------------------------
DROP TABLE IF EXISTS `account_bail`;
CREATE TABLE `account_bail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `AccountCode` varchar(16) NOT NULL,
  `TempID` int(11) DEFAULT '0',
  `MarketCode` char(3) DEFAULT 'all',
  `ClassCode` char(3) DEFAULT 'all',
  `BS` char(3) DEFAULT 'all',
  `UnderlyingCode` varchar(20) DEFAULT 'all',
  `Obj` varchar(20) DEFAULT 'all',
  `Bail0` float DEFAULT '0',
  `Bail1` float DEFAULT '0',
  `Bail2` float DEFAULT '0',
  `Bail0Rate` float DEFAULT '0',
  `Bail1Rate` float DEFAULT '0',
  `Bail2Rate` float DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`AccountCode`,`TempID`,`MarketCode`,`ClassCode`,`UnderlyingCode`,`Obj`,`BS`) USING BTREE,
  KEY `index1` (`TempID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for account_fare
-- ----------------------------
DROP TABLE IF EXISTS `account_fare`;
CREATE TABLE `account_fare` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `AccountCode` varchar(16) NOT NULL,
  `TempID` int(11) DEFAULT '0',
  `MarketCode` char(3) DEFAULT 'all',
  `ClassCode` char(3) DEFAULT 'all',
  `UnderlyingCode` varchar(20) DEFAULT 'all',
  `Obj` varchar(20) DEFAULT 'all',
  `BS` char(3) DEFAULT 'all',
  `OpenClose` char(3) DEFAULT 'all',
  `Commission0` float DEFAULT '0',
  `Commission1` float DEFAULT '0',
  `MinCommission0` float DEFAULT '0',
  `MinCommission1` float DEFAULT '0',
  `DeliverFare0` float DEFAULT '0',
  `DeliverFare1` float DEFAULT '0',
  `Tax0` float DEFAULT '0',
  `Tax1` float DEFAULT '0',
  `Etax1` float DEFAULT '0',
  `Etax0` float DEFAULT '0',
  `Cost0` float DEFAULT '0',
  `Cost1` float DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`AccountCode`,`TempID`,`MarketCode`,`ClassCode`,`UnderlyingCode`,`Obj`,`BS`,`OpenClose`) USING BTREE,
  KEY `index1` (`TempID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for account_icode
-- ----------------------------
DROP TABLE IF EXISTS `account_icode`;
CREATE TABLE `account_icode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iid` int(11) NOT NULL,
  `InvestID` varchar(20) NOT NULL,
  `Type` char(3) NOT NULL,
  `Code` varchar(20) NOT NULL,
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`iid`,`Type`) USING BTREE,
  KEY `index1` (`InvestID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for account_info
-- ----------------------------
DROP TABLE IF EXISTS `account_info`;
CREATE TABLE `account_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `AccountCode` varchar(16) NOT NULL,
  `AccountName` varchar(30) DEFAULT NULL,
  `PipID` int(11) NOT NULL,
  `Pwd` varchar(48) DEFAULT NULL,
  `IsRisk` char(3) DEFAULT '0',
  `iid` int(11) NOT NULL,
  `Type` char(3) DEFAULT NULL,
  `InvestID` varchar(12) NOT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`AccountCode`) USING BTREE,
  KEY `index1` (`PipID`),
  KEY `index2` (`IsRisk`),
  KEY `index3` (`iid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for account_invest
-- ----------------------------
DROP TABLE IF EXISTS `account_invest`;
CREATE TABLE `account_invest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `InvestID` varchar(12) NOT NULL,
  `RegionID` char(3) NOT NULL,
  `InvestName` varchar(48) DEFAULT '',
  `BrokerID` varchar(10) DEFAULT NULL,
  `Pwd` varchar(48) NOT NULL,
  `Type` char(3) NOT NULL,
  `OwnUserID` varchar(12) NOT NULL,
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`InvestID`,`RegionID`,`BrokerID`) USING BTREE,
  KEY `index1` (`RegionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bail_temp
-- ----------------------------
DROP TABLE IF EXISTS `bail_temp`;
CREATE TABLE `bail_temp` (
  `TempID` int(11) NOT NULL AUTO_INCREMENT,
  `TempName` varchar(30) NOT NULL,
  `ByOwnGroupID` int(11) DEFAULT '0',
  `ReMark` varchar(60) DEFAULT '',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`TempID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bail_tempobj
-- ----------------------------
DROP TABLE IF EXISTS `bail_tempobj`;
CREATE TABLE `bail_tempobj` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `TempID` int(11) NOT NULL,
  `MarketCode` char(3) DEFAULT 'all',
  `ClassCode` char(3) DEFAULT 'all',
  `BS` char(3) DEFAULT 'all',
  `UnderlyingCode` varchar(20) DEFAULT 'all',
  `Obj` varchar(20) DEFAULT 'all',
  `Bail0` float DEFAULT '0',
  `Bail1` float DEFAULT '0',
  `Bail2` float DEFAULT '0',
  `Bail0Rate` float DEFAULT '0',
  `Bail1Rate` float DEFAULT '0',
  `Bail2Rate` float DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`TempID`,`MarketCode`,`ClassCode`,`UnderlyingCode`,`Obj`,`BS`) USING BTREE,
  KEY `index1` (`UnderlyingCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for clearing_currencyrates
-- ----------------------------
DROP TABLE IF EXISTS `clearing_currencyrates`;
CREATE TABLE `clearing_currencyrates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ClearingDate` date NOT NULL,
  `AskCurrency` char(3) NOT NULL,
  `BidCurrency` char(3) NOT NULL,
  `RatePrice` float DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ClearingDate`,`AskCurrency`,`BidCurrency`) USING BTREE,
  KEY `index1` (`AskCurrency`,`BidCurrency`),
  KEY `index2` (`BidCurrency`,`AskCurrency`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for clearing_daybook
-- ----------------------------
DROP TABLE IF EXISTS `clearing_daybook`;
CREATE TABLE `clearing_daybook` (
  `dayid` bigint(11) NOT NULL AUTO_INCREMENT,
  `BookDate` date NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `BookID` int(11) NOT NULL,
  `ExeTime` time NOT NULL,
  `TradeDate` date NOT NULL,
  `AccountTitle` char(3) NOT NULL,
  `AmountReceived` double(10,6) DEFAULT '0.000000',
  `AmountPayed` double(10,6) DEFAULT '0.000000',
  `Balance` double(10,6) NOT NULL,
  `MarketCode` char(3) DEFAULT NULL,
  `ClassCode` char(3) DEFAULT NULL,
  `Obj` varchar(20) DEFAULT NULL,
  `Mac` varchar(17) DEFAULT NULL,
  `IP4` int(11) DEFAULT NULL,
  `OrderTypeEmit` char(3) NOT NULL,
  `TradeOperate` int(11) NOT NULL,
  `CloseType` char(3) DEFAULT NULL,
  `BS` char(3) DEFAULT NULL,
  `OpenClose` char(3) DEFAULT NULL,
  `HedgeFlag` char(3) DEFAULT NULL,
  `PosID` varchar(35) DEFAULT NULL,
  `ExeSequenceNo` int(11) DEFAULT NULL,
  `OutsideExeNo` varchar(35) DEFAULT NULL,
  `ExeQty` int(11) DEFAULT NULL,
  `ExePrice` float DEFAULT NULL,
  `Currency` char(3) DEFAULT NULL,
  `ExeAmount` float DEFAULT NULL,
  `TotalFare` float DEFAULT NULL,
  `CommissionFare` float DEFAULT NULL,
  `DeliverFare` float DEFAULT NULL,
  `Tax` float DEFAULT NULL,
  `OtherCost` float DEFAULT NULL,
  `EsCheckinAmount` double DEFAULT NULL,
  `InvestID` varchar(12) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`dayid`),
  KEY `index1` (`AccountCode`),
  KEY `index2` (`ExeTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for clearing_exright
-- ----------------------------
DROP TABLE IF EXISTS `clearing_exright`;
CREATE TABLE `clearing_exright` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `AccountCode` varchar(16) NOT NULL,
  `ClearingDate` date NOT NULL,
  `Obj` varchar(20) NOT NULL,
  `MarketCode` char(3) NOT NULL,
  `ClassCode` char(3) NOT NULL,
  `RecordeDate` date NOT NULL,
  `PosQty` int(11) DEFAULT '0',
  `ProfitsAmount` float DEFAULT '0',
  `ShareA` int(11) DEFAULT '0',
  `ShareB` int(11) DEFAULT '0',
  `Tax` float DEFAULT '0',
  `NetProfitsAmount` float DEFAULT '0',
  `ReMark` varchar(60) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ClearingDate`,`AccountCode`,`Obj`,`MarketCode`,`ClassCode`) USING BTREE,
  KEY `index1` (`AccountCode`),
  KEY `index2` (`Obj`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for clearing_fundstate
-- ----------------------------
DROP TABLE IF EXISTS `clearing_fundstate`;
CREATE TABLE `clearing_fundstate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ClearingDate` date DEFAULT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `FundTotal` float DEFAULT '0',
  `FundAvl` float DEFAULT '0',
  `FundFroze` float DEFAULT '0',
  `AssetTotal` float DEFAULT '0',
  `AssetNet` float DEFAULT '0',
  `AssetCredit` float DEFAULT '0',
  `PosTotalValue` float DEFAULT '0',
  `PosNetValue` float DEFAULT '0',
  `PosCreditValue` float DEFAULT '0',
  `Currency` char(3) DEFAULT 'RMB',
  `Rate` float DEFAULT '0',
  `EqualFundAvl` float DEFAULT '0',
  `BefAssetTotal` float DEFAULT '0',
  `BefFundAvl` float DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ClearingDate`,`AccountCode`) USING BTREE,
  KEY `index1` (`AccountCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for clearing_posdetail
-- ----------------------------
DROP TABLE IF EXISTS `clearing_posdetail`;
CREATE TABLE `clearing_posdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ClearingDate` date NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `PosID` varchar(35) DEFAULT '',
  `Obj` varchar(20) NOT NULL,
  `MarketCode` char(3) NOT NULL,
  `ClassCode` char(3) NOT NULL,
  `BS` char(3) NOT NULL,
  `Qty` int(11) NOT NULL,
  `AvlQty` int(11) NOT NULL,
  `FrozeQty` int(11) DEFAULT '0',
  `ClosePrice` float DEFAULT '0',
  `CostPriceA` float DEFAULT '0',
  `CostPriceB` float DEFAULT '0',
  `opencostprice` float DEFAULT '0',
  `AvgCostPrice` float DEFAULT '0',
  `HedgeFlag` char(3) DEFAULT '0',
  `PosType` char(3) DEFAULT '0',
  `PutCall` char(3) DEFAULT '0',
  `Currency` char(3) DEFAULT 'RMB',
  `PL` float DEFAULT '0',
  `CostPL` float DEFAULT '0',
  `TotalPL` float DEFAULT '0',
  `Margin` float DEFAULT NULL,
  `TotalFare` float DEFAULT NULL,
  `InvestID` varchar(12) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT NULL,
  `AlterUserID` varchar(12) DEFAULT NULL,
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ClearingDate`,`AccountCode`,`PosID`,`MarketCode`,`ClassCode`,`Obj`,`BS`) USING BTREE,
  KEY `index1` (`AccountCode`),
  KEY `index2` (`PosID`),
  KEY `index3` (`Obj`),
  KEY `index4` (`BS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for clearing_price
-- ----------------------------
DROP TABLE IF EXISTS `clearing_price`;
CREATE TABLE `clearing_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ClearingDate` date DEFAULT NULL,
  `RegionID` char(3) DEFAULT NULL,
  `Obj` varchar(20) NOT NULL,
  `ClassCode` char(3) NOT NULL,
  `MarketCode` char(3) NOT NULL,
  `ClosePrice` float NOT NULL,
  `clearprice` float DEFAULT NULL,
  `ALCPrice` float NOT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ClearingDate`,`Obj`,`ClassCode`,`MarketCode`) USING BTREE,
  KEY `index1` (`Obj`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for clearing_protfoliopos
-- ----------------------------
DROP TABLE IF EXISTS `clearing_protfoliopos`;
CREATE TABLE `clearing_protfoliopos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ClearingDate` date NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `PosID` varchar(35) DEFAULT '',
  `Qty` int(11) NOT NULL,
  `AvlQty` int(11) NOT NULL,
  `FrozeQty` int(11) DEFAULT '0',
  `ClosePrice` float DEFAULT '0',
  `CostPriceA` float DEFAULT '0',
  `CostPriceB` float DEFAULT '0',
  `opencostprice` float DEFAULT '0',
  `HedgeFlag` char(3) DEFAULT '0',
  `Currency` char(3) DEFAULT 'RMB',
  `PL` float DEFAULT '0',
  `CostPL` float DEFAULT '0',
  `TotalPL` float DEFAULT '0',
  `Margin` float DEFAULT NULL,
  `TotalFare` float DEFAULT NULL,
  `InvestID` varchar(12) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT NULL,
  `AlterUserID` varchar(12) DEFAULT NULL,
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ClearingDate`,`AccountCode`,`PosID`) USING BTREE,
  KEY `index1` (`AccountCode`),
  KEY `index2` (`PosID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for fare_temp
-- ----------------------------
DROP TABLE IF EXISTS `fare_temp`;
CREATE TABLE `fare_temp` (
  `TempID` int(11) NOT NULL AUTO_INCREMENT,
  `TempName` varchar(30) DEFAULT NULL,
  `ByOwnGroupID` int(11) DEFAULT NULL,
  `ReMark` varchar(60) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`TempID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for fare_tempeobj
-- ----------------------------
DROP TABLE IF EXISTS `fare_tempeobj`;
CREATE TABLE `fare_tempeobj` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `TempID` int(11) DEFAULT '0',
  `MarketCode` char(3) DEFAULT 'all',
  `ClassCode` char(3) DEFAULT 'all',
  `UnderlyingCode` varchar(20) DEFAULT 'all',
  `Obj` varchar(20) DEFAULT 'all',
  `BS` char(3) DEFAULT 'all',
  `OpenClose` char(3) DEFAULT 'all',
  `Commission0` float DEFAULT '0',
  `Commission1` float DEFAULT '0',
  `MinCommission0` float DEFAULT '0',
  `MinCommission1` float DEFAULT '0',
  `DeliverFare0` float DEFAULT '0',
  `DeliverFare1` float DEFAULT '0',
  `Tax0` float DEFAULT '0',
  `Tax1` float DEFAULT '0',
  `Etax1` float DEFAULT '0',
  `Etax0` float DEFAULT '0',
  `Cost0` float DEFAULT '0',
  `Cost1` float DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`TempID`,`MarketCode`,`ClassCode`,`UnderlyingCode`,`Obj`,`BS`,`OpenClose`) USING BTREE,
  KEY `index1` (`UnderlyingCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for iopv_account
-- ----------------------------
DROP TABLE IF EXISTS `iopv_account`;
CREATE TABLE `iopv_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Date` date NOT NULL,
  `ProductID` int(11) NOT NULL,
  `UnitID` int(11) NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `Net` float DEFAULT '0',
  `IOPV` float DEFAULT '0',
  `RealIOPV` float DEFAULT '0',
  `CurrentLot` bigint(11) DEFAULT '0',
  `DeltLot` int(11) DEFAULT '0',
  `FeeOfDay` float DEFAULT '0',
  `ClearFeeaAcumulation` float DEFAULT '0',
  `TotalAssetValue` float DEFAULT '0',
  `PosRate` float DEFAULT '0',
  `DebtAssetValue` float DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`Date`,`ProductID`,`UnitID`,`AccountCode`) USING BTREE,
  KEY `index1` (`AccountCode`),
  KEY `index2` (`UnitID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for iopv_product
-- ----------------------------
DROP TABLE IF EXISTS `iopv_product`;
CREATE TABLE `iopv_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Date` date NOT NULL,
  `ProductID` int(11) NOT NULL,
  `Net` float DEFAULT '0',
  `IOPV` float DEFAULT '0',
  `RealIOPV` float DEFAULT '0',
  `CurrentLot` bigint(11) DEFAULT '0',
  `DeltLot` int(11) DEFAULT '0',
  `FeeOfDay` float DEFAULT '0',
  `ClearFeeaAcumulation` float DEFAULT '0',
  `TotalAssetValue` float DEFAULT '0',
  `PosRate` float DEFAULT '0',
  `DebtAssetValue` float DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`Date`,`ProductID`) USING BTREE,
  KEY `index1` (`ProductID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for iopv_productunit
-- ----------------------------
DROP TABLE IF EXISTS `iopv_productunit`;
CREATE TABLE `iopv_productunit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Date` date NOT NULL,
  `ProductID` int(11) NOT NULL,
  `UnitID` int(11) NOT NULL,
  `Net` float DEFAULT '0',
  `IOPV` float DEFAULT '0',
  `RealIOPV` float DEFAULT '0',
  `CurrentLot` bigint(11) DEFAULT '0',
  `DeltLot` int(11) DEFAULT '0',
  `FeeOfDay` float DEFAULT '0',
  `ClearFeeaAcumulation` float DEFAULT '0',
  `TotalAssetValue` float DEFAULT '0',
  `PosRate` float DEFAULT '0',
  `DebtAssetValue` float DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`Date`,`ProductID`,`UnitID`) USING BTREE,
  KEY `index1` (`Date`),
  KEY `index2` (`UnitID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for log_logininfo
-- ----------------------------
DROP TABLE IF EXISTS `log_logininfo`;
CREATE TABLE `log_logininfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` varchar(12) NOT NULL,
  `LoginType` char(3) NOT NULL,
  `LogoutType` char(3) DEFAULT NULL,
  `LoginDateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `LogoutDateTime` datetime DEFAULT NULL,
  `Mac` varchar(17) NOT NULL,
  `IP4` int(11) NOT NULL,
  `IP6` varbinary(16) DEFAULT NULL,
  `Address` varchar(40) NOT NULL,
  `ClientType` char(3) DEFAULT NULL,
  `OSType` char(3) DEFAULT NULL,
  `ClientVersion` char(3) DEFAULT NULL,
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`UserID`,`LoginDateTime`,`IP4`) USING BTREE,
  KEY `index1` (`LoginDateTime`),
  KEY `index2` (`IP4`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for obj_classcode
-- ----------------------------
DROP TABLE IF EXISTS `obj_classcode`;
CREATE TABLE `obj_classcode` (
  `ClassCode` char(3) NOT NULL,
  `UnderlyingCode` varchar(20) NOT NULL,
  `ClassName` varchar(40) NOT NULL,
  `ReMark` varchar(90) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ClassCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for obj_classcodeatrr
-- ----------------------------
DROP TABLE IF EXISTS `obj_classcodeatrr`;
CREATE TABLE `obj_classcodeatrr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ClassCode` char(3) NOT NULL,
  `MarketCode` char(3) NOT NULL,
  `UnderlyingCode` varchar(20) DEFAULT '',
  `UpDownLimit` float DEFAULT '0.1' COMMENT '10%',
  `ContractSize` int(11) DEFAULT NULL,
  `TradeType` char(3) DEFAULT '1',
  `ClearType` char(3) DEFAULT '1',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ClassCode`,`MarketCode`,`UnderlyingCode`) USING BTREE,
  KEY `index1` (`UnderlyingCode`),
  KEY `index2` (`MarketCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for obj_etf
-- ----------------------------
DROP TABLE IF EXISTS `obj_etf`;
CREATE TABLE `obj_etf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ETFID` varchar(20) NOT NULL,
  `Obj` varchar(20) NOT NULL,
  `PrimaryObj` varchar(20) DEFAULT NULL,
  `CashObj` varchar(20) DEFAULT NULL,
  `Name` varchar(30) DEFAULT NULL,
  `MarketCode` char(3) DEFAULT NULL,
  `FundManagementCompany` varchar(30) DEFAULT NULL,
  `UnderlyingIndex` varchar(20) DEFAULT NULL,
  `TradeUnit` int(11) DEFAULT '1',
  `EstimateCashCompoment` float DEFAULT '0',
  `MaxCashRatio` float DEFAULT '0',
  `Publish` char(3) DEFAULT NULL,
  `Creation` char(3) DEFAULT NULL,
  `Redemption` char(3) DEFAULT NULL,
  `RecordeNum` int(11) DEFAULT NULL,
  `ListingDate` date NOT NULL,
  `CashComponent` float DEFAULT '0',
  `NavPercu` float DEFAULT '0',
  `Nav` float DEFAULT '0',
  `DividendPercu` float DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ETFID`,`MarketCode`,`ListingDate`) USING BTREE,
  KEY `index1` (`Obj`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for obj_etflist
-- ----------------------------
DROP TABLE IF EXISTS `obj_etflist`;
CREATE TABLE `obj_etflist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ETFID` varchar(20) NOT NULL,
  `ListingDate` date NOT NULL,
  `RegionID` char(3) DEFAULT NULL,
  `Obj` varchar(20) NOT NULL,
  `MarketCode` char(3) NOT NULL,
  `ClassCode` char(3) NOT NULL,
  `Qty` int(11) DEFAULT NULL,
  `CanCashRepl` char(3) DEFAULT '1',
  `CashBuffer` float DEFAULT '0',
  `CashAmount` float DEFAULT '0',
  `ReMark` varchar(60) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ETFID`,`Obj`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for obj_exrightinfo
-- ----------------------------
DROP TABLE IF EXISTS `obj_exrightinfo`;
CREATE TABLE `obj_exrightinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `RegionID` char(3) NOT NULL,
  `Obj` varchar(20) NOT NULL,
  `MarketCode` char(3) NOT NULL,
  `ClassCode` char(3) NOT NULL,
  `ExRightDate` date NOT NULL,
  `RecordeDate` date NOT NULL,
  `CheckInRightDate` date NOT NULL,
  `CheckInProfitsDate` date NOT NULL,
  `Denominator` int(11) DEFAULT '0',
  `MemberA` int(11) DEFAULT '0',
  `MemberB` int(11) DEFAULT '0',
  `Profits` float DEFAULT '0',
  `ReMark` varchar(60) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`Obj`,`MarketCode`,`ClassCode`,`ExRightDate`) USING BTREE,
  KEY `index1` (`ExRightDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for obj_index
-- ----------------------------
DROP TABLE IF EXISTS `obj_index`;
CREATE TABLE `obj_index` (
  `IndexID` int(11) NOT NULL AUTO_INCREMENT,
  `IndexObj` varchar(20) NOT NULL,
  `ListingDate` date NOT NULL,
  `IndexName` varchar(30) NOT NULL,
  `RegionID` char(3) DEFAULT 'C',
  `MarketCode` char(3) NOT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`IndexID`),
  UNIQUE KEY `unique_index` (`IndexObj`,`MarketCode`,`ListingDate`) USING BTREE,
  KEY `index1` (`MarketCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for obj_indexlist
-- ----------------------------
DROP TABLE IF EXISTS `obj_indexlist`;
CREATE TABLE `obj_indexlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IndexID` int(11) NOT NULL,
  `IndexObj` varchar(20) NOT NULL,
  `Obj` varchar(20) NOT NULL,
  `RegionID` char(3) DEFAULT 'C',
  `MarketCode` char(3) NOT NULL,
  `ClassCode` char(3) NOT NULL,
  `Qty` int(11) NOT NULL,
  `Rate` float NOT NULL,
  `ListingDate` date NOT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`IndexID`,`Obj`,`MarketCode`,`ClassCode`,`ListingDate`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for obj_info
-- ----------------------------
DROP TABLE IF EXISTS `obj_info`;
CREATE TABLE `obj_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ObjID` varchar(24) NOT NULL,
  `RegionID` char(3) NOT NULL,
  `Obj` varchar(20) NOT NULL,
  `MarketCode` char(3) NOT NULL,
  `ClassCode` char(3) NOT NULL,
  `Name` varchar(60) NOT NULL,
  `EName` varchar(20) NOT NULL,
  `LongName` varchar(40) DEFAULT '',
  `ListingDate` date DEFAULT NULL,
  `FaceValue` int(11) DEFAULT NULL,
  `TradeUnit` int(11) DEFAULT NULL,
  `Currency` char(3) DEFAULT 'RMB',
  `ContractSize` int(11) DEFAULT NULL,
  `TradeType` char(3) DEFAULT '1',
  `ClearType` char(3) DEFAULT '1',
  `ContractMonth` char(6) DEFAULT NULL,
  `ExpirationDate` date DEFAULT NULL,
  `UnderlyingCode` varchar(20) DEFAULT NULL,
  `Underlyingobj` varchar(20) DEFAULT NULL,
  `First` varchar(20) DEFAULT NULL,
  `Secend` varchar(20) DEFAULT NULL,
  `Third` varchar(20) DEFAULT NULL,
  `PutCall` char(3) DEFAULT NULL,
  `StrikePrice` float DEFAULT NULL,
  `OptionRate` float DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`RegionID`,`Obj`,`MarketCode`,`ClassCode`) USING BTREE,
  KEY `index1` (`Obj`),
  KEY `index2` (`ClassCode`),
  KEY `index3` (`ListingDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for obj_newstock
-- ----------------------------
DROP TABLE IF EXISTS `obj_newstock`;
CREATE TABLE `obj_newstock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Obj` varchar(20) NOT NULL,
  `PrimaryObj` varchar(20) NOT NULL,
  `Name` varchar(24) NOT NULL,
  `RegionID` char(3) DEFAULT 'C',
  `MarketCode` char(3) NOT NULL,
  `ClassCode` char(3) NOT NULL,
  `TradeUnit` int(11) NOT NULL,
  `MaxQty` int(11) NOT NULL,
  `PrimaryTradeDate` date NOT NULL,
  `TradeDate` date NOT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`Obj`,`MarketCode`,`ClassCode`) USING BTREE,
  KEY `index1` (`PrimaryTradeDate`),
  KEY `index2` (`TradeDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for obj_pricetick
-- ----------------------------
DROP TABLE IF EXISTS `obj_pricetick`;
CREATE TABLE `obj_pricetick` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ClassCode` char(3) NOT NULL,
  `MarketCode` char(3) NOT NULL,
  `UnderlyingCode` varchar(20) DEFAULT '',
  `LowerRange` float DEFAULT '0',
  `UpperRange` float DEFAULT '9000000',
  `Tick` float DEFAULT '0.01',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ClassCode`,`MarketCode`,`UnderlyingCode`,`LowerRange`,`UpperRange`) USING BTREE,
  KEY `index1` (`UnderlyingCode`),
  KEY `index2` (`ClassCode`),
  KEY `index3` (`LowerRange`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for obj_protfolio
-- ----------------------------
DROP TABLE IF EXISTS `obj_protfolio`;
CREATE TABLE `obj_protfolio` (
  `ProtfolioID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(24) DEFAULT NULL,
  `IsPublic` char(3) DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ProtfolioID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for obj_protfoliolist
-- ----------------------------
DROP TABLE IF EXISTS `obj_protfoliolist`;
CREATE TABLE `obj_protfoliolist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ProtfolioID` int(11) NOT NULL,
  `Obj` varchar(20) NOT NULL,
  `MarketCode` char(3) NOT NULL,
  `ClassCode` char(3) NOT NULL,
  `RegionID` char(3) DEFAULT 'C',
  `Qty` int(11) DEFAULT '0',
  `Rate` float DEFAULT '0',
  `ReMark` varchar(60) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ProtfolioID`,`Obj`,`MarketCode`,`ClassCode`) USING BTREE,
  KEY `index1` (`Obj`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for obj_structuredfund_base
-- ----------------------------
DROP TABLE IF EXISTS `obj_structuredfund_base`;
CREATE TABLE `obj_structuredfund_base` (
  `Obj` varchar(20) NOT NULL,
  `Name` varchar(24) NOT NULL,
  `MarketCode` char(3) NOT NULL,
  `FundManagementCompany` varchar(30) DEFAULT '',
  `UnderlyingIndex` varchar(20) DEFAULT '',
  `tradingdate` date DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Obj`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for obj_structuredfund_part
-- ----------------------------
DROP TABLE IF EXISTS `obj_structuredfund_part`;
CREATE TABLE `obj_structuredfund_part` (
  `Obj` varchar(20) NOT NULL,
  `BaseObj` varchar(20) NOT NULL,
  `MarketCode` char(3) NOT NULL,
  `Type` char(3) DEFAULT NULL,
  `Rate` float NOT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Obj`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for obj_underlyingcode
-- ----------------------------
DROP TABLE IF EXISTS `obj_underlyingcode`;
CREATE TABLE `obj_underlyingcode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `UnderlyingCode` varchar(20) NOT NULL,
  `RegionID` char(3) DEFAULT '',
  `MarketCode` char(3) DEFAULT '',
  `ClassCode` char(3) DEFAULT '',
  `UnderlyingName` varchar(24) DEFAULT NULL,
  `Currency` char(3) DEFAULT NULL,
  `Tick` float DEFAULT NULL,
  `TradeUnit` int(11) NOT NULL,
  `ContractSize` int(11) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`UnderlyingCode`,`RegionID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for product_account
-- ----------------------------
DROP TABLE IF EXISTS `product_account`;
CREATE TABLE `product_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ProductID` int(11) NOT NULL,
  `UnitID` int(11) NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `Net` float DEFAULT NULL,
  `Share` bigint(20) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `BelongGroupID` int(11) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ProductID`,`UnitID`) USING BTREE,
  KEY `index1` (`StartDate`),
  KEY `index2` (`EndDate`),
  KEY `index3` (`BelongGroupID`),
  KEY `index4` (`OwnUserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for product_info
-- ----------------------------
DROP TABLE IF EXISTS `product_info`;
CREATE TABLE `product_info` (
  `ProductID` int(11) NOT NULL AUTO_INCREMENT,
  `ProductName` varchar(30) NOT NULL,
  `Net` float DEFAULT NULL,
  `Share` bigint(20) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `BelongGroupID` int(11) DEFAULT NULL,
  `Type` char(3) DEFAULT NULL,
  `ReMark` varchar(60) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ProductID`),
  KEY `index1` (`StartDate`),
  KEY `index2` (`EndDate`),
  KEY `index3` (`BelongGroupID`),
  KEY `index4` (`OwnUserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for product_unit
-- ----------------------------
DROP TABLE IF EXISTS `product_unit`;
CREATE TABLE `product_unit` (
  `UnitID` int(11) NOT NULL AUTO_INCREMENT,
  `ProductID` int(11) NOT NULL,
  `UnitName` varchar(30) NOT NULL,
  `Net` float DEFAULT NULL,
  `Share` bigint(20) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `BelongGroupID` int(11) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`UnitID`),
  KEY `index1` (`StartDate`),
  KEY `index2` (`EndDate`),
  KEY `index3` (`BelongGroupID`),
  KEY `index4` (`OwnUserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for risk_ctrlassets
-- ----------------------------
DROP TABLE IF EXISTS `risk_ctrlassets`;
CREATE TABLE `risk_ctrlassets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CtrlAssetsCode` varchar(20) NOT NULL,
  `Obj` varchar(20) NOT NULL,
  `IsSingleObj` char(3) NOT NULL,
  `CalcDirect` char(3) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`CtrlAssetsCode`,`IsSingleObj`,`Obj`) USING BTREE,
  KEY `index1` (`IsSingleObj`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for risk_index
-- ----------------------------
DROP TABLE IF EXISTS `risk_index`;
CREATE TABLE `risk_index` (
  `IndexCode` varchar(20) NOT NULL,
  `IndexName` varchar(30) DEFAULT NULL,
  `TypeA` char(3) DEFAULT NULL,
  `TypeB` char(3) DEFAULT NULL,
  `TypeC` char(3) DEFAULT NULL,
  `ReMark` varchar(200) DEFAULT NULL,
  `Unit` varchar(10) DEFAULT NULL,
  `IsHaveThreshold` char(3) DEFAULT NULL,
  `ThresholdNum` smallint(6) DEFAULT NULL,
  `ThresholdSplitSig` char(3) DEFAULT NULL,
  `AssetsScope` char(3) DEFAULT NULL,
  `CanNumerator` char(3) DEFAULT NULL,
  `CanDenominator` char(3) DEFAULT NULL,
  `LevelType` char(3) DEFAULT NULL,
  `IsRateOrAbs` char(3) DEFAULT NULL,
  `IsForceDone` char(3) DEFAULT NULL,
  `SceneType` char(3) DEFAULT NULL,
  `CaseShow` text,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`IndexCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for risk_sets
-- ----------------------------
DROP TABLE IF EXISTS `risk_sets`;
CREATE TABLE `risk_sets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `RiskSetCode` varchar(35) NOT NULL,
  `IndexCode` varchar(20) DEFAULT NULL,
  `CtrlLevel` char(3) DEFAULT NULL,
  `CtrlObject` varchar(20) DEFAULT NULL,
  `CtrlAssetsCode` varchar(20) DEFAULT NULL,
  `OwnGroupID` int(11) DEFAULT NULL,
  `CtrlScene` char(3) DEFAULT NULL,
  `IsForceIntercept` char(3) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `GateValueA` float DEFAULT NULL,
  `GateValueB` float DEFAULT NULL,
  `GateValueC` float DEFAULT NULL,
  `GateDirectionA` char(3) DEFAULT NULL,
  `GateDirectionB` char(3) DEFAULT NULL,
  `GateDirectionC` char(3) DEFAULT NULL,
  `GateDirectionN` varchar(200) DEFAULT NULL,
  `GateValueN` varchar(200) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`RiskSetCode`) USING BTREE,
  KEY `index1` (`IndexCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for safe_forbidlogin
-- ----------------------------
DROP TABLE IF EXISTS `safe_forbidlogin`;
CREATE TABLE `safe_forbidlogin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Mac` varchar(17) NOT NULL,
  `IP4` int(11) NOT NULL,
  `UserID` varchar(12) NOT NULL,
  `ReMark` varchar(60) NOT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`UserID`,`Mac`,`IP4`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for safe_otheraccess
-- ----------------------------
DROP TABLE IF EXISTS `safe_otheraccess`;
CREATE TABLE `safe_otheraccess` (
  `UserID` varchar(12) NOT NULL,
  `MaxOnline` int(6) DEFAULT '5',
  `LevelPrice` char(3) DEFAULT '1',
  `Dev` char(3) DEFAULT '0',
  `Reseach` char(3) DEFAULT '0',
  `ReBack` char(3) DEFAULT '0',
  `Trade` char(3) DEFAULT '0',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tradeset_broker
-- ----------------------------
DROP TABLE IF EXISTS `tradeset_broker`;
CREATE TABLE `tradeset_broker` (
  `BrokerID` varchar(10) NOT NULL,
  `RegionID` char(3) NOT NULL,
  `Type` char(3) DEFAULT NULL,
  `MarketCodeIss` varchar(48) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`BrokerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tradeset_calendar
-- ----------------------------
DROP TABLE IF EXISTS `tradeset_calendar`;
CREATE TABLE `tradeset_calendar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `RegionID` char(3) NOT NULL,
  `Date` date NOT NULL,
  `IsTrade` char(3) NOT NULL,
  `ReMark` varchar(60) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`RegionID`,`Date`) USING BTREE,
  KEY `index1` (`IsTrade`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tradeset_exchangelimit
-- ----------------------------
DROP TABLE IF EXISTS `tradeset_exchangelimit`;
CREATE TABLE `tradeset_exchangelimit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `MarketCode` char(3) NOT NULL,
  `UnderlyingObj` varchar(20) DEFAULT '',
  `CancelLimit` int(11) NOT NULL,
  `OrderLimit` int(11) NOT NULL,
  `OnceMaxQty` int(11) NOT NULL,
  `ReMark` varchar(60) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`MarketCode`,`UnderlyingObj`) USING BTREE,
  KEY `index1` (`UnderlyingObj`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tradeset_pip
-- ----------------------------
DROP TABLE IF EXISTS `tradeset_pip`;
CREATE TABLE `tradeset_pip` (
  `PipID` int(11) NOT NULL,
  `PipName` varchar(48) DEFAULT NULL,
  `Type` char(3) DEFAULT 'S',
  `RegionID` char(3) DEFAULT '',
  `MarketCode` char(3) DEFAULT '',
  `ClassCode` char(3) DEFAULT '',
  `BrokerID` varchar(10) DEFAULT '',
  `LimitA` int(11) DEFAULT NULL,
  `LimitB` int(11) DEFAULT NULL,
  `LimitC` float DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`PipID`),
  KEY `index1` (`BrokerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trade_alterorder
-- ----------------------------
DROP TABLE IF EXISTS `trade_alterorder`;
CREATE TABLE `trade_alterorder` (
  `OrderID` int(11) NOT NULL,
  `DealerUserID` varchar(12) NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `Time` time NOT NULL,
  `TradeDate` date NOT NULL,
  `DateTime` datetime NOT NULL,
  `Mac` varchar(17) DEFAULT NULL,
  `IP4` int(11) DEFAULT NULL,
  `OrderTypeEmit` char(3) NOT NULL,
  `TradeOperate` int(11) NOT NULL,
  `EntrustNo` int(11) DEFAULT NULL,
  `Entrustkey` varchar(35) DEFAULT NULL,
  `AlterQty` int(11) DEFAULT NULL,
  `AlterPrice` float DEFAULT NULL,
  `WorkStationID` char(3) DEFAULT '',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`OrderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trade_cancelorder
-- ----------------------------
DROP TABLE IF EXISTS `trade_cancelorder`;
CREATE TABLE `trade_cancelorder` (
  `OrderID` int(11) NOT NULL,
  `DealerUserID` varchar(12) NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `Time` time NOT NULL,
  `TradeDate` date NOT NULL,
  `DateTime` datetime NOT NULL,
  `Mac` varchar(17) DEFAULT NULL,
  `IP4` int(11) DEFAULT NULL,
  `OrderTypeEmit` char(3) NOT NULL,
  `TradeOperate` int(11) NOT NULL,
  `EntrustNo` int(11) DEFAULT NULL,
  `Entrustkey` varchar(35) DEFAULT NULL,
  `CancelQty` int(11) DEFAULT NULL,
  `WorkStationID` char(3) DEFAULT '',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`OrderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trade_executeorder
-- ----------------------------
DROP TABLE IF EXISTS `trade_executeorder`;
CREATE TABLE `trade_executeorder` (
  `OrderID` int(11) NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `DealerUserID` varchar(12) NOT NULL,
  `SubmitTime` time NOT NULL,
  `TradeDate` date NOT NULL,
  `DateTime` datetime NOT NULL,
  `Mac` varchar(17) DEFAULT NULL,
  `IP4` int(11) DEFAULT NULL,
  `OrderTypeEmit` char(3) NOT NULL,
  `TradeOperate` int(11) NOT NULL,
  `MarketCode` char(3) DEFAULT NULL,
  `ClassCode` char(3) DEFAULT NULL,
  `Obj` varchar(20) DEFAULT NULL,
  `BS` char(3) DEFAULT NULL,
  `OpenClose` char(3) DEFAULT NULL,
  `HedgeFlag` char(3) DEFAULT NULL,
  `PosID` varchar(35) DEFAULT NULL,
  `CustomCode` varchar(35) DEFAULT NULL,
  `CloseType` char(3) DEFAULT NULL,
  `BasketID` int(11) DEFAULT '-1',
  `BasketTatol` int(11) DEFAULT NULL,
  `SeqNum` int(11) DEFAULT NULL,
  `ExeTime` time DEFAULT NULL,
  `ExeSequenceNo` int(11) NOT NULL,
  `OutsideExeNo` varchar(35) DEFAULT NULL,
  `ExeQty` int(11) DEFAULT NULL,
  `ExePrice` float DEFAULT NULL,
  `Currency` char(3) DEFAULT NULL,
  `ExeAmount` float DEFAULT NULL,
  `CancelQty` int(11) DEFAULT NULL,
  `OrderQty` int(11) DEFAULT NULL,
  `WorkingQty` int(11) DEFAULT NULL,
  `CompleteQty` int(11) DEFAULT NULL,
  `OrderStatus` char(3) DEFAULT NULL,
  `TotalFare` float DEFAULT '0',
  `CommissionFare` float DEFAULT '0',
  `DeliverFare` float DEFAULT '0',
  `Tax` float DEFAULT '0',
  `OtherCost` float DEFAULT '0',
  `EsCheckinAmount` double DEFAULT '0',
  `ReMark` varchar(200) DEFAULT NULL,
  `InvestID` varchar(12) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`OrderID`),
  KEY `index1` (`AccountCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trade_fundtransfer
-- ----------------------------
DROP TABLE IF EXISTS `trade_fundtransfer`;
CREATE TABLE `trade_fundtransfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `RegionID` char(3) DEFAULT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `Direct` char(3) NOT NULL,
  `Amount` double(10,6) NOT NULL,
  `Date` date NOT NULL,
  `Time` time DEFAULT NULL,
  `DateTime` datetime DEFAULT NULL,
  `Currency` char(3) DEFAULT NULL,
  `Checker` varchar(36) DEFAULT NULL,
  `ReMark` varchar(256) DEFAULT NULL,
  `IsEffectived` char(3) NOT NULL,
  `ReplyMsg` varchar(256) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `index1` (`AccountCode`),
  KEY `index2` (`DateTime`),
  KEY `index3` (`Direct`),
  KEY `index4` (`IsEffectived`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trade_neworder
-- ----------------------------
DROP TABLE IF EXISTS `trade_neworder`;
CREATE TABLE `trade_neworder` (
  `OrderID` int(11) NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `DealerUserID` varchar(12) NOT NULL,
  `SubmitTime` time NOT NULL,
  `TradeDate` date NOT NULL,
  `DateTime` datetime NOT NULL,
  `Mac` varchar(17) DEFAULT NULL,
  `IP4` int(11) DEFAULT NULL,
  `OrderTypeEmit` char(3) NOT NULL,
  `TradeOperate` int(11) NOT NULL,
  `Obj` varchar(20) NOT NULL,
  `MarketCode` char(3) DEFAULT NULL,
  `ClassCode` char(3) DEFAULT NULL,
  `BS` char(3) DEFAULT NULL,
  `OpenClose` char(3) DEFAULT NULL,
  `PriceType` char(3) DEFAULT NULL,
  `OrderPrice` float DEFAULT NULL,
  `OrderQty` int(11) DEFAULT NULL,
  `HedgeFlag` char(3) DEFAULT NULL,
  `PosID` varchar(35) DEFAULT NULL,
  `CustomCode` varchar(35) DEFAULT NULL,
  `CloseType` char(3) DEFAULT NULL,
  `BasketID` int(11) DEFAULT '-1',
  `BasketTatol` int(11) DEFAULT NULL,
  `SeqNum` int(11) DEFAULT NULL,
  `WorkStationID` char(3) DEFAULT '',
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`OrderID`),
  KEY `index1` (`AccountCode`),
  KEY `index2` (`BasketID`),
  KEY `index3` (`Obj`),
  KEY `index4` (`PosID`),
  KEY `index5` (`SubmitTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trade_reportdetail
-- ----------------------------
DROP TABLE IF EXISTS `trade_reportdetail`;
CREATE TABLE `trade_reportdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `MsgType` char(2) DEFAULT NULL,
  `OrderID` int(11) NOT NULL,
  `ReportNo` int(11) NOT NULL,
  `ReportContent` text,
  `EntrustNo` int(11) DEFAULT NULL,
  `Entrustkey` varchar(35) DEFAULT NULL,
  `WorkingQty` int(11) DEFAULT NULL,
  `UnWorkingQty` int(11) DEFAULT NULL,
  `CancelQty` int(11) DEFAULT NULL,
  `ExeQtyTatol` int(11) DEFAULT NULL,
  `ExePriceAvg` float DEFAULT NULL,
  `ExeAmountTatol` double DEFAULT NULL,
  `Price` float DEFAULT '0',
  `ExexSeq` int(11) NOT NULL,
  `ExeSequenceNo` int(11) NOT NULL,
  `OutsideExeNo` varchar(35) DEFAULT NULL,
  `ExeQty` int(11) DEFAULT NULL,
  `ExePrice` float DEFAULT NULL,
  `Currency` char(3) DEFAULT NULL,
  `ExeAmount` float DEFAULT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `MarketCode` char(3) DEFAULT NULL,
  `ClassCode` char(3) DEFAULT NULL,
  `Obj` varchar(20) DEFAULT NULL,
  `BS` char(3) DEFAULT NULL,
  `OpenClose` char(3) DEFAULT NULL,
  `HedgeFlag` char(3) DEFAULT NULL,
  `CloseType` char(3) DEFAULT NULL,
  `ExeTime` time DEFAULT NULL,
  `EntrustTime` time DEFAULT NULL,
  `SubmitTime` time DEFAULT NULL,
  `TradeDate` date NOT NULL,
  `InvestID` varchar(12) DEFAULT NULL,
  `ReMark` varchar(200) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`TradeDate`,`MsgType`,`OrderID`,`ExexSeq`,`ReportNo`) USING BTREE,
  KEY `index1` (`AccountCode`),
  KEY `index2` (`OrderID`),
  KEY `index3` (`Obj`),
  KEY `index4` (`ExeTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `trade_sim_orderquene`;
CREATE TABLE `trade_sim_orderquene` (
  `OrderID` int(11) NOT NULL,
  `ordertype` char(3) DEFAULT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `DealerUserID` varchar(12) NOT NULL,
  `Time` time NOT NULL,
  `TradeDate` date NOT NULL,
  `DateTime` datetime NOT NULL,
  `Mac` varchar(17) DEFAULT NULL,
  `IP4` int(11) DEFAULT NULL,
  `Obj` varchar(20) NOT NULL,
  `MarketCode` char(3) DEFAULT NULL,
  `ClassCode` char(3) DEFAULT NULL,
  `BS` char(3) DEFAULT NULL,
  `OpenClose` char(3) DEFAULT NULL,
  `PriceType` char(3) DEFAULT NULL,
  `OrderPrice` float DEFAULT NULL,
  `OrderQty` int(11) DEFAULT NULL,
  `HedgeFlag` char(3) DEFAULT NULL,
  `PosID` varchar(35) DEFAULT NULL,
  `ExexSeq` int(11) DEFAULT 0,
  `WorkingQty` int(11) DEFAULT 0,
  `UnWorkingQty` int(11) DEFAULT 0,
  `CancelQty` int(11) DEFAULT 0,
  `ExeQty` int(11) DEFAULT 0,
  PRIMARY KEY (`OrderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- ----------------------------
-- Table structure for user_access
-- ----------------------------
DROP TABLE IF EXISTS `user_access`;
CREATE TABLE `user_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` varchar(12) NOT NULL,
  `AccessUserID` varchar(12) NOT NULL,
  `AccessID` char(3) DEFAULT '1',
  `ReMark` varchar(60) DEFAULT NULL,
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`UserID`,`AccessUserID`) USING BTREE,
  KEY `index1` (`AccessUserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user_account
-- ----------------------------
DROP TABLE IF EXISTS `user_account`;
CREATE TABLE `user_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` varchar(12) NOT NULL,
  `AccountCode` varchar(16) NOT NULL,
  `AccessID` char(3) DEFAULT '1',
  `ReMark` varchar(60) DEFAULT NULL,
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`UserID`,`AccountCode`) USING BTREE,
  KEY `index1` (`AccountCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` varchar(12) NOT NULL,
  `UserName` varchar(30) NOT NULL,
  `Pwd` varchar(48) NOT NULL,
  `Stat` char(3) DEFAULT '1',
  `Type` char(3) DEFAULT '1',
  `LoginStat` char(3) DEFAULT '0',
  `CorrectNum` int(11) DEFAULT '0',
  `IsUSBKey` char(3) DEFAULT '0',
  `RoleID` char(3) DEFAULT '1',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user_privateinfo
-- ----------------------------
DROP TABLE IF EXISTS `user_privateinfo`;
CREATE TABLE `user_privateinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` varchar(12) NOT NULL,
  `UserName` varchar(30) NOT NULL,
  `SfID` varchar(48) NOT NULL,
  `AlterUserID` varchar(12) DEFAULT NULL,
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for work_flowpath
-- ----------------------------
DROP TABLE IF EXISTS `work_flowpath`;
CREATE TABLE `work_flowpath` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` int(11) NOT NULL,
  `AnchorCode` char(3) NOT NULL,
  `FlowSeqNo` int(11) NOT NULL,
  `UserID` varchar(12) NOT NULL,
  `RoleID` char(3) NOT NULL,
  `ReMark` varchar(60) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`GroupID`,`AnchorCode`,`FlowSeqNo`,`UserID`,`RoleID`) USING BTREE,
  KEY `index1` (`AnchorCode`,`FlowSeqNo`,`UserID`),
  KEY `index2` (`UserID`,`FlowSeqNo`,`AnchorCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for work_group
-- ----------------------------
DROP TABLE IF EXISTS `work_group`;
CREATE TABLE `work_group` (
  `GroupID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupName` varchar(48) DEFAULT NULL,
  `ManagerUserID` varchar(12) DEFAULT NULL,
  `Type` char(3) DEFAULT NULL,
  `UpGroupID` int(11) DEFAULT NULL,
  `Flag` char(3) DEFAULT '1',
  `Role` char(3) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`GroupID`),
  KEY `index1` (`ManagerUserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for work_groupuser
-- ----------------------------
DROP TABLE IF EXISTS `work_groupuser`;
CREATE TABLE `work_groupuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` int(11) NOT NULL,
  `UserID` varchar(12) NOT NULL,
  `RoleID` char(3) NOT NULL,
  `ReMark` varchar(60) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`GroupID`,`UserID`) USING BTREE,
  KEY `index1` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for work_productflow
-- ----------------------------
DROP TABLE IF EXISTS `work_productflow`;
CREATE TABLE `work_productflow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ProductID` int(11) NOT NULL,
  `AnchorCode` char(3) NOT NULL,
  `FlowSeqNo` int(11) NOT NULL,
  `UserID` varchar(12) NOT NULL,
  `RoleID` char(3) NOT NULL,
  `BlongGroupID` int(11) NOT NULL,
  `ReMark` varchar(60) DEFAULT NULL,
  `OwnUserID` varchar(12) DEFAULT '',
  `AlterUserID` varchar(12) DEFAULT '',
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `TimeStamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`ProductID`,`AnchorCode`,`FlowSeqNo`,`UserID`,`RoleID`) USING BTREE,
  KEY `index1` (`AnchorCode`,`FlowSeqNo`,`UserID`),
  KEY `index2` (`UserID`,`FlowSeqNo`,`AnchorCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
