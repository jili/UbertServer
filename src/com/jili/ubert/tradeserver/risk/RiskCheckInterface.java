/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.tradeserver.risk;

import com.jili.ubert.dao.db.AlterOrder;
import com.jili.ubert.code.client2server.BasketCancelOrders;
import com.jili.ubert.code.client2server.BasketOrders;
import com.jili.ubert.dao.db.CancelOrder;
import com.jili.ubert.dao.db.NewOrder;
import com.jili.ubert.code.server2client.MsgResult;

/**
 *
 * @author ChengJiLi
 */
public interface RiskCheckInterface {
    public MsgResult Run(RiskSet riskSet,NewOrder order);
    public MsgResult Run(RiskSet riskSet,BasketOrders basketOrders);
    public MsgResult Run(RiskSet riskSet,CancelOrder cancelOrder);
    public MsgResult Run(RiskSet riskSet,BasketCancelOrders bCancelorders);
    public MsgResult Run(RiskSet riskSet,AlterOrder alterOrder);
}
