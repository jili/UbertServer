/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.tool;

import com.jili.ubert.dao.db.NewOrder;
import java.util.HashMap;

/**
 *
 * @author ChengJili
 */
public class TradeTool {
    public static final HashMap<Integer,String> typeCode = new HashMap<>();
    static {
        typeCode.put(1, "普通买入");
        typeCode.put(2, "普通卖出");
        typeCode.put(3, "新股申购");
        typeCode.put(4, "申购");
        typeCode.put(5, "赎回");
        typeCode.put(6, "拆分");
        typeCode.put(7, "合并");
        typeCode.put(8, "转股");
        typeCode.put(9, "分红方式");
        typeCode.put(10, "债券质押入库");
        typeCode.put(11, "质押债券出库");
        typeCode.put(12, "质押式正回购");
        typeCode.put(13, "质押式逆回购");
        typeCode.put(14, "买断式正回购");
        typeCode.put(15, "买断式逆回购");
        typeCode.put(16, "履约申报");
        typeCode.put(17, "融资买入");
        typeCode.put(18, "融券卖出");
        typeCode.put(19, "卖券还款");
        typeCode.put(20, "买券还券");
        typeCode.put(21, "直接还款");
        typeCode.put(22, "直接还券");
        typeCode.put(23, "备兑证券锁定");
        typeCode.put(24, "备兑证券解锁");
        typeCode.put(25, "备兑开仓");
        typeCode.put(26, "备兑平仓");
        typeCode.put(27, "买入开仓");
        typeCode.put(28, "卖出平仓");
        typeCode.put(29, "卖出开仓");
        typeCode.put(30, "卖出平仓");
        typeCode.put(31, "行权");
        typeCode.put(32, "买入开仓");
        typeCode.put(33, "卖出开仓");
        typeCode.put(34, "卖出平仓");
        typeCode.put(35, "买入平仓");
        
        typeCode.put(250, "无效指令");
        
    }
    public static TradeTypeEntity getTradeTypeEntity(String bs,String oc,String cs){
        TradeTypeEntity type = new TradeTypeEntity();
        int t = getTradeType(bs,oc,cs);
        type.tradeType=t;
        type.tradeTypeName=typeCode.get(t);
        return type;
    }
    public static TradeTypeEntity getTradeTypeEntity(NewOrder order){
        TradeTypeEntity type = new TradeTypeEntity();
        int t = getTradeType(order);
        type.tradeType=t;
        type.tradeTypeName=typeCode.get(t);
        return type;
    }
    public static TradeTypeEntity getTradeTypeEntity(char bs,char oc,String cs){
        TradeTypeEntity type = new TradeTypeEntity();
        int t = getTradeType(bs,oc,cs);
        type.tradeType=t;
        type.tradeTypeName=typeCode.get(t);
        return type;
    }
    public static int getTradeType(char bs,char oc,String cs){
        return getTradeType(String.valueOf(bs) ,String.valueOf(oc),cs);
    }
    public static int getTradeType(String bs,String oc,String cs){
        int rst=250;
        switch (oc){
            case "0":{rst= bs.equals("B")? 1:2; break;}
            case "N":{rst=3; break;}
            case "1":{rst= bs.equals("B")? 4:5; break;}
            case "2":{rst= bs.equals("B")? 6:7; break;}
            case "3":{rst= bs.equals("B")? 11:10; break;}
            case "4":{rst= bs.equals("B")? 12:13; break;}
            case "5":{rst= bs.equals("B")? 14:15; break;}
            case "6":{rst= bs.equals("B")? 17:18; break;}
            case "7":{rst= bs.equals("B")? 20:19; break;}
            case "8":{rst= bs.equals("B")? 21:22; break;}
            case "9":{rst= bs.equals("B")? 24:23; break;}
            case "A":{rst= bs.equals("B")? 26:25; break;}
            case "L":{rst= bs.equals("B")? 27:28; break;}
            case "S":{rst= bs.equals("B")? 30:29; break;}
            case "O":{rst= bs.equals("B")? 32:33; break;}
            case "C":{rst= bs.equals("B")? 35:34; break;}
            case "R":{rst= 31; break;}
            case "T":{rst= 8; break;}
            case "M":{rst= 9; break;}
        }
        
        return rst;
    }
    public static int getTradeType(NewOrder order){
        return getTradeType(order.getBs(),order.getOpenClose(),order.getClassCode());
    }
    public static void DealOrderTradeType(NewOrder order){
        int ty = getTradeType(order);
        order.setTradeOperate(ty);
    }
}
