/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.tool;

import java.util.Random;



/**
 *
 * @author ChengJiLi
 */
public class VerifyCodeGenerator {
    static Random random=new Random();
    private static final String[] str={"0","1","2","3","4","5","6","7","8","9",
       "a","b","c","d","e","f","g","h","i","j",
       "k","l","m","n","p","q","r","s","t"};
    private static String text = "";
  //随机产生4个字符的字符串
    public static String GenerateVerifyCode(){
        for(int i=0;i<4;i++){
            text+=str[random.nextInt(36)];
        }
        return text;
    }
}
