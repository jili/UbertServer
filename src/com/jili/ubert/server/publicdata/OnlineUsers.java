/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.publicdata;

import io.netty.channel.Channel;

/**
 *
 * @author ChengJiLi
 */
public class OnlineUsers {
    public int ip4;
    public String userid;
    public String mac;
    private Channel channel;
    public int getIP4(){
        return ip4;
    }
    public void setIp4(int ip){
        this.ip4=ip;
    }
    public String getUserID(){
        return userid;
    }
    public void setUserID(String userid){
        this.userid = userid;
    }
    public Channel getChannel() {
        return channel;
    }
    public void setChannel(Channel channel) {
        this.channel = channel;
    }
}
