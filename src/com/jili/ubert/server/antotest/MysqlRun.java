/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.antotest;

import com.jili.ubert.dao.DaoBaseMysql;
import com.jili.ubert.dao.db.OrderExecuteProgress;
import com.jili.ubert.dao.history.HistoryLogLoginInfo;
import com.jili.ubert.dao.recent.RecentTradeExecuteOrder;
import com.jili.ubert.dao.trace.TraceSafeForbidLogin;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 *
 * @author ChengJiLi
 */
public class MysqlRun {
    private static final Log log = LogFactory.getLog(MysqlRun.class);
    public static void main(String[] args) {
        GenList ls = new GenList();
     //   HibernatePersistenceProvider
    //   /* 
        EntityManagerFactory factory=Persistence.createEntityManagerFactory("ubert_db");
        DaoBaseMysql<OrderExecuteProgress> db = new DaoBaseMysql(OrderExecuteProgress.class,factory);
    //    db.update(ls.genOrderExecuteProgressList(1000));
        
        EntityManagerFactory factory1=Persistence.createEntityManagerFactory("ubert_history_db");
        DaoBaseMysql<HistoryLogLoginInfo> db1 = new DaoBaseMysql(HistoryLogLoginInfo.class,factory1);
        db1.insert(ls.genHistoryLogLoginInfoList(1000));
        EntityManagerFactory factory2=Persistence.createEntityManagerFactory("ubert_trace_db");
        DaoBaseMysql<TraceSafeForbidLogin> db2 = new DaoBaseMysql(TraceSafeForbidLogin.class,factory2);
        db2.insert(ls.genTraceSafeForbidLoginList(1000));
        EntityManagerFactory factory3=Persistence.createEntityManagerFactory("ubert_recent_db");
        DaoBaseMysql<RecentTradeExecuteOrder> db3 = new DaoBaseMysql(RecentTradeExecuteOrder.class,factory3);
        db3.insert(ls.genRecentTradeExecuteOrderList(1000));
    //    */
      
 //   DBHandler db = new DBHandler();
//    Session ss = db.getOpenSession();
       
    
 //   ss.beginTransaction();
/*
Transaction tt = ss.beginTransaction();
    for (OrderExecuteProgress oo:ls.genOrderExecuteProgressList(100)){
        ss.merge(oo);
    }
    tt.commit();
    log.info("完成");
  */
/*
    Criteria cr= ss.createCriteria(OrderExecuteProgress.class);
    cr.add(Restrictions.eq("id", 1));
     java.util.List ls1 = cr.list();
     log.info("结果："+JSON.toJSONString(ls1));
*/
    
    }
}
