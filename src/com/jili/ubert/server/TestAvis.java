/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server;

import com.alibaba.fastjson.JSON;
import com.jili.ubert.code.client2server.custom.BankIntoInvest;
import com.jili.ubert.code.client2server.custom.TestObjectJProtobuf;
import java.util.Arrays;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.avis.client.Elvin;
import org.avis.client.Notification;
import org.avis.client.NotificationEvent;
import org.avis.client.NotificationListener;
import org.avis.client.Subscription;

/**
 *
 * @author ChengJiLi
 */
public class TestAvis {
    private static final Log log = LogFactory.getLog(TestAvis.class);
    public static void main(String[] arg) throws Exception{
            
      String elvinUri = System.getProperty ("elvin", "elvin://localhost");
        log.info("启动连接avis router:"+elvinUri);
      // create a client that listens for messages with a "Greeting" field
      Elvin listeningClient = new Elvin(elvinUri);
        listeningClient.closeOnExit();
      final Subscription greetingSubscription;
        greetingSubscription = listeningClient.subscribe ("From == 'PriceServer'");

      greetingSubscription.addListener (new NotificationListener ()
      {
        public void notificationReceived (NotificationEvent msg)
        {
          // show the greeting
            log.info("From:"+msg.notification.getString("From")+" seq:"+msg.notification.getInt("Seq")+"  Data:"+msg.notification.get("Data"));
            log.info(msg);

          // notify the waiting main thread that we got the message
         
        }
      });

        // create a client that sends a greeting
        Elvin sendingClient = new Elvin(elvinUri);
        sendingClient.closeOnExit();
        Notification greeting = new Notification();
    //    BankIntoInvest data = new BankIntoInvest("sf", "wer564fg", "123456");
        BankIntoInvest data = new BankIntoInvest("sfsf");
        TestObjectJProtobuf ts = new TestObjectJProtobuf("22", data);
        ts.dd = 234555555;
        ts.fd = 114566666666666666666666666666666656666.4436654f;
        ts.dcd = 106.145;
        byte[] dd = ts.EnCode(ts);
        log.info(Arrays.toString(dd));
        greeting.set("Greeting", "Hello World!");
        greeting.set("Data", dd);
        greeting.set("DataJson", JSON.toJSONString(ts));
        log.info("start");
        log.info("JSON-String:" + JSON.toJSONString(ts));
        greeting.set("Data0", data.EnCode(data));
        log.info("Data0:"+Arrays.toString(data.EnCode(data)));
        greeting.set("DataJson0", JSON.toJSONString(data));
        log.info("DataJson0:"+JSON.toJSONString(data));
        sendingClient.send(greeting);
      
    }
}
