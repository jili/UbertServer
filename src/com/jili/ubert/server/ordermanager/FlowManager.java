/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.ordermanager;

import com.jili.ubert.dao.db.AlterOrder;
import com.jili.ubert.code.client2server.BasketCancelOrders;
import com.jili.ubert.code.client2server.BasketOrders;
import com.jili.ubert.dao.db.CancelOrder;
import com.jili.ubert.code.client2server.CheckAdvise;
import com.jili.ubert.dao.db.NewOrder;

/**
 *
 * @author ChengJiLi
 */
class FlowManager {
    private NewOrder order;
    private BasketOrders basketOrders;
    private CancelOrder cancelOrder;
    private BasketCancelOrders bCancelorders;
    private AlterOrder alterOrder;
    public FlowManager(){}
    public FlowManager(NewOrder order){
        this.order = order;
    }
    public FlowManager(BasketOrders basketOrders){
        this.basketOrders = basketOrders;
    }
    public FlowManager(CancelOrder cancelOrder){
        this.cancelOrder = cancelOrder;
    }
    public FlowManager(BasketCancelOrders bCancelorders){
        this.bCancelorders = bCancelorders;
    }
    public FlowManager(AlterOrder alterOrder){
        this.alterOrder = alterOrder;
    }
    public void Accept(NewOrder order){
        //1.发送交易审批通知
    }
    public void Accept(BasketOrders basketOrders){
        //1.发送交易审批通知
    }
    public void Accept(CancelOrder cancelOrder){
        //1.发送交易审批通知
    }
    public void Accept(BasketCancelOrders bCancelorders){
        //1.发送交易审批通知
    }
    public void Accept(AlterOrder alterOrder){
        //1.发送交易审批通知
    }
    public void OnCheckAdvise(CheckAdvise advsie){
        /*
        1.判断，是否需要过风控
        2.不过风控广播委托回报   
        3.是否需要风控出发审批
        */
        
    }
    public void OnRiskAdvise(CheckAdvise advsie){
        /*
        1.风控出发审批
        2.不过通过广播委托回报，通过下单        
        */
        
    }
}
