/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.ordermanager;

import com.jili.ubert.code.ReturnIDEntity;
import com.jili.ubert.dao.db.AlterOrder;
import com.jili.ubert.code.client2server.BasketCancelOrders;
import com.jili.ubert.code.client2server.BasketOrders;
import com.jili.ubert.dao.db.CancelOrder;
import com.jili.ubert.code.client2server.CheckAdvise;
import com.jili.ubert.dao.db.NewOrder;
import com.jili.ubert.code.server2client.MsgResult;
import com.jili.ubert.code.server2client.MsgResultList;
import com.jili.ubert.dao.db.AccountInfo;
import com.jili.ubert.server.adapter.AdapterTrade;
import com.jili.ubert.server.datapool.DataPoolTrade;
import com.jili.ubert.server.publicdata.PublicData;
import com.jili.ubert.server.tradecore.TradeCoreLib;
import java.util.HashMap;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author ChengJiLi
 */
public class OrderManager {

    private RiskManager RiskManager;
    private HashMap<Integer, FlowManager> FlowManagerinstaneTable;
    private DataPoolTrade dataPool;
    private HashMap<Integer, AdapterTrade> adapterTradelines;
    private PublicData publicData;
    private TradeCoreLib tradeCoreLib;
    private HashMap<String, AccountInfo> accountInfoTable = new HashMap<>();//accouncode,pipID

    /*
     1.调用资源管理类检查：验资验券
     2.判断流程：0-直接下单；1-直接过风控；2-走业务控制流程；暂未考虑指令执行。
     订单管理的唯一入口；只有Send;
     */
    public OrderManager() {
    }

    public MsgResult Send(NewOrder order) {
        MsgResult rst = tradeCoreLib.CheckOrder(order);
        if (rst.isSuccess()) {
            AccountInfo accinfo = getAccountInfo(order.getAccountCode());
            String type = accinfo.getIsRisk();
            int pid = accinfo.getPipID();
            if (type.equals("0")) {
                //调用资源类下单接口，直接下单
                adapterTradelines.get(pid).Send(order);
            } else if (type.equals("1")) {
                RiskManager.Check(order);
            } else {
                FlowManager flowmaneger = new FlowManager(order);
                FlowManagerinstaneTable.put(order.getOrderID(), flowmaneger);
                flowmaneger.Accept(order);
            }
        }
        return rst;
    }

    public MsgResult Send(CancelOrder cancelorder) {
        AccountInfo accinfo = getAccountInfo(cancelorder.getAccountCode());
        String type = accinfo.getIsRisk();
        int pid = accinfo.getPipID();
        adapterTradelines.get(pid).Send(cancelorder);
        return ReturnIDEntity.Success;
    }

    public MsgResultList Send(BasketOrders basketorders) {
        MsgResultList rst = tradeCoreLib.CheckOrder(basketorders);
        byte type = 0;
        if (type == 0) {
            //调用资源类下单接口，直接下单
        //    resouce.Deliver(basketorders);
        } else if (type == 1) {
            RiskManager.Check(basketorders);
        } else {
            FlowManager flowmaneger = new FlowManager(basketorders);
            FlowManagerinstaneTable.put(basketorders.getBasketId(), flowmaneger);
            flowmaneger.Accept(basketorders);
        }

        return rst;
    }

    public MsgResultList Send(BasketCancelOrders bcancleorders) {
        MsgResultList rst = null;

        return rst;
    }

    public MsgResult Send(AlterOrder alterorder) {
        MsgResult rst = tradeCoreLib.CheckOrder(alterorder);
        if (rst.isSuccess()) {
            AccountInfo accinfo = getAccountInfo(alterorder.getAccountCode());
            String type = accinfo.getIsRisk();
            int pid = accinfo.getPipID();
            if (type.equals("0")) {
                //调用资源类下单接口，直接下单
                adapterTradelines.get(pid).Send(alterorder);
            } else if (type.equals("1")) {
                RiskManager.Check(alterorder);
            } else {
                FlowManager flowmaneger = new FlowManager(alterorder);
                FlowManagerinstaneTable.put(alterorder.getOrderID(), flowmaneger);
                flowmaneger.Accept(alterorder);
            }
        }
        return rst;
    }

    public void OnTradeCheckAdvise(CheckAdvise advise) {
        //    if (FlowManagerinstaneTable.containsKey(advise.getID())) {
        FlowManagerinstaneTable.get(advise.getID()).OnCheckAdvise(advise);
        //    }
    }

    public void OnRiskCheckAdvise(CheckAdvise advise) {
        FlowManagerinstaneTable.get(advise.getID()).OnRiskAdvise(advise);
    }

    public void setDataPool(DataPoolTrade dataPool) {
        this.dataPool = dataPool;
    }

    public void setAdapterTradelines(HashMap<Integer, AdapterTrade> adapterTradelines) {
        this.adapterTradelines = adapterTradelines;
    }

    public void setPublicData(PublicData publicData) {
        this.publicData = publicData;
    }

    public void setTradeCoreLib(TradeCoreLib tradeCoreLib) {
        this.tradeCoreLib = tradeCoreLib;
    }

    private AccountInfo getAccountInfo(String acc) {
        if (accountInfoTable.containsKey(acc)) {
            return accountInfoTable.get(acc);
        } else {
            EntityManager em = dataPool.getDBFactory().createEntityManager();
            Query query = em.createNamedQuery("AccountInfo.findByAccountCode", AccountInfo.class);
            query.setParameter(1, acc);
            AccountInfo acctable = (AccountInfo) query.getSingleResult();
            accountInfoTable.put(acc, acctable);
            return acctable;
        }
    }
}
