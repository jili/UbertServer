/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.adapter;

import com.jili.ubert.code.server2client.Price;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.avis.client.NotificationEvent;
import org.avis.client.NotificationListener;

/**
 *
 * @author ChengJili
 */
public class AcceptPrice  implements NotificationListener {

    private static final Log log = LogFactory.getLog(AcceptPrice.class);
    private OnPrice onprice;

    public AcceptPrice(OnPrice onprice) {
        this.onprice = onprice;
    }

    @Override
    public void notificationReceived(NotificationEvent ne) {
        try {
            //解析行情
            log.debug("Title:" + ne.notification.getString("Title")
                    + "PipID:" + ne.notification.getInt("PipID")
                    + " Data:" + ne.notification.get("Data"));
            byte[] dd = ne.notification.getOpaque("Data");
            Price price = Price.DeCode(dd);
            onprice.OnPrice(price);
        } catch (IOException ex) {
            Logger.getLogger(AcceptPrice.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
