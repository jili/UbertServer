/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.adapter;

import com.alibaba.fastjson.JSON;
import com.jili.ubert.dao.db.TradeReportDetail;
import com.jili.ubert.server.tradecore.TradeCoreLib;
import java.io.IOException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.avis.client.NotificationEvent;
import org.avis.client.NotificationListener;

/**
 *
 * @author ChengJiLi
 */
public class AcceptReport implements NotificationListener {

    private static final Log log = LogFactory.getLog(AcceptReport.class);
    private TradeCoreLib tradecorelib;

    public AcceptReport(TradeCoreLib tradecorelib) {
        this.tradecorelib = tradecorelib;
    }

    @Override
    public void notificationReceived(NotificationEvent ne) {
        log.info("Title:" + ne.notification.getString("Title")
                + "PipID:" + ne.notification.getInt("PipID")
                + " Data:" + ne.notification.get("Data"));
        byte[] dd = ne.notification.getOpaque("Data");

        try {
            TradeReportDetail report = TradeReportDetail.DeCode(dd);
            String str = "";
            /*
             1.委托回报：MsgType:C1：下单回报，C2:撤单回报,C3:改价回报
             1)模拟成交：OR:订单委托回报，OS:订单成交回报；CR:撤单委托回报，CS:撤单成交回报；AR改单委托回报，AS：改单成交回报；OO：其他指令单回报
             2）内部成交：IS:内部成交回报，IC：内部撤单回报，IA: 内部改单回报；注意，内部成交不需要委托确认，直接处理结果
             3）内部移仓：PS：内部移仓成交回报
             4）状态修订：ST:状态修复回报
             2.多态处理，接收3种委托
             type:1.模拟成交，2.内部成交，3.内部移仓，4.状态修改；OrderType只在type为1的时候有效
             */
            tradecorelib.OnTradeReport(report);
            switch (report.getMsgType()) {
                case "OR": {
                    str = "订单委托回报,挂：" + report.getWorkingQty();
                    break;
                }
                case "OS": {
                    str = "订单成交回报,成交数：" + report.getExeQty() + ",成交价：" + report.getExePrice() + " ,挂单：" + report.getWorkingQty();
                    break;
                }
                case "CR": {
                    str = "撤单委托回报";
                    break;
                }
                case "CS": {
                    str = "撤单成交回报；撤单数：" + report.getCancelQty();
                    break;
                }
                case "AR": {
                    str = "改单委托回报";
                    break;
                }
                case "AS": {
                    str = "改单成交回报";
                    break;
                }
                case "IS": {
                    str = "内部成交回报";
                    break;
                }
                case "IA": {
                    str = "内部改单回报";
                    break;
                }
                case "IR": {
                    str = "内部委托回报";
                    break;
                }
            }
            if (report.getReportNo() == 1) {
                str = str + "-成功";
            } else {
                String ss = new String(report.getReportContent());
                str = str + "-失败-原因：" + ss;
            }
            log.info("订单号：orderID:" + report.getOrderID() + " " + str + "  数据明细data:" + JSON.toJSONString(report));
        } catch (IOException ex) {
            log.error(ex);
        }

    }
    
}
