/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.adapter;

import com.alibaba.fastjson.JSON;
import com.jili.ubert.dao.db.AlterOrder;
import com.jili.ubert.code.client2server.BasketCancelOrders;
import com.jili.ubert.code.client2server.BasketOrders;
import com.jili.ubert.dao.db.CancelOrder;
import com.jili.ubert.dao.db.NewOrder;
import com.jili.ubert.dao.db.TradesetPip;
import com.jili.ubert.server.publicdata.PublicData;
import com.jili.ubert.server.tradecore.TradeCoreLib;
import java.io.IOException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.avis.client.Elvin;
import org.avis.client.Notification;
import org.avis.client.NotificationListener;
import org.avis.client.Subscription;

/**
 *
 * @author ChengJiLi
 */
public final class AdapterTrade {

    private static Log log = LogFactory.getLog(AdapterTrade.class);
    private  Elvin elvin;

    public void setElvin(Elvin aElvin) {
        this.elvin = aElvin;
    }
    private TradeCoreLib tradecorelib;//交易核心库，接收回调
    private int pid;
    private TradesetPip pipInfo;

    //发送订单到接口；
    /*
     1.打包成bute[]；调用内部构建发送
     2.内部构建消息打包打包发送端
     3.外部传来资源：1.账户信息，资金账号密码------放在资金账号认证中，adapater内存保留；重启时效，重新认证（server重发吧）,code,brookeID,营业部码，PiPID，2.avis服务器资源，默认本机
     */
    public AdapterTrade(TradesetPip pipInfo) {
        this.pid = pipInfo.getPipID();
        this.pipInfo = pipInfo;
    }

    public void ListenToReport(TradeCoreLib tradecorelib) {
        this.setTradecorelib(tradecorelib);
        try {
            //    final Subscription orderSubscription = elvin.subscribe("Title == 'Report' && PipID == 0");
            final Subscription orderSubscription = elvin.subscribe("Title == 'Report'");
            NotificationListener orderListener = new AcceptReport(tradecorelib);
            orderSubscription.addListener(orderListener);
        } catch (Exception e) {
            log.info(e);
        }
    }

    public void Send(NewOrder order) {
        log.debug(JSON.toJSON(order));
        try {
            SendMsg(order.EnCode(order), 1, pid);
        } catch (IOException ex) {
            log.error(ex);
        }
    }

    public void Send(BasketOrders basketOrders) {
        log.debug(JSON.toJSON(basketOrders));
        try {
            SendMsg(basketOrders.EnCode(basketOrders), 2, pid);
        } catch (IOException ex) {
            log.error(ex);
        }

    }

    public void Send(CancelOrder cancelOrder) {
        log.debug(JSON.toJSON(cancelOrder));
        try {
            SendMsg(cancelOrder.EnCode(cancelOrder), 4, pid);
        } catch (IOException ex) {
            log.error(ex);
        }
    }

    public void Send(BasketCancelOrders bCancelorders) {
        int pid = 0;
        log.debug(JSON.toJSON(bCancelorders));
        bCancelorders.getCancelOrders().stream().forEach(this::Send);
    }

    public void Send(AlterOrder alterOrder) {
        log.debug(JSON.toJSON(alterOrder));
        try {
            SendMsg(alterOrder.EnCode(alterOrder), 3, pid);
        } catch (IOException ex) {
            log.error(ex);
        }
    }

    public void InnerExe(NewOrder order) {
        log.debug(JSON.toJSON(order));
        try {
            SendMsg(order.EnCode(order), 6, 0);
        } catch (IOException ex) {
            log.error(ex);
        }
    }

    public void InnerExe(CancelOrder cancelOrder) {
        log.debug(JSON.toJSON(cancelOrder));
        try {
            SendMsg(cancelOrder.EnCode(cancelOrder), 8, 0);
        } catch (IOException ex) {
            log.error(ex);
        }
    }

    public void InnerExe(AlterOrder alterOrder) {
        log.debug(JSON.toJSON(alterOrder));
        try {
            SendMsg(alterOrder.EnCode(alterOrder), 7, 0);
        } catch (IOException ex) {
            log.error(ex);
        }
    }

    public void PosAdjust() {

    }

    public void OrderStatusAlter() {

    }

    private void SendMsg(byte[] dd, int orderType, int pipID) {
        //发送委托
        Notification order0 = new Notification();
        order0.set("Title", "Trade");
        order0.set("PipID", pipID);
        order0.set("OrderType", orderType);
        order0.set("Data", dd);
        //发送结束
        try {
            order0.set("Data", dd);
            elvin.send(order0);
        } catch (IOException ex) {
            log.error(ex);
        }
    }

    public void setTradecorelib(TradeCoreLib tradecorelib) {
        this.tradecorelib = tradecorelib;
    }

}
