/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.adapter;

import com.jili.ubert.server.priceserver.PriceServer;
import com.jili.ubert.server.tradecore.TradeCoreLib;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.avis.client.Elvin;
import org.avis.client.NotificationListener;
import org.avis.client.Subscription;

/**
 *
 * @author ChengJiLi
 */
public final class AdapterPrice {

    private static Log log = LogFactory.getLog(AdapterPrice.class);
    private static Elvin elvin;
    private OnPrice onprice;//交易核心库，接收回调

    //发送订单到接口；
    /*
     1.打包成bute[]；调用内部构建发送
     2.内部构建消息打包打包发送端
     3.外部传来资源：1.账户信息，资金账号密码------放在资金账号认证中，adapater内存保留；重启时效，重新认证（server重发吧）,code,brookeID,营业部码，PiPID，2.avis服务器资源，默认本机
     */
    public AdapterPrice(Elvin elvin) {
        AdapterPrice.elvin = elvin;
    }
    public void ListenPrice(OnPrice onprice) {
        this.onprice = onprice;
        try {
            //    final Subscription orderSubscription = elvin.subscribe("Title == 'Report' && PipID == 0");
            final Subscription orderSubscription = elvin.subscribe("Title == 'Report'");
            NotificationListener priceHandle = new AcceptPrice(onprice);
            orderSubscription.addListener(priceHandle);
        } catch (Exception e) {
            log.info(e);
        }
    }
    
    public void ListenPrice(String regionID,OnPrice onprice) {
        this.onprice = onprice;
        try {
            //    final Subscription orderSubscription = elvin.subscribe("Title == 'Report' && PipID == 0");
            final Subscription orderSubscription = elvin.subscribe("Title == 'Report'");
            NotificationListener priceHandle = new AcceptPrice(onprice);
            orderSubscription.addListener(priceHandle);
        } catch (Exception e) {
            log.info(e);
        }
    }
}
