/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server;

import com.alibaba.fastjson.JSON;
import com.jili.ubert.code.OrderBS;
import com.jili.ubert.code.client2server.custom.BankIntoInvest;
import com.jili.ubert.code.client2server.custom.TestObjectJProtobuf;
import com.jili.ubert.code.server2client.MsgResult;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Arrays;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.util.Date;

/**
 *
 * @author ChengJiLi
 */
public class TestJProtobuf {
    private static final Log log = LogFactory.getLog(TestJProtobuf.class);
    public static void main(String[] arg) throws IOException {
        OrderBS bs = OrderBS.DeCode("B");
        if (bs==OrderBS.B){
            System.out.println("相等"+bs);
        }
        else{
            System.out.println("不相等"+bs);
        }
        MsgResult rst = new MsgResult();
        rst.setRemark("我是吉利");
        rst.setBig(BigDecimal.valueOf(12333.98276652252525));
        System.out.println(JSON.toJSONString(rst));
        System.out.println(JSON.toJSONString(rst.getBitremark()));
        byte[] aa=rst.EnCode(rst);
        MsgResult rst2= new MsgResult();
        rst2=rst.DeCode(aa);
        System.out.println(JSON.toJSONString(rst2));
        System.out.println(JSON.toJSONString(rst2.getBitremark()));
        Date d1 = Date.from(Instant.MIN);
        long dssw = 123339889;
        Date d2=new Date(dssw);
        d1.getTime();
        
    }
}
