/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.priceserver;

import com.jili.ubert.code.client2server.RegistPriceMsg;
import com.jili.ubert.code.server2client.Price;
import com.jili.ubert.server.adapter.OnPrice;
import com.jili.ubert.server.publicdata.PublicData;
import io.netty.channel.Channel;

/**
 *
 * @author ChengJiLi
 */
public class PriceServer implements OnPrice {
    private PublicData publicData;
    private DeliveryBucket deliveryBucket;
    
    public PriceServer(){
        deliveryBucket = new DeliveryBucket();
    }
    public void OnPrice(Price price) {
        deliveryBucket.OnDealListen(price);
    }

    public void dealRegister(RegistPriceMsg rp,Channel channel) {
        String mk = rp.getMarketCode();
        rp.getObjList().stream().forEach((String o)->{deliveryBucket.Register(mk.concat(o), channel);});
        
    }

    public void dealCancelRegister(RegistPriceMsg rp,Channel channel) {
        String mk = rp.getMarketCode();
        rp.getObjList().stream().forEach((String o)->{deliveryBucket.CancelRegister(mk.concat(o), channel);});
    }
    public void dealCancelRegister(Channel channel) {
        deliveryBucket.CancelRegister(channel);
    }

    public void setPublicData(PublicData publicData) {
        this.publicData = publicData;
    }
}
