/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.manager;

import com.jili.ubert.server.datapool.DataPoolTrade;
import com.jili.ubert.server.publicdata.PublicData;

/**
 *
 * @author ChengJiLi
 */
public class ManagerServer {
    private  DataPoolTrade dataPool;
    private PublicData publicData;
    public ManagerServer(DataPoolTrade dataPool){
        this.dataPool = dataPool;
    }

    public void setPublicData(PublicData publicData) {
        this.publicData = publicData;
    }
}
