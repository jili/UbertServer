/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.resourcemanager;

import com.alibaba.fastjson.JSON;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.c3p0.internal.C3P0ConnectionProvider;

/**
 *
 * @author ChengJiLi
 */
public class DBConnctionProvider extends C3P0ConnectionProvider {

    private static final Log log = LogFactory.getLog(DBConnctionProvider.class);

    @Override
    public void configure(Map props) {
    //    log.debug(JSON.toJSONString(props));
        String ipport = (String) props.get("hibernate.connection.url");
        String name = (String) props.get("hibernate.connection.username");
        String password = (String) props.get("hibernate.connection.password");
        String dbtype = (String) props.get("hibernate.connection.dbtype");
        log.debug("dbtype:" + dbtype);
        log.debug("name:" + name + "  decode:" + AESLock.decrypt(name, "root54jili"));
        props.put("hibernate.connection.username", AESLock.decrypt(name, "root54jili"));
        log.debug("password:" + password + "  decode:" + AESLock.decrypt(password, "54jiliroot"));
        props.put("hibernate.connection.password", AESLock.decrypt(password, "54jiliroot"));
        String url;
        if (dbtype==null) {
            dbtype = (String) props.get("hibernate.ejb.persistenceUnitName");
        }
        url = "jdbc:mysql://" + AESLock.decrypt(ipport, "jilicheng") + "/" + dbtype + "?zeroDateTimeBehavior=convertToNull";
        props.put("hibernate.connection.url", url);
        log.debug("url:" + name + "  decode:" + url);
        super.configure(props);
    }
}
