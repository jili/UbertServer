/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.resourcemanager;

import com.jili.ubert.dao.db.TradesetPip;
import com.jili.ubert.netty.priceserver.NettyServer2Price;
import com.jili.ubert.netty.traderserver.NettyServer2Trade;
import com.jili.ubert.netty.otherserver.NettyServer2Other;
import com.jili.ubert.server.adapter.AdapterPrice;
import com.jili.ubert.server.adapter.AdapterTrade;
import com.jili.ubert.server.datapool.DataPoolHistory;
import com.jili.ubert.server.datapool.DataPoolPrice;
import com.jili.ubert.server.datapool.DataPoolTrade;
import com.jili.ubert.server.historyserver.HistoryServer;
import com.jili.ubert.server.manager.ManagerServer;
import com.jili.ubert.server.manager.ServerManager;
import com.jili.ubert.server.manager.SystemManager;
import com.jili.ubert.server.monitor.MonitorManager;
import com.jili.ubert.server.monitor.MonitorServer;
import com.jili.ubert.server.ordermanager.OrderManager;
import com.jili.ubert.server.priceserver.PriceServer;
import com.jili.ubert.server.publicdata.PublicData;
import com.jili.ubert.server.tradecore.TradeCoreLib;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author ChengJili
 */
public class ResourceServer {

    private static final Log log = LogFactory.getLog(ResourceServer.class);
    private final Properties props;
    private String runtype;
//    private static 
    /*
    模块方法：公共模块，数据池，交易核心库，订单管理，管理服务，交易接口适配器，行情接口适配器，监控服务，netty服务组，历史数据服务，行情服务
    判断模式启动不同组件；
    对象生成工厂，能实例化时处理关系的实例化就处理关系，不能的放在start中
     */
    private PublicData publicdata;
    private DataPoolTrade dataPoolTrade;
    private DataPoolPrice dataPoolPrice;
    private DataPoolHistory dataPoolHistory;
    private TradeCoreLib tradeCoreLib;
    private OrderManager orderManager;
    private ManagerServer managerServer;
    private ServerManager serverManager;
    private SystemManager systemManager;
    private HashMap<Integer, AdapterTrade> tradeAdapterlist = new HashMap<>();
    private HashMap<String, AdapterPrice> priceAdapterlist = new HashMap<>();
    private MonitorServer monitorServer;
    private MonitorManager monitorManager;
    private PriceServer priceServer;
    private HistoryServer historyServer;
    private boolean isSSL = false;
    private boolean isZlib =false;
    
    ExecutorService exec;

    public ResourceServer(Properties props) {
        this.props = props;
        runtype = props.getProperty("Server.Run.Scheme", "0");//Server.Run.Scheme ,1:TradeServer,2.PriceServer,3.Data ,0,other为：综合模式
        log.info("配置文件运行模式：" + runtype);
        isSSL = Boolean.parseBoolean(props.getProperty("isSSL", "false"));
        isZlib = Boolean.parseBoolean(props.getProperty("isZlib", "false"));
        switch (runtype) {
            case "1": {
                dataPoolTrade = new DataPoolTrade();
                publicdata = new PublicData(props);
                publicdata.setDatapool(dataPoolTrade);
                tradeCoreLib = new TradeCoreLib(dataPoolTrade);
                orderManager = new OrderManager();
                orderManager.setDataPool(dataPoolTrade);
                orderManager.setPublicData(publicdata);
                orderManager.setTradeCoreLib(tradeCoreLib);
                EntityManager em = dataPoolTrade.getDBFactory().createEntityManager();
                Query queryuser = em.createNamedQuery("TradesetPip.findAll", TradesetPip.class);
                List<TradesetPip> userls = queryuser.getResultList();
                userls.stream().forEach((TradesetPip o) -> {
                    AdapterTrade at = new AdapterTrade(o);
                    at.setTradecorelib(tradeCoreLib);
                    at.setElvin(publicdata.getTradeElvin());
                    tradeAdapterlist.put(o.getPipID(), at);
                });
                orderManager.setAdapterTradelines(tradeAdapterlist);
                String region = props.getProperty("RegionID", "C");
                AdapterPrice ap = new AdapterPrice(publicdata.getPriceElvin());
                ap.ListenPrice(region, priceServer);
                ap.ListenPrice(region, tradeCoreLib);
                priceAdapterlist.put(region, ap);
                
                managerServer = new ManagerServer(dataPoolTrade);
                managerServer.setPublicData(publicdata);
            }
            case "2": {
                dataPoolPrice = new DataPoolPrice();
                dataPoolTrade = new DataPoolTrade();
                publicdata = new PublicData(props);
                publicdata.setDatapool(dataPoolTrade);
                String region = props.getProperty("RegionID", "C");
                AdapterPrice ap = new AdapterPrice(publicdata.getPriceElvin());
                ap.ListenPrice(region, priceServer);
                priceAdapterlist.put(region, ap);
                priceServer = new PriceServer();
                priceServer.setPublicData(publicdata);
            }
            case "3": {
                dataPoolHistory = new DataPoolHistory();
                dataPoolTrade = new DataPoolTrade();
                publicdata = new PublicData(props);
                publicdata.setDatapool(dataPoolTrade);
                historyServer = new HistoryServer(dataPoolHistory);
            }
            default: {
                dataPoolHistory = new DataPoolHistory();
                dataPoolPrice = new DataPoolPrice();
                dataPoolTrade = new DataPoolTrade();
                publicdata = new PublicData(props);
                publicdata.setDatapool(dataPoolTrade);
                tradeCoreLib = new TradeCoreLib(dataPoolTrade);
                orderManager = new OrderManager();
                orderManager.setDataPool(dataPoolTrade);
                orderManager.setPublicData(publicdata);
                orderManager.setTradeCoreLib(tradeCoreLib);
                EntityManager em = dataPoolTrade.getDBFactory().createEntityManager();
                Query queryuser = em.createNamedQuery("TradesetPip.findAll", TradesetPip.class);
                List<TradesetPip> userls = queryuser.getResultList();
                userls.stream().forEach((TradesetPip o) -> {
                    AdapterTrade at = new AdapterTrade(o);
                    at.setTradecorelib(tradeCoreLib);
                    at.setElvin(publicdata.getTradeElvin());
                    tradeAdapterlist.put(o.getPipID(), at);
                });
                orderManager.setAdapterTradelines(tradeAdapterlist);
                String region = props.getProperty("RegionID", "C");
                AdapterPrice ap = new AdapterPrice(publicdata.getPriceElvin());
                ap.ListenPrice(region, priceServer);
                ap.ListenPrice(region, tradeCoreLib);
                priceAdapterlist.put(region, ap);
                priceServer = new PriceServer();
                priceServer.setPublicData(publicdata);
                historyServer = new HistoryServer(dataPoolHistory);
                
                managerServer = new ManagerServer(dataPoolTrade);
                managerServer.setPublicData(publicdata);

            }
        }
    }

    public void Start() {//netty服务组，
        //根据配置文件，看看初始化几个线程，每个线程干什么工作

        //启动
        switch (runtype) {
            case "1": {
                exec = Executors.newFixedThreadPool(1);
                int port=Integer.valueOf(props.getProperty("Server.Trade.Port","3773"));
                NettyServer2Trade nt = new NettyServer2Trade(port,this.isSSL,this.isZlib);
                nt.setDatapool(dataPoolTrade);
                nt.setManagerServer(managerServer);
                nt.setOrderManager(orderManager);
                nt.setPublicData(publicdata);
            }
            case "2": {
                exec = Executors.newFixedThreadPool(1);
                int port1=Integer.valueOf(props.getProperty("Server.Trade.Port","3774"));
                NettyServer2Price np = new NettyServer2Price(port1);
                np.setDatapool(dataPoolPrice);
                np.setPriceServer(priceServer);
                np.setPublicData(publicdata);
            }
            case "3": {
                exec = Executors.newFixedThreadPool(1);
                int port2=Integer.valueOf(props.getProperty("Server.Trade.Port","3775"));
                NettyServer2Other nh = new NettyServer2Other(port2,isSSL,isZlib);
                nh.setDatapool(dataPoolHistory);
                nh.setPublicData(publicdata);
                nh.setServer(historyServer);
                
            }
            default: {
                exec = Executors.newFixedThreadPool(3);
                int port=Integer.valueOf(props.getProperty("Server.Trade.Port","3773"));
                NettyServer2Trade nt = new NettyServer2Trade(port,this.isSSL,this.isZlib);
                nt.setDatapool(dataPoolTrade);
                nt.setManagerServer(managerServer);
                nt.setOrderManager(orderManager);
                nt.setPublicData(publicdata);
                int port1=Integer.valueOf(props.getProperty("Server.Trade.Port","3774"));
                NettyServer2Price np = new NettyServer2Price(port1);
                np.setDatapool(dataPoolPrice);
                np.setPriceServer(priceServer);
                np.setPublicData(publicdata);
                int port2=Integer.valueOf(props.getProperty("Server.Trade.Port","3775"));
                NettyServer2Other nh = new NettyServer2Other(port2,isSSL,isZlib);
                nh.setDatapool(dataPoolHistory);
                nh.setPublicData(publicdata);
                nh.setServer(historyServer);
                
            }
        }
    }
}
