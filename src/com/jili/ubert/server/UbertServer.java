/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server;

import com.jili.ubert.server.resourcemanager.ResourceServer;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author ChengJiLi
 */
public class UbertServer {

    /**
     * @param args the command line arguments
     */
    private static final Log log = LogFactory.getLog(UbertServer.class);
    public static void main(String[] args) throws FileNotFoundException, IOException {
        /*
        File file;
        
         String tt = "localhost:3306";
        
         System.out.println("mima:" + AESLock.encrypt("54jili", "54jiliroot"));
         System.out.println("name:" + AESLock.encrypt("root", "root54jili"));
         System.out.println("IPport:" + AESLock.encrypt("localhost:3306", "jilicheng"));
         */

        Properties props = new Properties();//属性集合对象   
        FileInputStream fis;
        String filename = "config\\UbertServer.properties";
        if (args.length >= 1) {
            if (!args[0].isEmpty()) {
                filename = args[0];
            }
        }
        fis = new FileInputStream(filename);//属性文件流 
        props.load(fis);//将属性文件流装载到Properties对象中 
        log.info("配置文件："+props.getProperty("Avis.Price.IP"));
        ResourceServer rs = new ResourceServer(props);
        rs.Start();

    }

}
