/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.tradecore;

import com.jili.ubert.dao.db.AccountBail;

public class Bail {

    private String marketCode;
    private String classCode;
    private String bs;
    private String underlyingCode;
    private String obj;
    private Float bail0;
    private Float bail1;
    private Float bail2;
    private Float bail0Rate;
    private Float bail1Rate;
    private Float bail2Rate;

    public Bail(AccountBail bail) {
        this.marketCode = bail.getMarketCode();
        this.classCode = bail.getClassCode();
        this.bs = bail.getBs();
        this.underlyingCode = bail.getUnderlyingCode();
        this.obj = bail.getObj();
        this.bail0 = bail.getBail0();
        this.bail1 = bail.getBail1();
        this.bail2 = bail.getBail2();
        this.bail0Rate = bail.getBail0Rate();
        this.bail1Rate = bail.getBail1Rate();
        this.bail2Rate = bail.getBail2Rate();
    }

    public String getMarketCode() {
        return marketCode;
    }

    public String getClassCode() {
        return classCode;
    }

    public String getBs() {
        return bs;
    }

    public String getUnderlyingCode() {
        return underlyingCode;
    }

    public String getObj() {
        return obj;
    }

    public Float getBail0() {
        return bail0;
    }

    public Float getBail1() {
        return bail1;
    }

    public Float getBail2() {
        return bail2;
    }

    public Float getBail0Rate() {
        return bail0Rate;
    }

    public Float getBail1Rate() {
        return bail1Rate;
    }

    public Float getBail2Rate() {
        return bail2Rate;
    }
}
