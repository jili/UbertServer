/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.tradecore;

import com.jili.ubert.dao.db.NewOrder;
import com.jili.ubert.dao.db.PosDetail;
import com.jili.ubert.dao.db.TradeReportDetail;

public class PosRecord implements POS {

    private String accountCode;
    private String posID;
    private String obj;
    private String marketCode;
    private String classCode;
    private String bs;
    private int qty;
    private int avlQty;
    private int frozeQty;
    private Float costPriceA;
    private Float costPriceB;
    private Float opencostprice;
    private Float avgCostPrice;
    private String hedgeFlag;
    private String posType;
    private String putCall;
    private String currency;
    private Float pl;
    private Float costPL;
    private Float totalPL;
    private Float margin;
    private Float totalFare;
    private String investID;

    public PosRecord() {
    }

    public PosRecord(PosDetail pos) {
        this.accountCode = pos.getAccountCode();
        this.posID = pos.getPosID();
        this.obj = pos.getObj();
        this.marketCode = pos.getMarketCode();
        this.classCode = pos.getClassCode();
        this.bs = pos.getBs();
        this.qty = pos.getQty();
        this.avlQty = pos.getAvlQty();
        this.frozeQty = pos.getFrozeQty();
        this.costPriceA = pos.getCostPriceA();
        this.costPriceB = pos.getCostPriceB();
        this.opencostprice = pos.getOpencostprice();
        this.avgCostPrice = pos.getAvgCostPrice();
        this.hedgeFlag = pos.getHedgeFlag();
        this.posType = pos.getPosType();
        this.putCall = pos.getPutCall();
        this.currency = pos.getCurrency();
        this.pl = pos.getPl();
        this.costPL = pos.getCostPL();
        this.totalPL = pos.getTotalPL();
        this.margin = pos.getMargin();
        this.totalFare = pos.getTotalFare();
    }
    public PosRecord(FrozeOrder pos) {
        this.posID = pos.getPosID();
        this.obj = pos.getObj();
        this.marketCode = pos.getMarketCode();
        this.classCode = pos.getClasscode();
        this.bs = pos.getBs().equals("B")?"S":"B";
        this.qty = 0;
        this.avlQty = 0;
        this.frozeQty = pos.getQty();
        this.hedgeFlag = pos.getHelgflag();
//        this.posType = pos.getPosType();
        this.totalFare = (float)pos.getFare();
    }
    public PosRecord(TradeReportDetail pos,NewOrder order) {
        this.accountCode = pos.getAccountCode();
        this.posID = order.getPosID();
        this.obj = pos.getObj();
        this.marketCode = pos.getMarketCode();
        this.classCode = pos.getClassCode();
        this.bs = pos.getBs();
        this.qty = 0;
        this.avlQty = 0;
        this.frozeQty = 0;
        this.hedgeFlag = pos.getHedgeFlag();
        this.currency = pos.getCurrency();
        this.investID = pos.getInvestID();
    }

    @Override
    public String getAccountCode() {
        return accountCode;
    }

    @Override
    public String getPosID() {
        return posID;
    }

    @Override
    public String getObj() {
        return obj;
    }

    @Override
    public String getMarketCode() {
        return marketCode;
    }

    @Override
    public String getClassCode() {
        return classCode;
    }

    @Override
    public String getBs() {
        return bs;
    }

    @Override
    public int getQty() {
        return qty;
    }

    @Override
    public int getAvlQty() {
        return avlQty;
    }

    @Override
    public int getFrozeQty() {
        return frozeQty;
    }

    @Override
    public Float getCostPriceA() {
        return costPriceA;
    }

    @Override
    public Float getCostPriceB() {
        return costPriceB;
    }

    @Override
    public Float getOpencostprice() {
        return opencostprice;
    }

    @Override
    public Float getAvgCostPrice() {
        return avgCostPrice;
    }

    @Override
    public String getHedgeFlag() {
        return hedgeFlag;
    }

    @Override
    public String getPosType() {
        return posType;
    }

    @Override
    public String getPutCall() {
        return putCall;
    }

    @Override
    public String getCurrency() {
        return currency;
    }

    @Override
    public Float getPl() {
        return pl;
    }

    @Override
    public Float getCostPL() {
        return costPL;
    }

    @Override
    public Float getTotalPL() {
        return totalPL;
    }

    @Override
    public Float getMargin() {
        return margin;
    }

    @Override
    public Float getTotalFare() {
        return totalFare;
    }

    @Override
    public String getInvestID() {
        return investID;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public void setPosID(String posID) {
        this.posID = posID;
    }

    public void setObj(String obj) {
        this.obj = obj;
    }

    public void setMarketCode(String marketCode) {
        this.marketCode = marketCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public void setBs(String bs) {
        this.bs = bs;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public void setAvlQty(int avlQty) {
        this.avlQty = avlQty;
    }

    public void setFrozeQty(int frozeQty) {
        this.frozeQty = frozeQty;
    }

    public void setCostPriceA(Float costPriceA) {
        this.costPriceA = costPriceA;
    }

    public void setCostPriceB(Float costPriceB) {
        this.costPriceB = costPriceB;
    }

    public void setOpencostprice(Float opencostprice) {
        this.opencostprice = opencostprice;
    }

    public void setAvgCostPrice(Float avgCostPrice) {
        this.avgCostPrice = avgCostPrice;
    }

    public void setHedgeFlag(String hedgeFlag) {
        this.hedgeFlag = hedgeFlag;
    }

    public void setPosType(String posType) {
        this.posType = posType;
    }

    public void setPutCall(String putCall) {
        this.putCall = putCall;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setPl(Float pl) {
        this.pl = pl;
    }

    public void setCostPL(Float costPL) {
        this.costPL = costPL;
    }

    public void setTotalPL(Float totalPL) {
        this.totalPL = totalPL;
    }

    public void setMargin(Float margin) {
        this.margin = margin;
    }

    public void setTotalFare(Float totalFare) {
        this.totalFare = totalFare;
    }

    public void setInvestID(String investID) {
        this.investID = investID;
    }

}
