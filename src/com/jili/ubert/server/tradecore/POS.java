/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.tradecore;

/**
 *
 * @author dragon
 */
public interface POS {

    public String getAccountCode();

    public String getPosID();

    public String getObj();

    public String getMarketCode();

    public String getClassCode();

    public String getBs();

    public int getQty();

    public int getAvlQty();

    public int getFrozeQty();

    public Float getCostPriceA();

    public Float getCostPriceB();

    public Float getOpencostprice();

    public Float getAvgCostPrice();

    public String getHedgeFlag();

    public String getPosType();

    public String getPutCall();

    public String getCurrency();

    public Float getPl();

    public Float getCostPL();

    public Float getTotalPL();

    public Float getMargin();

    public Float getTotalFare();

    public String getInvestID();
}
