/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.tradecore;

import com.jili.ubert.dao.db.AccountFare;

public class Fare {

    private String marketCode;
    private String classCode;
    private String underlyingCode;
    private String obj;
    private String bs;
    private String openClose;
    private Float commission0;//佣金率
    private Float minCommission0;//最低佣金
    private Float deliverFare0;//过户费率
    private Float tax0;//税率
    private Float cost0;//其它成本

    public Fare(AccountFare fare) {
        this.marketCode = fare.getMarketCode();
        this.classCode = fare.getClassCode();
        this.underlyingCode = fare.getUnderlyingCode();
        this.obj = fare.getObj();
        this.bs = fare.getBs();
        this.openClose = fare.getOpenClose();
        this.commission0 = fare.getCommission0();
        this.minCommission0 = fare.getMinCommission0();
        this.deliverFare0 = fare.getDeliverFare0();
        this.tax0 = fare.getTax0();
        this.cost0 = fare.getCost0();
    }

    public String getMarketCode() {
        return marketCode;
    }

    public String getClassCode() {
        return classCode;
    }

    public String getUnderlyingCode() {
        return underlyingCode;
    }

    public String getObj() {
        return obj;
    }

    public String getBs() {
        return bs;
    }

    public String getOpenClose() {
        return openClose;
    }

    public Float getCommission0() {
        return commission0;
    }

    public Float getMinCommission0() {
        return minCommission0;
    }

    public Float getDeliverFare0() {
        return deliverFare0;
    }

    public Float getTax0() {
        return tax0;
    }

    public Float getCost0() {
        return cost0;
    }

}
