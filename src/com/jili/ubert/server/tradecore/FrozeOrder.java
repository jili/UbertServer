/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.tradecore;

import com.jili.ubert.dao.db.NewOrder;

/**
 *
 * @author ChengJili
 */
class FrozeOrder {

    private int orderID;
    private String obj;
    private String marketCode;
    private String classcode;
    private String bs;
    private String op;
    private String helgflag;
    private int qty;
    private float price;
    private String pType;
    private String posID;//开仓是的持仓标志，平仓时的poskey
    private double fare;
    private double frozeFund;
    private boolean fundFroze;
//java.util.concurrent.atomic.AtomicBoolean

    /**
     * @return the orderID
     */
    public FrozeOrder(NewOrder order) {
        this.orderID = order.getOrderID();
        this.obj = order.getObj();
        this.marketCode = order.getMarketCode();
        this.classcode = order.getClassCode();
        this.bs = order.getBs();
        this.op = order.getOpenClose();
        this.helgflag = order.getHedgeFlag();
        this.qty = order.getOrderQty();
        this.price = order.getOrderPrice();
        this.pType = order.getPriceType();
        this.posID = order.getPosID();
    }

    public int getOrderID() {
        return orderID;
    }

    public String getObj() {
        return obj;
    }

    public String getMarketCode() {
        return marketCode;
    }

    public String getClasscode() {
        return classcode;
    }

    public String getBs() {
        return bs;
    }

    public String getOp() {
        return op;
    }

    public String getHelgflag() {
        return helgflag;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getpType() {
        return pType;
    }

    public String getPosID() {
        return posID;
    }

    public double getFare() {
        return fare;
    }

    public void setFare(double fare) {
        this.fare = fare;
    }

    public double getFrozeFund() {
        return frozeFund;
    }

    public void setFrozeFund(double frozeFund) {
        this.frozeFund = frozeFund;
    }

    public void setPosID(String posID) {
        this.posID = posID;
    }

    public boolean isFundFroze() {
        return fundFroze;
    }

    public void setFundFroze(boolean fundFroze) {
        this.fundFroze = fundFroze;
    }

}
