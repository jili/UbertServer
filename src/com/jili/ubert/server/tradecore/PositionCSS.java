/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.tradecore;

import com.jili.ubert.code.ReturnIDEntity;
import com.jili.ubert.code.client2server.BasketOrders;
import com.jili.ubert.code.server2client.MsgResult;
import com.jili.ubert.code.server2client.MsgResultList;
import com.jili.ubert.dao.db.AlterOrder;
import com.jili.ubert.dao.db.FundState;
import com.jili.ubert.dao.db.NewOrder;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dragon
 */
public class PositionCSS extends Position {

    @Override
    public MsgResult CheckOrder(NewOrder order) {
        return CheckOrderlmp(order);
    }

    @Override
    public MsgResultList CheckBasketOrder(BasketOrders orders) {
        MsgResultList rstlist = new MsgResultList();
        rstlist.setSuccess(true);
        List<NewOrder> orderlist = orders.getOrders();
        List<MsgResult> ls = new ArrayList<>(orders.getBasketTatol());
        orderlist.stream().forEach((NewOrder o) -> {
            MsgResult rst = CheckOrderlmp(o);
            ls.add(rst);
            if (!rst.isSuccess()) {
                rstlist.setSuccess(false);
            }
        });
        if (rstlist.isSuccess()) {
            orderlist.stream().forEach((NewOrder order) -> {
                saveOrder(order.getOrderID(), order);
            });
        } else {
            orderlist.stream().forEach((NewOrder order) -> {
                int id = order.getOrderID();
                UnFrozeOrder(id);
                super.deleteOrder(id);
            });
        }
        rstlist.setBasketOrderCheckRST(ls);
        return rstlist;
    }

    @Override
    public MsgResult CheckOrder(AlterOrder alterOrder) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setData(Fare data) {
        String key = data.getMarketCode().concat(data.getClassCode()).concat(data.getBs());
        fareTable.put(key, data);
    }

    @Override
    public void setData(Bail data) {
    }

    @Override
    public void setData(PosRecord data) {//key:obj,mk,bs,posID
        String poskey = data.getObj().concat(data.getMarketCode()).concat(data.getBs()).concat(data.getPosID());
        super.posTable.put(poskey, data);
    }

    @Override
    public void setData(FundState data) {
        super.accountCode = data.getAccountCode();
        super.fundTotal = (double)data.getFundTotal();
        super.fundAvl = (double)data.getFundAvl();
        super.fundFroze = (double)data.getFundFroze();
        super.assetTotal = (double)data.getAssetTotal();
        super.assetNet = (double)data.getAssetNet();
        super.assetCredit = (double)data.getAssetCredit();
        super.posTotalValue = (double)data.getPosTotalValue();
        super.posNetValue = (double)data.getPosNetValue();
        super.posCreditValue = (double)data.getPosCreditValue();
        super.currency = data.getCurrency();
        super.rate = data.getRate();
        super.equalFundAvl = (double)data.getEqualFundAvl();
        super.befAssetTotal = (double)data.getBefAssetTotal();
        super.befFundAvl = (double)data.getBefFundAvl();

    }

}
