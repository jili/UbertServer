/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.datapool;

import com.jili.ubert.dao.DaoBaseMysql;
import com.jili.ubert.dao.trace.TraceAccountBail;
import com.jili.ubert.dao.trace.TraceAccountFare;
import com.jili.ubert.dao.trace.TraceAccountIcode;
import com.jili.ubert.dao.trace.TraceAccountInfo;
import com.jili.ubert.dao.trace.TraceAccountInvest;
import com.jili.ubert.dao.trace.TraceBailTemp;
import com.jili.ubert.dao.trace.TraceBailTempObj;
import com.jili.ubert.dao.trace.TraceFareTemp;
import com.jili.ubert.dao.trace.TraceFareTempeObj;
import com.jili.ubert.dao.trace.TraceObjInfo;
import com.jili.ubert.dao.trace.TraceObjProtfolio;
import com.jili.ubert.dao.trace.TraceObjProtfolioList;
import com.jili.ubert.dao.trace.TraceObjUnderlying;
import com.jili.ubert.dao.trace.TraceProductAccount;
import com.jili.ubert.dao.trace.TraceProductInfo;
import com.jili.ubert.dao.trace.TraceProductUnit;
import com.jili.ubert.dao.trace.TraceRiskCtrlAssets;
import com.jili.ubert.dao.trace.TraceRiskSets;
import com.jili.ubert.dao.trace.TraceSafeForbidLogin;
import com.jili.ubert.dao.trace.TraceSafeOtherAccess;
import com.jili.ubert.dao.trace.TraceTradesetExchangeLimit;
import com.jili.ubert.dao.trace.TraceUserAccess;
import com.jili.ubert.dao.trace.TraceUserAccount;
import com.jili.ubert.dao.trace.TraceUserInfo;
import com.jili.ubert.dao.trace.TraceUserPrivateInfo;
import com.jili.ubert.dao.trace.TraceWorkFlowPath;
import com.jili.ubert.dao.trace.TraceWorkGroup;
import com.jili.ubert.dao.trace.TraceWorkGroupUser;
import com.jili.ubert.dao.trace.TraceWorkProductFlow;
import com.panemu.tiwulfx.common.TableCriteria;
import com.panemu.tiwulfx.common.TableData;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author ChengJiLi
 */
public class TraceDBProvider {
    EntityManagerFactory factory=Persistence.createEntityManagerFactory("ubert_trace_db");
    public EntityManagerFactory getFactory(){
        return factory;
    }
    private final DaoBaseMysql<TraceAccountBail> tableTraceAccountBail = new DaoBaseMysql(TraceAccountBail.class, factory);
private final DaoBaseMysql<TraceAccountFare> tableTraceAccountFare = new DaoBaseMysql(TraceAccountFare.class, factory);
private final DaoBaseMysql<TraceAccountIcode> tableTraceAccountIcode = new DaoBaseMysql(TraceAccountIcode.class, factory);
private final DaoBaseMysql<TraceAccountInfo> tableTraceAccountInfo = new DaoBaseMysql(TraceAccountInfo.class, factory);
private final DaoBaseMysql<TraceAccountInvest> tableTraceAccountInvest = new DaoBaseMysql(TraceAccountInvest.class, factory);
private final DaoBaseMysql<TraceBailTemp> tableTraceBailTemp = new DaoBaseMysql(TraceBailTemp.class, factory);
private final DaoBaseMysql<TraceBailTempObj> tableTraceBailTempObj = new DaoBaseMysql(TraceBailTempObj.class, factory);
private final DaoBaseMysql<TraceFareTemp> tableTraceFareTemp = new DaoBaseMysql(TraceFareTemp.class, factory);
private final DaoBaseMysql<TraceFareTempeObj> tableTraceFareTempeObj = new DaoBaseMysql(TraceFareTempeObj.class, factory);
private final DaoBaseMysql<TraceObjInfo> tableTraceObjInfo = new DaoBaseMysql(TraceObjInfo.class, factory);
private final DaoBaseMysql<TraceObjProtfolio> tableTraceObjProtfolio = new DaoBaseMysql(TraceObjProtfolio.class, factory);
private final DaoBaseMysql<TraceObjProtfolioList> tableTraceObjProtfolioList = new DaoBaseMysql(TraceObjProtfolioList.class, factory);
private final DaoBaseMysql<TraceObjUnderlying> tableTraceObjUnderlying = new DaoBaseMysql(TraceObjUnderlying.class, factory);
private final DaoBaseMysql<TraceProductAccount> tableTraceProductAccount = new DaoBaseMysql(TraceProductAccount.class, factory);
private final DaoBaseMysql<TraceProductInfo> tableTraceProductInfo = new DaoBaseMysql(TraceProductInfo.class, factory);
private final DaoBaseMysql<TraceProductUnit> tableTraceProductUnit = new DaoBaseMysql(TraceProductUnit.class, factory);
private final DaoBaseMysql<TraceRiskCtrlAssets> tableTraceRiskCtrlAssets = new DaoBaseMysql(TraceRiskCtrlAssets.class, factory);
private final DaoBaseMysql<TraceRiskSets> tableTraceRiskSets = new DaoBaseMysql(TraceRiskSets.class, factory);
private final DaoBaseMysql<TraceSafeForbidLogin> tableTraceSafeForbidLogin = new DaoBaseMysql(TraceSafeForbidLogin.class, factory);
private final DaoBaseMysql<TraceSafeOtherAccess> tableTraceSafeOtherAccess = new DaoBaseMysql(TraceSafeOtherAccess.class, factory);
private final DaoBaseMysql<TraceTradesetExchangeLimit> tableTraceTradesetExchangeLimit = new DaoBaseMysql(TraceTradesetExchangeLimit.class, factory);
private final DaoBaseMysql<TraceUserAccess> tableTraceUserAccess = new DaoBaseMysql(TraceUserAccess.class, factory);
private final DaoBaseMysql<TraceUserAccount> tableTraceUserAccount = new DaoBaseMysql(TraceUserAccount.class, factory);
private final DaoBaseMysql<TraceUserInfo> tableTraceUserInfo = new DaoBaseMysql(TraceUserInfo.class, factory);
private final DaoBaseMysql<TraceUserPrivateInfo> tableTraceUserPrivateInfo = new DaoBaseMysql(TraceUserPrivateInfo.class, factory);
private final DaoBaseMysql<TraceWorkFlowPath> tableTraceWorkFlowPath = new DaoBaseMysql(TraceWorkFlowPath.class, factory);
private final DaoBaseMysql<TraceWorkGroup> tableTraceWorkGroup = new DaoBaseMysql(TraceWorkGroup.class, factory);
private final DaoBaseMysql<TraceWorkGroupUser> tableTraceWorkGroupUser = new DaoBaseMysql(TraceWorkGroupUser.class, factory);
private final DaoBaseMysql<TraceWorkProductFlow> tableTraceWorkProductFlow = new DaoBaseMysql(TraceWorkProductFlow.class, factory);
public TableData<TraceAccountBail> queryTraceAccountBail(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceAccountBail.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceAccountFare> queryTraceAccountFare(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceAccountFare.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceAccountIcode> queryTraceAccountIcode(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceAccountIcode.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceAccountInfo> queryTraceAccountInfo(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceAccountInfo.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceAccountInvest> queryTraceAccountInvest(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceAccountInvest.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceBailTemp> queryTraceBailTemp(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceBailTemp.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceBailTempObj> queryTraceBailTempObj(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceBailTempObj.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceFareTemp> queryTraceFareTemp(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceFareTemp.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceFareTempeObj> queryTraceFareTempeObj(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceFareTempeObj.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceObjInfo> queryTraceObjInfo(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceObjInfo.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceObjProtfolio> queryTraceObjProtfolio(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceObjProtfolio.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceObjProtfolioList> queryTraceObjProtfolioList(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceObjProtfolioList.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceObjUnderlying> queryTraceObjUnderlying(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceObjUnderlying.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceProductAccount> queryTraceProductAccount(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceProductAccount.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceProductInfo> queryTraceProductInfo(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceProductInfo.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceProductUnit> queryTraceProductUnit(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceProductUnit.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceRiskCtrlAssets> queryTraceRiskCtrlAssets(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceRiskCtrlAssets.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceRiskSets> queryTraceRiskSets(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceRiskSets.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceSafeForbidLogin> queryTraceSafeForbidLogin(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceSafeForbidLogin.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceSafeOtherAccess> queryTraceSafeOtherAccess(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceSafeOtherAccess.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceTradesetExchangeLimit> queryTraceTradesetExchangeLimit(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceTradesetExchangeLimit.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceUserAccess> queryTraceUserAccess(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceUserAccess.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceUserAccount> queryTraceUserAccount(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceUserAccount.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceUserInfo> queryTraceUserInfo(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceUserInfo.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceUserPrivateInfo> queryTraceUserPrivateInfo(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceUserPrivateInfo.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceWorkFlowPath> queryTraceWorkFlowPath(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceWorkFlowPath.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceWorkGroup> queryTraceWorkGroup(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceWorkGroup.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceWorkGroupUser> queryTraceWorkGroupUser(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceWorkGroupUser.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<TraceWorkProductFlow> queryTraceWorkProductFlow(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableTraceWorkProductFlow.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}

}
