/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.datapool;

import com.jili.ubert.dao.DaoBaseMysql;
import com.jili.ubert.dao.recent.RecentClearingAccountBail;
import com.jili.ubert.dao.recent.RecentClearingAccountFare;
import com.jili.ubert.dao.recent.RecentClearingDayBook;
import com.jili.ubert.dao.recent.RecentClearingExRight;
import com.jili.ubert.dao.recent.RecentClearingFundState;
import com.jili.ubert.dao.recent.RecentClearingPosDetail;
import com.jili.ubert.dao.recent.RecentClearingPrice;
import com.jili.ubert.dao.recent.RecentClearingProtfoliopos;
import com.jili.ubert.dao.recent.RecentObjETF;
import com.jili.ubert.dao.recent.RecentObjETFList;
import com.jili.ubert.dao.recent.RecentObjIndex;
import com.jili.ubert.dao.recent.RecentObjIndexList;
import com.jili.ubert.dao.recent.RecentTradeAlterOrder;
import com.jili.ubert.dao.recent.RecentTradeCancelOrder;
import com.jili.ubert.dao.recent.RecentTradeExecuteOrder;
import com.jili.ubert.dao.recent.RecentTradeNewOrder;
import com.jili.ubert.dao.recent.RecentTradeReportDetail;
import com.panemu.tiwulfx.common.TableCriteria;
import com.panemu.tiwulfx.common.TableData;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author ChengJiLi
 */
public class RecentDBProvider {
    EntityManagerFactory factory=Persistence.createEntityManagerFactory("ubert_recent_db");
    private final DaoBaseMysql<RecentClearingAccountBail> tableRecentClearingAccountBail = new DaoBaseMysql(RecentClearingAccountBail.class, factory);
private final DaoBaseMysql<RecentClearingAccountFare> tableRecentClearingAccountFare = new DaoBaseMysql(RecentClearingAccountFare.class, factory);
private final DaoBaseMysql<RecentClearingDayBook> tableRecentClearingDayBook = new DaoBaseMysql(RecentClearingDayBook.class, factory);
private final DaoBaseMysql<RecentClearingExRight> tableRecentClearingExRight = new DaoBaseMysql(RecentClearingExRight.class, factory);
private final DaoBaseMysql<RecentClearingFundState> tableRecentClearingFundState = new DaoBaseMysql(RecentClearingFundState.class, factory);
private final DaoBaseMysql<RecentClearingPosDetail> tableRecentClearingPosDetail = new DaoBaseMysql(RecentClearingPosDetail.class, factory);
private final DaoBaseMysql<RecentClearingPrice> tableRecentClearingPrice = new DaoBaseMysql(RecentClearingPrice.class, factory);
private final DaoBaseMysql<RecentClearingProtfoliopos> tableRecentClearingProtfoliopos = new DaoBaseMysql(RecentClearingProtfoliopos.class, factory);
private final DaoBaseMysql<RecentObjETF> tableRecentObjETF = new DaoBaseMysql(RecentObjETF.class, factory);
private final DaoBaseMysql<RecentObjETFList> tableRecentObjETFList = new DaoBaseMysql(RecentObjETFList.class, factory);
private final DaoBaseMysql<RecentObjIndex> tableRecentObjIndex = new DaoBaseMysql(RecentObjIndex.class, factory);
private final DaoBaseMysql<RecentObjIndexList> tableRecentObjIndexList = new DaoBaseMysql(RecentObjIndexList.class, factory);
private final DaoBaseMysql<RecentTradeAlterOrder> tableRecentTradeAlterOrder = new DaoBaseMysql(RecentTradeAlterOrder.class, factory);
private final DaoBaseMysql<RecentTradeCancelOrder> tableRecentTradeCancelOrder = new DaoBaseMysql(RecentTradeCancelOrder.class, factory);
private final DaoBaseMysql<RecentTradeExecuteOrder> tableRecentTradeExecuteOrder = new DaoBaseMysql(RecentTradeExecuteOrder.class, factory);
private final DaoBaseMysql<RecentTradeNewOrder> tableRecentTradeNewOrder = new DaoBaseMysql(RecentTradeNewOrder.class, factory);
private final DaoBaseMysql<RecentTradeReportDetail> tableRecentTradeReportDetail = new DaoBaseMysql(RecentTradeReportDetail.class, factory);
public TableData<RecentClearingAccountBail> queryRecentClearingAccountBail(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableRecentClearingAccountBail.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<RecentClearingAccountFare> queryRecentClearingAccountFare(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableRecentClearingAccountFare.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<RecentClearingDayBook> queryRecentClearingDayBook(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableRecentClearingDayBook.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<RecentClearingExRight> queryRecentClearingExRight(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableRecentClearingExRight.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<RecentClearingFundState> queryRecentClearingFundState(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableRecentClearingFundState.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<RecentClearingPosDetail> queryRecentClearingPosDetail(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableRecentClearingPosDetail.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<RecentClearingPrice> queryRecentClearingPrice(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableRecentClearingPrice.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<RecentClearingProtfoliopos> queryRecentClearingProtfoliopos(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableRecentClearingProtfoliopos.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<RecentObjETF> queryRecentObjETF(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableRecentObjETF.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<RecentObjETFList> queryRecentObjETFList(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableRecentObjETFList.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<RecentObjIndex> queryRecentObjIndex(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableRecentObjIndex.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<RecentObjIndexList> queryRecentObjIndexList(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableRecentObjIndexList.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<RecentTradeAlterOrder> queryRecentTradeAlterOrder(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableRecentTradeAlterOrder.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<RecentTradeCancelOrder> queryRecentTradeCancelOrder(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableRecentTradeCancelOrder.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<RecentTradeExecuteOrder> queryRecentTradeExecuteOrder(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableRecentTradeExecuteOrder.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<RecentTradeNewOrder> queryRecentTradeNewOrder(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableRecentTradeNewOrder.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}
public TableData<RecentTradeReportDetail> queryRecentTradeReportDetail(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult){return tableRecentTradeReportDetail.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);}

}
