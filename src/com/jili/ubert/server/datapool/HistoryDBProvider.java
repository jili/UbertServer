/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.datapool;

import com.jili.ubert.dao.DaoBaseMysql;
import com.jili.ubert.dao.history.HistoryClearingCurrencyRates;
import com.jili.ubert.dao.history.HistoryIOPVAccount;
import com.jili.ubert.dao.history.HistoryIOPVProduct;
import com.jili.ubert.dao.history.HistoryIOPVProductUnit;
import com.jili.ubert.dao.history.HistoryLogLoginInfo;
import com.jili.ubert.dao.history.HistoryObjExRightInfo;
import com.jili.ubert.dao.history.HistoryTradeFundTransfer;
import com.jili.ubert.dao.history.HistoryTradesetCalendar;
import com.panemu.tiwulfx.common.TableCriteria;
import com.panemu.tiwulfx.common.TableData;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author ChengJiLi
 */
public class HistoryDBProvider {

    EntityManagerFactory factory = Persistence.createEntityManagerFactory("ubert_history_db");
    private final DaoBaseMysql<HistoryClearingCurrencyRates> tableHistoryClearingCurrencyRates = new DaoBaseMysql(HistoryClearingCurrencyRates.class, factory);
    private final DaoBaseMysql<HistoryIOPVAccount> tableHistoryIOPVAccount = new DaoBaseMysql(HistoryIOPVAccount.class, factory);
    private final DaoBaseMysql<HistoryIOPVProduct> tableHistoryIOPVProduct = new DaoBaseMysql(HistoryIOPVProduct.class, factory);
    private final DaoBaseMysql<HistoryIOPVProductUnit> tableHistoryIOPVProductUnit = new DaoBaseMysql(HistoryIOPVProductUnit.class, factory);
    private final DaoBaseMysql<HistoryLogLoginInfo> tableHistoryLogLoginInfo = new DaoBaseMysql(HistoryLogLoginInfo.class, factory);
    private final DaoBaseMysql<HistoryObjExRightInfo> tableHistoryObjExRightInfo = new DaoBaseMysql(HistoryObjExRightInfo.class, factory);
    private final DaoBaseMysql<HistoryTradeFundTransfer> tableHistoryTradeFundTransfer = new DaoBaseMysql(HistoryTradeFundTransfer.class, factory);
    private final DaoBaseMysql<HistoryTradesetCalendar> tableHistoryTradesetCalendar = new DaoBaseMysql(HistoryTradesetCalendar.class, factory);

    public TableData<HistoryClearingCurrencyRates> queryHistoryClearingCurrencyRates(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableHistoryClearingCurrencyRates.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<HistoryIOPVAccount> queryHistoryIOPVAccount(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableHistoryIOPVAccount.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<HistoryIOPVProduct> queryHistoryIOPVProduct(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableHistoryIOPVProduct.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<HistoryIOPVProductUnit> queryHistoryIOPVProductUnit(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableHistoryIOPVProductUnit.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<HistoryLogLoginInfo> queryHistoryLogLoginInfo(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableHistoryLogLoginInfo.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<HistoryObjExRightInfo> queryHistoryObjExRightInfo(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableHistoryObjExRightInfo.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<HistoryTradeFundTransfer> queryHistoryTradeFundTransfer(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableHistoryTradeFundTransfer.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<HistoryTradesetCalendar> queryHistoryTradesetCalendar(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableHistoryTradesetCalendar.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

}
