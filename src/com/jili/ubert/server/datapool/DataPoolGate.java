/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.datapool;

import com.alibaba.fastjson.JSON;
import com.jili.ubert.code.client2server.RequestPageData;
import com.jili.ubert.code.server2client.PageDataProto;
import com.panemu.tiwulfx.common.TableCriteria;
import com.panemu.tiwulfx.common.TableData;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ChengJiLi
 */
public class DataPoolGate {
    public static List<TableCriteria> decodeFilter(RequestPageData rpd) {
        return (List<TableCriteria>) JSON.parse(rpd.getFilteredColumns());
    }

    public static PageDataProto TransferPageData(RequestPageData rpd, TableData dataList) {
        int s = rpd.getStartIndex();
        long t = dataList.getTotalRows();
        int max = rpd.getMaxResult();
        PageDataProto pdp = new PageDataProto();
        pdp.setMaxNum(max);
        pdp.setTotalRows(t);
        pdp.setMoreRows(dataList.isMoreRows());
        //    pdp.setCurrentPage();
        return pdp;
    }
}
