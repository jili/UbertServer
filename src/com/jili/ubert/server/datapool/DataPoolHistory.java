/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.datapool;

import com.alibaba.fastjson.JSON;
import com.jili.ubert.code.client2server.RequestPageData;
import com.jili.ubert.code.client2server.SelectPageData;
import com.jili.ubert.code.server2client.PageDataProto;
import com.jili.ubert.dao.history.HistoryClearingCurrencyRates;
import com.jili.ubert.dao.history.HistoryIOPVAccount;
import com.jili.ubert.dao.history.HistoryIOPVProduct;
import com.jili.ubert.dao.history.HistoryIOPVProductUnit;
import com.jili.ubert.dao.history.HistoryLogLoginInfo;
import com.jili.ubert.dao.history.HistoryObjExRightInfo;
import com.jili.ubert.dao.history.HistoryTradeFundTransfer;
import com.jili.ubert.dao.history.HistoryTradesetCalendar;
import com.jili.ubert.dao.recent.RecentClearingAccountBail;
import com.jili.ubert.dao.recent.RecentClearingAccountFare;
import com.jili.ubert.dao.recent.RecentClearingDayBook;
import com.jili.ubert.dao.recent.RecentClearingExRight;
import com.jili.ubert.dao.recent.RecentClearingFundState;
import com.jili.ubert.dao.recent.RecentClearingPosDetail;
import com.jili.ubert.dao.recent.RecentClearingPrice;
import com.jili.ubert.dao.recent.RecentClearingProtfoliopos;
import com.jili.ubert.dao.recent.RecentObjETF;
import com.jili.ubert.dao.recent.RecentObjETFList;
import com.jili.ubert.dao.recent.RecentObjIndex;
import com.jili.ubert.dao.recent.RecentObjIndexList;
import com.jili.ubert.dao.recent.RecentTradeAlterOrder;
import com.jili.ubert.dao.recent.RecentTradeCancelOrder;
import com.jili.ubert.dao.recent.RecentTradeExecuteOrder;
import com.jili.ubert.dao.recent.RecentTradeNewOrder;
import com.jili.ubert.dao.recent.RecentTradeReportDetail;
import com.jili.ubert.dao.trace.TraceAccountBail;
import com.jili.ubert.dao.trace.TraceAccountFare;
import com.jili.ubert.dao.trace.TraceAccountIcode;
import com.jili.ubert.dao.trace.TraceAccountInfo;
import com.jili.ubert.dao.trace.TraceAccountInvest;
import com.jili.ubert.dao.trace.TraceBailTemp;
import com.jili.ubert.dao.trace.TraceBailTempObj;
import com.jili.ubert.dao.trace.TraceFareTemp;
import com.jili.ubert.dao.trace.TraceFareTempeObj;
import com.jili.ubert.dao.trace.TraceObjInfo;
import com.jili.ubert.dao.trace.TraceObjProtfolio;
import com.jili.ubert.dao.trace.TraceObjProtfolioList;
import com.jili.ubert.dao.trace.TraceObjUnderlying;
import com.jili.ubert.dao.trace.TraceProductAccount;
import com.jili.ubert.dao.trace.TraceProductInfo;
import com.jili.ubert.dao.trace.TraceProductUnit;
import com.jili.ubert.dao.trace.TraceRiskCtrlAssets;
import com.jili.ubert.dao.trace.TraceRiskSets;
import com.jili.ubert.dao.trace.TraceSafeForbidLogin;
import com.jili.ubert.dao.trace.TraceSafeOtherAccess;
import com.jili.ubert.dao.trace.TraceTradesetExchangeLimit;
import com.jili.ubert.dao.trace.TraceUserAccess;
import com.jili.ubert.dao.trace.TraceUserAccount;
import com.jili.ubert.dao.trace.TraceUserInfo;
import com.jili.ubert.dao.trace.TraceUserPrivateInfo;
import com.jili.ubert.dao.trace.TraceWorkFlowPath;
import com.jili.ubert.dao.trace.TraceWorkGroup;
import com.jili.ubert.dao.trace.TraceWorkGroupUser;
import com.jili.ubert.dao.trace.TraceWorkProductFlow;
import com.panemu.tiwulfx.common.TableCriteria;
import com.panemu.tiwulfx.common.TableData;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author ChengJiLi
 */
public class DataPoolHistory {

    private static final Log log = LogFactory.getLog(DataPoolHistory.class);
    private final HistoryDBProvider historyDBProvider;
    private final RecentDBProvider recentDBProvider;
    private final TraceDBProvider traceDBProvider;
    private DataPoolGate dataPoolGate;

    public DataPoolHistory() {
        this.historyDBProvider = new HistoryDBProvider();
        this.recentDBProvider = new RecentDBProvider();
        this.traceDBProvider = new TraceDBProvider();
    }
 public PageDataProto queryHistoryClearingCurrencyRates(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<HistoryClearingCurrencyRates> dataList = historyDBProvider.queryHistoryClearingCurrencyRates(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((HistoryClearingCurrencyRates o) -> {
            try {
                data.add(HistoryClearingCurrencyRates.EnCode(o));
            } catch (IOException ex) {
                log.info("HistoryClearingCurrencyRates EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryHistoryIOPVAccount(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<HistoryIOPVAccount> dataList = historyDBProvider.queryHistoryIOPVAccount(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((HistoryIOPVAccount o) -> {
            try {
                data.add(HistoryIOPVAccount.EnCode(o));
            } catch (IOException ex) {
                log.info("HistoryIOPVAccount EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryHistoryIOPVProduct(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<HistoryIOPVProduct> dataList = historyDBProvider.queryHistoryIOPVProduct(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((HistoryIOPVProduct o) -> {
            try {
                data.add(HistoryIOPVProduct.EnCode(o));
            } catch (IOException ex) {
                log.info("HistoryIOPVProduct EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryHistoryIOPVProductUnit(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<HistoryIOPVProductUnit> dataList = historyDBProvider.queryHistoryIOPVProductUnit(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((HistoryIOPVProductUnit o) -> {
            try {
                data.add(HistoryIOPVProductUnit.EnCode(o));
            } catch (IOException ex) {
                log.info("HistoryIOPVProductUnit EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryHistoryLogLoginInfo(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<HistoryLogLoginInfo> dataList = historyDBProvider.queryHistoryLogLoginInfo(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((HistoryLogLoginInfo o) -> {
            try {
                data.add(HistoryLogLoginInfo.EnCode(o));
            } catch (IOException ex) {
                log.info("HistoryLogLoginInfo EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryHistoryObjExRightInfo(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<HistoryObjExRightInfo> dataList = historyDBProvider.queryHistoryObjExRightInfo(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((HistoryObjExRightInfo o) -> {
            try {
                data.add(HistoryObjExRightInfo.EnCode(o));
            } catch (IOException ex) {
                log.info("HistoryObjExRightInfo EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryHistoryTradeFundTransfer(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<HistoryTradeFundTransfer> dataList = historyDBProvider.queryHistoryTradeFundTransfer(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((HistoryTradeFundTransfer o) -> {
            try {
                data.add(HistoryTradeFundTransfer.EnCode(o));
            } catch (IOException ex) {
                log.info("HistoryTradeFundTransfer EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryHistoryTradesetCalendar(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<HistoryTradesetCalendar> dataList = historyDBProvider.queryHistoryTradesetCalendar(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((HistoryTradesetCalendar o) -> {
            try {
                data.add(HistoryTradesetCalendar.EnCode(o));
            } catch (IOException ex) {
                log.info("HistoryTradesetCalendar EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryRecentClearingAccountBail(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<RecentClearingAccountBail> dataList = recentDBProvider.queryRecentClearingAccountBail(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((RecentClearingAccountBail o) -> {
            try {
                data.add(RecentClearingAccountBail.EnCode(o));
            } catch (IOException ex) {
                log.info("RecentClearingAccountBail EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryRecentClearingAccountFare(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<RecentClearingAccountFare> dataList = recentDBProvider.queryRecentClearingAccountFare(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((RecentClearingAccountFare o) -> {
            try {
                data.add(RecentClearingAccountFare.EnCode(o));
            } catch (IOException ex) {
                log.info("RecentClearingAccountFare EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryRecentClearingDayBook(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<RecentClearingDayBook> dataList = recentDBProvider.queryRecentClearingDayBook(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((RecentClearingDayBook o) -> {
            try {
                data.add(RecentClearingDayBook.EnCode(o));
            } catch (IOException ex) {
                log.info("RecentClearingDayBook EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryRecentClearingExRight(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<RecentClearingExRight> dataList = recentDBProvider.queryRecentClearingExRight(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((RecentClearingExRight o) -> {
            try {
                data.add(RecentClearingExRight.EnCode(o));
            } catch (IOException ex) {
                log.info("RecentClearingExRight EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryRecentClearingFundState(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<RecentClearingFundState> dataList = recentDBProvider.queryRecentClearingFundState(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((RecentClearingFundState o) -> {
            try {
                data.add(RecentClearingFundState.EnCode(o));
            } catch (IOException ex) {
                log.info("RecentClearingFundState EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryRecentClearingPosDetail(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<RecentClearingPosDetail> dataList = recentDBProvider.queryRecentClearingPosDetail(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((RecentClearingPosDetail o) -> {
            try {
                data.add(RecentClearingPosDetail.EnCode(o));
            } catch (IOException ex) {
                log.info("RecentClearingPosDetail EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryRecentClearingPrice(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<RecentClearingPrice> dataList = recentDBProvider.queryRecentClearingPrice(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((RecentClearingPrice o) -> {
            try {
                data.add(RecentClearingPrice.EnCode(o));
            } catch (IOException ex) {
                log.info("RecentClearingPrice EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryRecentClearingProtfoliopos(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<RecentClearingProtfoliopos> dataList = recentDBProvider.queryRecentClearingProtfoliopos(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((RecentClearingProtfoliopos o) -> {
            try {
                data.add(RecentClearingProtfoliopos.EnCode(o));
            } catch (IOException ex) {
                log.info("RecentClearingProtfoliopos EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryRecentObjETF(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<RecentObjETF> dataList = recentDBProvider.queryRecentObjETF(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((RecentObjETF o) -> {
            try {
                data.add(RecentObjETF.EnCode(o));
            } catch (IOException ex) {
                log.info("RecentObjETF EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryRecentObjETFList(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<RecentObjETFList> dataList = recentDBProvider.queryRecentObjETFList(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((RecentObjETFList o) -> {
            try {
                data.add(RecentObjETFList.EnCode(o));
            } catch (IOException ex) {
                log.info("RecentObjETFList EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryRecentObjIndex(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<RecentObjIndex> dataList = recentDBProvider.queryRecentObjIndex(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((RecentObjIndex o) -> {
            try {
                data.add(RecentObjIndex.EnCode(o));
            } catch (IOException ex) {
                log.info("RecentObjIndex EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryRecentObjIndexList(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<RecentObjIndexList> dataList = recentDBProvider.queryRecentObjIndexList(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((RecentObjIndexList o) -> {
            try {
                data.add(RecentObjIndexList.EnCode(o));
            } catch (IOException ex) {
                log.info("RecentObjIndexList EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryRecentTradeAlterOrder(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<RecentTradeAlterOrder> dataList = recentDBProvider.queryRecentTradeAlterOrder(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((RecentTradeAlterOrder o) -> {
            try {
                data.add(RecentTradeAlterOrder.EnCode(o));
            } catch (IOException ex) {
                log.info("RecentTradeAlterOrder EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryRecentTradeCancelOrder(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<RecentTradeCancelOrder> dataList = recentDBProvider.queryRecentTradeCancelOrder(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((RecentTradeCancelOrder o) -> {
            try {
                data.add(RecentTradeCancelOrder.EnCode(o));
            } catch (IOException ex) {
                log.info("RecentTradeCancelOrder EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryRecentTradeExecuteOrder(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<RecentTradeExecuteOrder> dataList = recentDBProvider.queryRecentTradeExecuteOrder(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((RecentTradeExecuteOrder o) -> {
            try {
                data.add(RecentTradeExecuteOrder.EnCode(o));
            } catch (IOException ex) {
                log.info("RecentTradeExecuteOrder EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryRecentTradeNewOrder(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<RecentTradeNewOrder> dataList = recentDBProvider.queryRecentTradeNewOrder(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((RecentTradeNewOrder o) -> {
            try {
                data.add(RecentTradeNewOrder.EnCode(o));
            } catch (IOException ex) {
                log.info("RecentTradeNewOrder EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryRecentTradeReportDetail(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<RecentTradeReportDetail> dataList = recentDBProvider.queryRecentTradeReportDetail(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((RecentTradeReportDetail o) -> {
            try {
                data.add(RecentTradeReportDetail.EnCode(o));
            } catch (IOException ex) {
                log.info("RecentTradeReportDetail EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceAccountBail(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceAccountBail> dataList = traceDBProvider.queryTraceAccountBail(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceAccountBail o) -> {
            try {
                data.add(TraceAccountBail.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceAccountBail EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceAccountFare(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceAccountFare> dataList = traceDBProvider.queryTraceAccountFare(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceAccountFare o) -> {
            try {
                data.add(TraceAccountFare.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceAccountFare EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceAccountIcode(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceAccountIcode> dataList = traceDBProvider.queryTraceAccountIcode(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceAccountIcode o) -> {
            try {
                data.add(TraceAccountIcode.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceAccountIcode EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceAccountInfo(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceAccountInfo> dataList = traceDBProvider.queryTraceAccountInfo(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceAccountInfo o) -> {
            try {
                data.add(TraceAccountInfo.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceAccountInfo EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceAccountInvest(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceAccountInvest> dataList = traceDBProvider.queryTraceAccountInvest(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceAccountInvest o) -> {
            try {
                data.add(TraceAccountInvest.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceAccountInvest EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceBailTemp(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceBailTemp> dataList = traceDBProvider.queryTraceBailTemp(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceBailTemp o) -> {
            try {
                data.add(TraceBailTemp.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceBailTemp EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceBailTempObj(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceBailTempObj> dataList = traceDBProvider.queryTraceBailTempObj(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceBailTempObj o) -> {
            try {
                data.add(TraceBailTempObj.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceBailTempObj EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceFareTemp(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceFareTemp> dataList = traceDBProvider.queryTraceFareTemp(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceFareTemp o) -> {
            try {
                data.add(TraceFareTemp.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceFareTemp EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceFareTempeObj(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceFareTempeObj> dataList = traceDBProvider.queryTraceFareTempeObj(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceFareTempeObj o) -> {
            try {
                data.add(TraceFareTempeObj.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceFareTempeObj EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceObjInfo(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceObjInfo> dataList = traceDBProvider.queryTraceObjInfo(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceObjInfo o) -> {
            try {
                data.add(TraceObjInfo.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceObjInfo EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceObjProtfolio(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceObjProtfolio> dataList = traceDBProvider.queryTraceObjProtfolio(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceObjProtfolio o) -> {
            try {
                data.add(TraceObjProtfolio.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceObjProtfolio EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceObjProtfolioList(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceObjProtfolioList> dataList = traceDBProvider.queryTraceObjProtfolioList(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceObjProtfolioList o) -> {
            try {
                data.add(TraceObjProtfolioList.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceObjProtfolioList EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceObjUnderlying(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceObjUnderlying> dataList = traceDBProvider.queryTraceObjUnderlying(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceObjUnderlying o) -> {
            try {
                data.add(TraceObjUnderlying.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceObjUnderlying EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceProductAccount(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceProductAccount> dataList = traceDBProvider.queryTraceProductAccount(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceProductAccount o) -> {
            try {
                data.add(TraceProductAccount.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceProductAccount EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceProductInfo(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceProductInfo> dataList = traceDBProvider.queryTraceProductInfo(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceProductInfo o) -> {
            try {
                data.add(TraceProductInfo.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceProductInfo EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceProductUnit(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceProductUnit> dataList = traceDBProvider.queryTraceProductUnit(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceProductUnit o) -> {
            try {
                data.add(TraceProductUnit.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceProductUnit EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceRiskCtrlAssets(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceRiskCtrlAssets> dataList = traceDBProvider.queryTraceRiskCtrlAssets(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceRiskCtrlAssets o) -> {
            try {
                data.add(TraceRiskCtrlAssets.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceRiskCtrlAssets EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceRiskSets(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceRiskSets> dataList = traceDBProvider.queryTraceRiskSets(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceRiskSets o) -> {
            try {
                data.add(TraceRiskSets.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceRiskSets EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceSafeForbidLogin(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceSafeForbidLogin> dataList = traceDBProvider.queryTraceSafeForbidLogin(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceSafeForbidLogin o) -> {
            try {
                data.add(TraceSafeForbidLogin.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceSafeForbidLogin EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceSafeOtherAccess(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceSafeOtherAccess> dataList = traceDBProvider.queryTraceSafeOtherAccess(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceSafeOtherAccess o) -> {
            try {
                data.add(TraceSafeOtherAccess.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceSafeOtherAccess EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceTradesetExchangeLimit(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceTradesetExchangeLimit> dataList = traceDBProvider.queryTraceTradesetExchangeLimit(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceTradesetExchangeLimit o) -> {
            try {
                data.add(TraceTradesetExchangeLimit.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceTradesetExchangeLimit EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceUserAccess(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceUserAccess> dataList = traceDBProvider.queryTraceUserAccess(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceUserAccess o) -> {
            try {
                data.add(TraceUserAccess.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceUserAccess EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceUserAccount(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceUserAccount> dataList = traceDBProvider.queryTraceUserAccount(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceUserAccount o) -> {
            try {
                data.add(TraceUserAccount.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceUserAccount EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceUserInfo(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceUserInfo> dataList = traceDBProvider.queryTraceUserInfo(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceUserInfo o) -> {
            try {
                data.add(TraceUserInfo.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceUserInfo EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceUserPrivateInfo(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceUserPrivateInfo> dataList = traceDBProvider.queryTraceUserPrivateInfo(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceUserPrivateInfo o) -> {
            try {
                data.add(TraceUserPrivateInfo.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceUserPrivateInfo EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceWorkFlowPath(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceWorkFlowPath> dataList = traceDBProvider.queryTraceWorkFlowPath(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceWorkFlowPath o) -> {
            try {
                data.add(TraceWorkFlowPath.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceWorkFlowPath EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceWorkGroup(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceWorkGroup> dataList = traceDBProvider.queryTraceWorkGroup(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceWorkGroup o) -> {
            try {
                data.add(TraceWorkGroup.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceWorkGroup EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceWorkGroupUser(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceWorkGroupUser> dataList = traceDBProvider.queryTraceWorkGroupUser(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceWorkGroupUser o) -> {
            try {
                data.add(TraceWorkGroupUser.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceWorkGroupUser EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceWorkProductFlow(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceWorkProductFlow> dataList = traceDBProvider.queryTraceWorkProductFlow(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceWorkProductFlow o) -> {
            try {
                data.add(TraceWorkProductFlow.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceWorkProductFlow EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryString(SelectPageData rpd) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
