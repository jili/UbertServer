/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.datapool;

import com.alibaba.fastjson.JSON;
import com.jili.ubert.code.client2server.RequestBatchOperateDB;
import com.jili.ubert.code.client2server.RequestPageData;
import com.jili.ubert.code.server2client.MsgResult;
import com.jili.ubert.code.server2client.PageDataProto;
import com.jili.ubert.dao.db.AccountBail;
import com.jili.ubert.dao.db.AccountFare;
import com.jili.ubert.dao.db.AccountIcode;
import com.jili.ubert.dao.db.AccountInfo;
import com.jili.ubert.dao.db.AccountInvest;
import com.jili.ubert.dao.db.AlterOrder;
import com.jili.ubert.dao.db.BailTemp;
import com.jili.ubert.dao.db.BailTempObj;
import com.jili.ubert.dao.db.CancelOrder;
import com.jili.ubert.dao.db.ClearingCurrencyRates;
import com.jili.ubert.dao.db.ClearingDayBook;
import com.jili.ubert.dao.db.ClearingExRight;
import com.jili.ubert.dao.db.ClearingPrice;
import com.jili.ubert.dao.db.ClearingProtfoliopos;
import com.jili.ubert.dao.db.FareTemp;
import com.jili.ubert.dao.db.FareTempeObj;
import com.jili.ubert.dao.db.FundState;
import com.jili.ubert.dao.db.FundTransfer;
import com.jili.ubert.dao.db.IOPVAccount;
import com.jili.ubert.dao.db.IOPVProduct;
import com.jili.ubert.dao.db.IOPVProductUnit;
import com.jili.ubert.dao.db.LogLoginInfo;
import com.jili.ubert.dao.db.NewOrder;
import com.jili.ubert.dao.db.ObjClassCode;
import com.jili.ubert.dao.db.ObjClassCodeAtrr;
import com.jili.ubert.dao.db.ObjETF;
import com.jili.ubert.dao.db.ObjETFList;
import com.jili.ubert.dao.db.ObjExRightInfo;
import com.jili.ubert.dao.db.ObjIndex;
import com.jili.ubert.dao.db.ObjIndexList;
import com.jili.ubert.dao.db.ObjInfo;
import com.jili.ubert.dao.db.ObjNewStock;
import com.jili.ubert.dao.db.ObjPriceTick;
import com.jili.ubert.dao.db.ObjProtfolio;
import com.jili.ubert.dao.db.ObjProtfolioList;
import com.jili.ubert.dao.db.ObjStructuredFundBase;
import com.jili.ubert.dao.db.ObjStructuredFundPart;
import com.jili.ubert.dao.db.ObjUnderlyingCode;
import com.jili.ubert.dao.db.OrderExecuteProgress;
import com.jili.ubert.dao.db.PosDetail;
import com.jili.ubert.dao.db.ProductAccount;
import com.jili.ubert.dao.db.ProductInfo;
import com.jili.ubert.dao.db.ProductUnit;
import com.jili.ubert.dao.db.RiskCtrlAssets;
import com.jili.ubert.dao.db.RiskIndex;
import com.jili.ubert.dao.db.RiskSets;
import com.jili.ubert.dao.db.SafeForbidLogin;
import com.jili.ubert.dao.db.SafeOtherAccess;
import com.jili.ubert.dao.db.TradeReportDetail;
import com.jili.ubert.dao.db.TradeSimOrderQuene;
import com.jili.ubert.dao.db.TradesetBroker;
import com.jili.ubert.dao.db.TradesetCalendar;
import com.jili.ubert.dao.db.TradesetExchangeLimit;
import com.jili.ubert.dao.db.TradesetPip;
import com.jili.ubert.dao.db.UserAccess;
import com.jili.ubert.dao.db.UserAccount;
import com.jili.ubert.dao.db.UserInfo;
import com.jili.ubert.dao.db.UserPrivateInfo;
import com.jili.ubert.dao.db.WorkFlowPath;
import com.jili.ubert.dao.db.WorkGroup;
import com.jili.ubert.dao.db.WorkGroupUser;
import com.jili.ubert.dao.db.WorkProductFlow;
import com.jili.ubert.dao.trace.TraceAccountBail;
import com.jili.ubert.dao.trace.TraceAccountFare;
import com.jili.ubert.dao.trace.TraceAccountIcode;
import com.jili.ubert.dao.trace.TraceAccountInfo;
import com.jili.ubert.dao.trace.TraceAccountInvest;
import com.jili.ubert.dao.trace.TraceBailTemp;
import com.jili.ubert.dao.trace.TraceBailTempObj;
import com.jili.ubert.dao.trace.TraceFareTemp;
import com.jili.ubert.dao.trace.TraceFareTempeObj;
import com.jili.ubert.dao.trace.TraceObjInfo;
import com.jili.ubert.dao.trace.TraceObjProtfolio;
import com.jili.ubert.dao.trace.TraceObjProtfolioList;
import com.jili.ubert.dao.trace.TraceObjUnderlying;
import com.jili.ubert.dao.trace.TraceProductAccount;
import com.jili.ubert.dao.trace.TraceProductInfo;
import com.jili.ubert.dao.trace.TraceProductUnit;
import com.jili.ubert.dao.trace.TraceRiskCtrlAssets;
import com.jili.ubert.dao.trace.TraceRiskSets;
import com.jili.ubert.dao.trace.TraceSafeForbidLogin;
import com.jili.ubert.dao.trace.TraceSafeOtherAccess;
import com.jili.ubert.dao.trace.TraceTradesetExchangeLimit;
import com.jili.ubert.dao.trace.TraceUserAccess;
import com.jili.ubert.dao.trace.TraceUserAccount;
import com.jili.ubert.dao.trace.TraceUserInfo;
import com.jili.ubert.dao.trace.TraceUserPrivateInfo;
import com.jili.ubert.dao.trace.TraceWorkFlowPath;
import com.jili.ubert.dao.trace.TraceWorkGroup;
import com.jili.ubert.dao.trace.TraceWorkGroupUser;
import com.jili.ubert.dao.trace.TraceWorkProductFlow;
import com.panemu.tiwulfx.common.TableCriteria;
import com.panemu.tiwulfx.common.TableData;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author ChengJiLi
 */
public class DataPoolTrade {
    private static final Log log = LogFactory.getLog(DataPoolTrade.class);
    private final DBProvider dBProvider;
    private final TraceDBProvider traceDBProvider;

    public DataPoolTrade() {
        dBProvider = new DBProvider();
        traceDBProvider = new TraceDBProvider();
    }
    public EntityManagerFactory getDBFactory(){
        return dBProvider.getFactory();
    }
    public EntityManagerFactory getTraceDBFactory(){
        return traceDBProvider.getFactory();
    }
public PageDataProto queryAccountBail(RequestPageData rpd) {List<TableCriteria> filteredColumns =DataPoolGate.decodeFilter(rpd);TableData<AccountBail> dataList = dBProvider.queryAccountBail(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());List<byte[]> data = new ArrayList<>(rpd.getMaxResult());dataList.getRows().stream().forEach((AccountBail o) -> {try {data.add(AccountBail.EnCode(o));} catch (IOException ex) {log.info("AccountBail EnCode fail:"+ ex);}});PageDataProto pdp = DataPoolGate.TransferPageData(rpd,dataList);pdp.setDataList(data);return pdp;}

    public PageDataProto queryAccountFare(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<AccountFare> dataList = dBProvider.queryAccountFare(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((AccountFare o) -> {
            try {
                data.add(AccountFare.EnCode(o));
            } catch (IOException ex) {
                log.info("AccountFare EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryAccountIcode(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<AccountIcode> dataList = dBProvider.queryAccountIcode(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((AccountIcode o) -> {
            try {
                data.add(AccountIcode.EnCode(o));
            } catch (IOException ex) {
                log.info("AccountIcode EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryAccountInfo(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<AccountInfo> dataList = dBProvider.queryAccountInfo(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((AccountInfo o) -> {
            try {
                data.add(AccountInfo.EnCode(o));
            } catch (IOException ex) {
                log.info("AccountInfo EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryAccountInvest(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<AccountInvest> dataList = dBProvider.queryAccountInvest(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((AccountInvest o) -> {
            try {
                data.add(AccountInvest.EnCode(o));
            } catch (IOException ex) {
                log.info("AccountInvest EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryAlterOrder(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<AlterOrder> dataList = dBProvider.queryAlterOrder(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((AlterOrder o) -> {
            try {
                data.add(AlterOrder.EnCode(o));
            } catch (IOException ex) {
                log.info("AlterOrder EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryBailTemp(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<BailTemp> dataList = dBProvider.queryBailTemp(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((BailTemp o) -> {
            try {
                data.add(BailTemp.EnCode(o));
            } catch (IOException ex) {
                log.info("BailTemp EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryBailTempObj(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<BailTempObj> dataList = dBProvider.queryBailTempObj(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((BailTempObj o) -> {
            try {
                data.add(BailTempObj.EnCode(o));
            } catch (IOException ex) {
                log.info("BailTempObj EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryCancelOrder(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<CancelOrder> dataList = dBProvider.queryCancelOrder(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((CancelOrder o) -> {
            try {
                data.add(CancelOrder.EnCode(o));
            } catch (IOException ex) {
                log.info("CancelOrder EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryClearingCurrencyRates(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ClearingCurrencyRates> dataList = dBProvider.queryClearingCurrencyRates(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ClearingCurrencyRates o) -> {
            try {
                data.add(ClearingCurrencyRates.EnCode(o));
            } catch (IOException ex) {
                log.info("ClearingCurrencyRates EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryClearingDayBook(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ClearingDayBook> dataList = dBProvider.queryClearingDayBook(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ClearingDayBook o) -> {
            try {
                data.add(ClearingDayBook.EnCode(o));
            } catch (IOException ex) {
                log.info("ClearingDayBook EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryClearingExRight(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ClearingExRight> dataList = dBProvider.queryClearingExRight(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ClearingExRight o) -> {
            try {
                data.add(ClearingExRight.EnCode(o));
            } catch (IOException ex) {
                log.info("ClearingExRight EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryClearingPrice(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ClearingPrice> dataList = dBProvider.queryClearingPrice(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ClearingPrice o) -> {
            try {
                data.add(ClearingPrice.EnCode(o));
            } catch (IOException ex) {
                log.info("ClearingPrice EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryClearingProtfoliopos(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ClearingProtfoliopos> dataList = dBProvider.queryClearingProtfoliopos(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ClearingProtfoliopos o) -> {
            try {
                data.add(ClearingProtfoliopos.EnCode(o));
            } catch (IOException ex) {
                log.info("ClearingProtfoliopos EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryFareTemp(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<FareTemp> dataList = dBProvider.queryFareTemp(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((FareTemp o) -> {
            try {
                data.add(FareTemp.EnCode(o));
            } catch (IOException ex) {
                log.info("FareTemp EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryFareTempeObj(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<FareTempeObj> dataList = dBProvider.queryFareTempeObj(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((FareTempeObj o) -> {
            try {
                data.add(FareTempeObj.EnCode(o));
            } catch (IOException ex) {
                log.info("FareTempeObj EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryFundState(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<FundState> dataList = dBProvider.queryFundState(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((FundState o) -> {
            try {
                data.add(FundState.EnCode(o));
            } catch (IOException ex) {
                log.info("FundState EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryFundTransfer(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<FundTransfer> dataList = dBProvider.queryFundTransfer(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((FundTransfer o) -> {
            try {
                data.add(FundTransfer.EnCode(o));
            } catch (IOException ex) {
                log.info("FundTransfer EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryIOPVAccount(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<IOPVAccount> dataList = dBProvider.queryIOPVAccount(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((IOPVAccount o) -> {
            try {
                data.add(IOPVAccount.EnCode(o));
            } catch (IOException ex) {
                log.info("IOPVAccount EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryIOPVProduct(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<IOPVProduct> dataList = dBProvider.queryIOPVProduct(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((IOPVProduct o) -> {
            try {
                data.add(IOPVProduct.EnCode(o));
            } catch (IOException ex) {
                log.info("IOPVProduct EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryIOPVProductUnit(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<IOPVProductUnit> dataList = dBProvider.queryIOPVProductUnit(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((IOPVProductUnit o) -> {
            try {
                data.add(IOPVProductUnit.EnCode(o));
            } catch (IOException ex) {
                log.info("IOPVProductUnit EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryLogLoginInfo(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<LogLoginInfo> dataList = dBProvider.queryLogLoginInfo(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((LogLoginInfo o) -> {
            try {
                data.add(LogLoginInfo.EnCode(o));
            } catch (IOException ex) {
                log.info("LogLoginInfo EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryNewOrder(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<NewOrder> dataList = dBProvider.queryNewOrder(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((NewOrder o) -> {
            try {
                data.add(NewOrder.EnCode(o));
            } catch (IOException ex) {
                log.info("NewOrder EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryObjClassCode(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ObjClassCode> dataList = dBProvider.queryObjClassCode(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ObjClassCode o) -> {
            try {
                data.add(ObjClassCode.EnCode(o));
            } catch (IOException ex) {
                log.info("ObjClassCode EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryObjClassCodeAtrr(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ObjClassCodeAtrr> dataList = dBProvider.queryObjClassCodeAtrr(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ObjClassCodeAtrr o) -> {
            try {
                data.add(ObjClassCodeAtrr.EnCode(o));
            } catch (IOException ex) {
                log.info("ObjClassCodeAtrr EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryObjETF(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ObjETF> dataList = dBProvider.queryObjETF(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ObjETF o) -> {
            try {
                data.add(ObjETF.EnCode(o));
            } catch (IOException ex) {
                log.info("ObjETF EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryObjETFList(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ObjETFList> dataList = dBProvider.queryObjETFList(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ObjETFList o) -> {
            try {
                data.add(ObjETFList.EnCode(o));
            } catch (IOException ex) {
                log.info("ObjETFList EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryObjExRightInfo(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ObjExRightInfo> dataList = dBProvider.queryObjExRightInfo(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ObjExRightInfo o) -> {
            try {
                data.add(ObjExRightInfo.EnCode(o));
            } catch (IOException ex) {
                log.info("ObjExRightInfo EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryObjIndex(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ObjIndex> dataList = dBProvider.queryObjIndex(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ObjIndex o) -> {
            try {
                data.add(ObjIndex.EnCode(o));
            } catch (IOException ex) {
                log.info("ObjIndex EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryObjIndexList(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ObjIndexList> dataList = dBProvider.queryObjIndexList(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ObjIndexList o) -> {
            try {
                data.add(ObjIndexList.EnCode(o));
            } catch (IOException ex) {
                log.info("ObjIndexList EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryObjInfo(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ObjInfo> dataList = dBProvider.queryObjInfo(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ObjInfo o) -> {
            try {
                data.add(ObjInfo.EnCode(o));
            } catch (IOException ex) {
                log.info("ObjInfo EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryObjNewStock(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ObjNewStock> dataList = dBProvider.queryObjNewStock(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ObjNewStock o) -> {
            try {
                data.add(ObjNewStock.EnCode(o));
            } catch (IOException ex) {
                log.info("ObjNewStock EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryObjPriceTick(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ObjPriceTick> dataList = dBProvider.queryObjPriceTick(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ObjPriceTick o) -> {
            try {
                data.add(ObjPriceTick.EnCode(o));
            } catch (IOException ex) {
                log.info("ObjPriceTick EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryObjProtfolio(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ObjProtfolio> dataList = dBProvider.queryObjProtfolio(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ObjProtfolio o) -> {
            try {
                data.add(ObjProtfolio.EnCode(o));
            } catch (IOException ex) {
                log.info("ObjProtfolio EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryObjProtfolioList(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ObjProtfolioList> dataList = dBProvider.queryObjProtfolioList(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ObjProtfolioList o) -> {
            try {
                data.add(ObjProtfolioList.EnCode(o));
            } catch (IOException ex) {
                log.info("ObjProtfolioList EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryObjStructuredFundBase(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ObjStructuredFundBase> dataList = dBProvider.queryObjStructuredFundBase(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ObjStructuredFundBase o) -> {
            try {
                data.add(ObjStructuredFundBase.EnCode(o));
            } catch (IOException ex) {
                log.info("ObjStructuredFundBase EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryObjStructuredFundPart(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ObjStructuredFundPart> dataList = dBProvider.queryObjStructuredFundPart(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ObjStructuredFundPart o) -> {
            try {
                data.add(ObjStructuredFundPart.EnCode(o));
            } catch (IOException ex) {
                log.info("ObjStructuredFundPart EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryObjUnderlyingCode(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ObjUnderlyingCode> dataList = dBProvider.queryObjUnderlyingCode(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ObjUnderlyingCode o) -> {
            try {
                data.add(ObjUnderlyingCode.EnCode(o));
            } catch (IOException ex) {
                log.info("ObjUnderlyingCode EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryOrderExecuteProgress(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<OrderExecuteProgress> dataList = dBProvider.queryOrderExecuteProgress(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((OrderExecuteProgress o) -> {
            try {
                data.add(OrderExecuteProgress.EnCode(o));
            } catch (IOException ex) {
                log.info("OrderExecuteProgress EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryPosDetail(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<PosDetail> dataList = dBProvider.queryPosDetail(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((PosDetail o) -> {
            try {
                data.add(PosDetail.EnCode(o));
            } catch (IOException ex) {
                log.info("PosDetail EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryProductAccount(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ProductAccount> dataList = dBProvider.queryProductAccount(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ProductAccount o) -> {
            try {
                data.add(ProductAccount.EnCode(o));
            } catch (IOException ex) {
                log.info("ProductAccount EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryProductInfo(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ProductInfo> dataList = dBProvider.queryProductInfo(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ProductInfo o) -> {
            try {
                data.add(ProductInfo.EnCode(o));
            } catch (IOException ex) {
                log.info("ProductInfo EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryProductUnit(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<ProductUnit> dataList = dBProvider.queryProductUnit(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((ProductUnit o) -> {
            try {
                data.add(ProductUnit.EnCode(o));
            } catch (IOException ex) {
                log.info("ProductUnit EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryRiskCtrlAssets(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<RiskCtrlAssets> dataList = dBProvider.queryRiskCtrlAssets(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((RiskCtrlAssets o) -> {
            try {
                data.add(RiskCtrlAssets.EnCode(o));
            } catch (IOException ex) {
                log.info("RiskCtrlAssets EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryRiskIndex(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<RiskIndex> dataList = dBProvider.queryRiskIndex(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((RiskIndex o) -> {
            try {
                data.add(RiskIndex.EnCode(o));
            } catch (IOException ex) {
                log.info("RiskIndex EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryRiskSets(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<RiskSets> dataList = dBProvider.queryRiskSets(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((RiskSets o) -> {
            try {
                data.add(RiskSets.EnCode(o));
            } catch (IOException ex) {
                log.info("RiskSets EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto querySafeForbidLogin(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<SafeForbidLogin> dataList = dBProvider.querySafeForbidLogin(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((SafeForbidLogin o) -> {
            try {
                data.add(SafeForbidLogin.EnCode(o));
            } catch (IOException ex) {
                log.info("SafeForbidLogin EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto querySafeOtherAccess(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<SafeOtherAccess> dataList = dBProvider.querySafeOtherAccess(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((SafeOtherAccess o) -> {
            try {
                data.add(SafeOtherAccess.EnCode(o));
            } catch (IOException ex) {
                log.info("SafeOtherAccess EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTradeReportDetail(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TradeReportDetail> dataList = dBProvider.queryTradeReportDetail(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TradeReportDetail o) -> {
            try {
                data.add(TradeReportDetail.EnCode(o));
            } catch (IOException ex) {
                log.info("TradeReportDetail EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTradesetBroker(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TradesetBroker> dataList = dBProvider.queryTradesetBroker(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TradesetBroker o) -> {
            try {
                data.add(TradesetBroker.EnCode(o));
            } catch (IOException ex) {
                log.info("TradesetBroker EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTradesetCalendar(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TradesetCalendar> dataList = dBProvider.queryTradesetCalendar(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TradesetCalendar o) -> {
            try {
                data.add(TradesetCalendar.EnCode(o));
            } catch (IOException ex) {
                log.info("TradesetCalendar EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTradesetExchangeLimit(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TradesetExchangeLimit> dataList = dBProvider.queryTradesetExchangeLimit(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TradesetExchangeLimit o) -> {
            try {
                data.add(TradesetExchangeLimit.EnCode(o));
            } catch (IOException ex) {
                log.info("TradesetExchangeLimit EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTradesetPip(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TradesetPip> dataList = dBProvider.queryTradesetPip(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TradesetPip o) -> {
            try {
                data.add(TradesetPip.EnCode(o));
            } catch (IOException ex) {
                log.info("TradesetPip EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTradeSimOrderQuene(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TradeSimOrderQuene> dataList = dBProvider.queryTradeSimOrderQuene(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TradeSimOrderQuene o) -> {
            try {
                data.add(TradeSimOrderQuene.EnCode(o));
            } catch (IOException ex) {
                log.info("TradeSimOrderQuene EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryUserAccess(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<UserAccess> dataList = dBProvider.queryUserAccess(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((UserAccess o) -> {
            try {
                data.add(UserAccess.EnCode(o));
            } catch (IOException ex) {
                log.info("UserAccess EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryUserAccount(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<UserAccount> dataList = dBProvider.queryUserAccount(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((UserAccount o) -> {
            try {
                data.add(UserAccount.EnCode(o));
            } catch (IOException ex) {
                log.info("UserAccount EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryUserInfo(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<UserInfo> dataList = dBProvider.queryUserInfo(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((UserInfo o) -> {
            try {
                data.add(UserInfo.EnCode(o));
            } catch (IOException ex) {
                log.info("UserInfo EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryUserPrivateInfo(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<UserPrivateInfo> dataList = dBProvider.queryUserPrivateInfo(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((UserPrivateInfo o) -> {
            try {
                data.add(UserPrivateInfo.EnCode(o));
            } catch (IOException ex) {
                log.info("UserPrivateInfo EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryWorkFlowPath(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<WorkFlowPath> dataList = dBProvider.queryWorkFlowPath(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((WorkFlowPath o) -> {
            try {
                data.add(WorkFlowPath.EnCode(o));
            } catch (IOException ex) {
                log.info("WorkFlowPath EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryWorkGroup(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<WorkGroup> dataList = dBProvider.queryWorkGroup(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((WorkGroup o) -> {
            try {
                data.add(WorkGroup.EnCode(o));
            } catch (IOException ex) {
                log.info("WorkGroup EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryWorkGroupUser(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<WorkGroupUser> dataList = dBProvider.queryWorkGroupUser(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((WorkGroupUser o) -> {
            try {
                data.add(WorkGroupUser.EnCode(o));
            } catch (IOException ex) {
                log.info("WorkGroupUser EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryWorkProductFlow(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<WorkProductFlow> dataList = dBProvider.queryWorkProductFlow(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((WorkProductFlow o) -> {
            try {
                data.add(WorkProductFlow.EnCode(o));
            } catch (IOException ex) {
                log.info("WorkProductFlow EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceAccountBail(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceAccountBail> dataList = traceDBProvider.queryTraceAccountBail(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceAccountBail o) -> {
            try {
                data.add(TraceAccountBail.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceAccountBail EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceAccountFare(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceAccountFare> dataList = traceDBProvider.queryTraceAccountFare(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceAccountFare o) -> {
            try {
                data.add(TraceAccountFare.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceAccountFare EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceAccountIcode(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceAccountIcode> dataList = traceDBProvider.queryTraceAccountIcode(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceAccountIcode o) -> {
            try {
                data.add(TraceAccountIcode.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceAccountIcode EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceAccountInfo(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceAccountInfo> dataList = traceDBProvider.queryTraceAccountInfo(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceAccountInfo o) -> {
            try {
                data.add(TraceAccountInfo.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceAccountInfo EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceAccountInvest(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceAccountInvest> dataList = traceDBProvider.queryTraceAccountInvest(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceAccountInvest o) -> {
            try {
                data.add(TraceAccountInvest.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceAccountInvest EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceBailTemp(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceBailTemp> dataList = traceDBProvider.queryTraceBailTemp(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceBailTemp o) -> {
            try {
                data.add(TraceBailTemp.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceBailTemp EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceBailTempObj(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceBailTempObj> dataList = traceDBProvider.queryTraceBailTempObj(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceBailTempObj o) -> {
            try {
                data.add(TraceBailTempObj.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceBailTempObj EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceFareTemp(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceFareTemp> dataList = traceDBProvider.queryTraceFareTemp(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceFareTemp o) -> {
            try {
                data.add(TraceFareTemp.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceFareTemp EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceFareTempeObj(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceFareTempeObj> dataList = traceDBProvider.queryTraceFareTempeObj(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceFareTempeObj o) -> {
            try {
                data.add(TraceFareTempeObj.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceFareTempeObj EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceObjInfo(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceObjInfo> dataList = traceDBProvider.queryTraceObjInfo(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceObjInfo o) -> {
            try {
                data.add(TraceObjInfo.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceObjInfo EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceObjProtfolio(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceObjProtfolio> dataList = traceDBProvider.queryTraceObjProtfolio(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceObjProtfolio o) -> {
            try {
                data.add(TraceObjProtfolio.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceObjProtfolio EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceObjProtfolioList(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceObjProtfolioList> dataList = traceDBProvider.queryTraceObjProtfolioList(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceObjProtfolioList o) -> {
            try {
                data.add(TraceObjProtfolioList.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceObjProtfolioList EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceObjUnderlying(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceObjUnderlying> dataList = traceDBProvider.queryTraceObjUnderlying(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceObjUnderlying o) -> {
            try {
                data.add(TraceObjUnderlying.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceObjUnderlying EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceProductAccount(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceProductAccount> dataList = traceDBProvider.queryTraceProductAccount(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceProductAccount o) -> {
            try {
                data.add(TraceProductAccount.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceProductAccount EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceProductInfo(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceProductInfo> dataList = traceDBProvider.queryTraceProductInfo(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceProductInfo o) -> {
            try {
                data.add(TraceProductInfo.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceProductInfo EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceProductUnit(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceProductUnit> dataList = traceDBProvider.queryTraceProductUnit(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceProductUnit o) -> {
            try {
                data.add(TraceProductUnit.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceProductUnit EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceRiskCtrlAssets(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceRiskCtrlAssets> dataList = traceDBProvider.queryTraceRiskCtrlAssets(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceRiskCtrlAssets o) -> {
            try {
                data.add(TraceRiskCtrlAssets.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceRiskCtrlAssets EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceRiskSets(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceRiskSets> dataList = traceDBProvider.queryTraceRiskSets(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceRiskSets o) -> {
            try {
                data.add(TraceRiskSets.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceRiskSets EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceSafeForbidLogin(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceSafeForbidLogin> dataList = traceDBProvider.queryTraceSafeForbidLogin(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceSafeForbidLogin o) -> {
            try {
                data.add(TraceSafeForbidLogin.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceSafeForbidLogin EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceSafeOtherAccess(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceSafeOtherAccess> dataList = traceDBProvider.queryTraceSafeOtherAccess(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceSafeOtherAccess o) -> {
            try {
                data.add(TraceSafeOtherAccess.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceSafeOtherAccess EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceTradesetExchangeLimit(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceTradesetExchangeLimit> dataList = traceDBProvider.queryTraceTradesetExchangeLimit(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceTradesetExchangeLimit o) -> {
            try {
                data.add(TraceTradesetExchangeLimit.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceTradesetExchangeLimit EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceUserAccess(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceUserAccess> dataList = traceDBProvider.queryTraceUserAccess(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceUserAccess o) -> {
            try {
                data.add(TraceUserAccess.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceUserAccess EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceUserAccount(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceUserAccount> dataList = traceDBProvider.queryTraceUserAccount(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceUserAccount o) -> {
            try {
                data.add(TraceUserAccount.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceUserAccount EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceUserInfo(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceUserInfo> dataList = traceDBProvider.queryTraceUserInfo(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceUserInfo o) -> {
            try {
                data.add(TraceUserInfo.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceUserInfo EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceUserPrivateInfo(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceUserPrivateInfo> dataList = traceDBProvider.queryTraceUserPrivateInfo(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceUserPrivateInfo o) -> {
            try {
                data.add(TraceUserPrivateInfo.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceUserPrivateInfo EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceWorkFlowPath(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceWorkFlowPath> dataList = traceDBProvider.queryTraceWorkFlowPath(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceWorkFlowPath o) -> {
            try {
                data.add(TraceWorkFlowPath.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceWorkFlowPath EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceWorkGroup(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceWorkGroup> dataList = traceDBProvider.queryTraceWorkGroup(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceWorkGroup o) -> {
            try {
                data.add(TraceWorkGroup.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceWorkGroup EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceWorkGroupUser(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceWorkGroupUser> dataList = traceDBProvider.queryTraceWorkGroupUser(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceWorkGroupUser o) -> {
            try {
                data.add(TraceWorkGroupUser.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceWorkGroupUser EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }

    public PageDataProto queryTraceWorkProductFlow(RequestPageData rpd) {
        List<TableCriteria> filteredColumns = DataPoolGate.decodeFilter(rpd);
        TableData<TraceWorkProductFlow> dataList = traceDBProvider.queryTraceWorkProductFlow(rpd.getStartIndex(), filteredColumns, rpd.getSortedColumns(), rpd.getSortingOrders(), rpd.getMaxResult());
        List<byte[]> data = new ArrayList<>(rpd.getMaxResult());
        dataList.getRows().stream().forEach((TraceWorkProductFlow o) -> {
            try {
                data.add(TraceWorkProductFlow.EnCode(o));
            } catch (IOException ex) {
                log.info("TraceWorkProductFlow EnCode fail:" + ex);
            }
        });
        PageDataProto pdp = DataPoolGate.TransferPageData(rpd, dataList);
        pdp.setDataList(data);
        return pdp;
    }
/*
    public MsgResult operateAccountBail(RequestBatchOperateDB ro){
        MsgResult rst;
        List<AccountBail> ls = new ArrayList<>(ro.getDataList().size());
        ro.getDataList().stream().forEach((byte[] o) -> {
            try {
                ls.add(AccountBail.DeCode(o));
            } catch (IOException ex) {
                log.info("AccountBail DeCode fail:" + ex);
            }
        });
        int oo = ro.getInstructionID();
        if (00==1){
            rst=dBProvider.insertAccountBail(ls);
        }else if(oo==2){
            rst=dBProvider.deleteAccountBail(ls);
        }else{
            rst=dBProvider.updateAccountBail(ls);
        }
        return rst;
    }
*/
public MsgResult operateAccountBail(RequestBatchOperateDB ro){MsgResult rst;List<AccountBail> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(AccountBail.DeCode(o));} catch (IOException ex) {log.info("AccountBail DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertAccountBail(ls);}else if(oo==2){rst=dBProvider.deleteAccountBail(ls);}else{rst=dBProvider.updateAccountBail(ls);}return rst;}
public MsgResult operateAccountFare(RequestBatchOperateDB ro){MsgResult rst;List<AccountFare> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(AccountFare.DeCode(o));} catch (IOException ex) {log.info("AccountFare DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertAccountFare(ls);}else if(oo==2){rst=dBProvider.deleteAccountFare(ls);}else{rst=dBProvider.updateAccountFare(ls);}return rst;}
public MsgResult operateAccountIcode(RequestBatchOperateDB ro){MsgResult rst;List<AccountIcode> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(AccountIcode.DeCode(o));} catch (IOException ex) {log.info("AccountIcode DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertAccountIcode(ls);}else if(oo==2){rst=dBProvider.deleteAccountIcode(ls);}else{rst=dBProvider.updateAccountIcode(ls);}return rst;}
public MsgResult operateAccountInfo(RequestBatchOperateDB ro){MsgResult rst;List<AccountInfo> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(AccountInfo.DeCode(o));} catch (IOException ex) {log.info("AccountInfo DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertAccountInfo(ls);}else if(oo==2){rst=dBProvider.deleteAccountInfo(ls);}else{rst=dBProvider.updateAccountInfo(ls);}return rst;}
public MsgResult operateAccountInvest(RequestBatchOperateDB ro){MsgResult rst;List<AccountInvest> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(AccountInvest.DeCode(o));} catch (IOException ex) {log.info("AccountInvest DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertAccountInvest(ls);}else if(oo==2){rst=dBProvider.deleteAccountInvest(ls);}else{rst=dBProvider.updateAccountInvest(ls);}return rst;}
public MsgResult operateAlterOrder(RequestBatchOperateDB ro){MsgResult rst;List<AlterOrder> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(AlterOrder.DeCode(o));} catch (IOException ex) {log.info("AlterOrder DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertAlterOrder(ls);}else if(oo==2){rst=dBProvider.deleteAlterOrder(ls);}else{rst=dBProvider.updateAlterOrder(ls);}return rst;}
public MsgResult operateBailTemp(RequestBatchOperateDB ro){MsgResult rst;List<BailTemp> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(BailTemp.DeCode(o));} catch (IOException ex) {log.info("BailTemp DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertBailTemp(ls);}else if(oo==2){rst=dBProvider.deleteBailTemp(ls);}else{rst=dBProvider.updateBailTemp(ls);}return rst;}
public MsgResult operateBailTempObj(RequestBatchOperateDB ro){MsgResult rst;List<BailTempObj> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(BailTempObj.DeCode(o));} catch (IOException ex) {log.info("BailTempObj DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertBailTempObj(ls);}else if(oo==2){rst=dBProvider.deleteBailTempObj(ls);}else{rst=dBProvider.updateBailTempObj(ls);}return rst;}
public MsgResult operateCancelOrder(RequestBatchOperateDB ro){MsgResult rst;List<CancelOrder> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(CancelOrder.DeCode(o));} catch (IOException ex) {log.info("CancelOrder DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertCancelOrder(ls);}else if(oo==2){rst=dBProvider.deleteCancelOrder(ls);}else{rst=dBProvider.updateCancelOrder(ls);}return rst;}
public MsgResult operateClearingCurrencyRates(RequestBatchOperateDB ro){MsgResult rst;List<ClearingCurrencyRates> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ClearingCurrencyRates.DeCode(o));} catch (IOException ex) {log.info("ClearingCurrencyRates DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertClearingCurrencyRates(ls);}else if(oo==2){rst=dBProvider.deleteClearingCurrencyRates(ls);}else{rst=dBProvider.updateClearingCurrencyRates(ls);}return rst;}
public MsgResult operateClearingDayBook(RequestBatchOperateDB ro){MsgResult rst;List<ClearingDayBook> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ClearingDayBook.DeCode(o));} catch (IOException ex) {log.info("ClearingDayBook DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertClearingDayBook(ls);}else if(oo==2){rst=dBProvider.deleteClearingDayBook(ls);}else{rst=dBProvider.updateClearingDayBook(ls);}return rst;}
public MsgResult operateClearingExRight(RequestBatchOperateDB ro){MsgResult rst;List<ClearingExRight> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ClearingExRight.DeCode(o));} catch (IOException ex) {log.info("ClearingExRight DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertClearingExRight(ls);}else if(oo==2){rst=dBProvider.deleteClearingExRight(ls);}else{rst=dBProvider.updateClearingExRight(ls);}return rst;}
public MsgResult operateClearingPrice(RequestBatchOperateDB ro){MsgResult rst;List<ClearingPrice> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ClearingPrice.DeCode(o));} catch (IOException ex) {log.info("ClearingPrice DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertClearingPrice(ls);}else if(oo==2){rst=dBProvider.deleteClearingPrice(ls);}else{rst=dBProvider.updateClearingPrice(ls);}return rst;}
public MsgResult operateClearingProtfoliopos(RequestBatchOperateDB ro){MsgResult rst;List<ClearingProtfoliopos> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ClearingProtfoliopos.DeCode(o));} catch (IOException ex) {log.info("ClearingProtfoliopos DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertClearingProtfoliopos(ls);}else if(oo==2){rst=dBProvider.deleteClearingProtfoliopos(ls);}else{rst=dBProvider.updateClearingProtfoliopos(ls);}return rst;}
public MsgResult operateFareTemp(RequestBatchOperateDB ro){MsgResult rst;List<FareTemp> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(FareTemp.DeCode(o));} catch (IOException ex) {log.info("FareTemp DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertFareTemp(ls);}else if(oo==2){rst=dBProvider.deleteFareTemp(ls);}else{rst=dBProvider.updateFareTemp(ls);}return rst;}
public MsgResult operateFareTempeObj(RequestBatchOperateDB ro){MsgResult rst;List<FareTempeObj> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(FareTempeObj.DeCode(o));} catch (IOException ex) {log.info("FareTempeObj DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertFareTempeObj(ls);}else if(oo==2){rst=dBProvider.deleteFareTempeObj(ls);}else{rst=dBProvider.updateFareTempeObj(ls);}return rst;}
public MsgResult operateFundState(RequestBatchOperateDB ro){MsgResult rst;List<FundState> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(FundState.DeCode(o));} catch (IOException ex) {log.info("FundState DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertFundState(ls);}else if(oo==2){rst=dBProvider.deleteFundState(ls);}else{rst=dBProvider.updateFundState(ls);}return rst;}
public MsgResult operateFundTransfer(RequestBatchOperateDB ro){MsgResult rst;List<FundTransfer> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(FundTransfer.DeCode(o));} catch (IOException ex) {log.info("FundTransfer DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertFundTransfer(ls);}else if(oo==2){rst=dBProvider.deleteFundTransfer(ls);}else{rst=dBProvider.updateFundTransfer(ls);}return rst;}
public MsgResult operateIOPVAccount(RequestBatchOperateDB ro){MsgResult rst;List<IOPVAccount> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(IOPVAccount.DeCode(o));} catch (IOException ex) {log.info("IOPVAccount DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertIOPVAccount(ls);}else if(oo==2){rst=dBProvider.deleteIOPVAccount(ls);}else{rst=dBProvider.updateIOPVAccount(ls);}return rst;}
public MsgResult operateIOPVProduct(RequestBatchOperateDB ro){MsgResult rst;List<IOPVProduct> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(IOPVProduct.DeCode(o));} catch (IOException ex) {log.info("IOPVProduct DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertIOPVProduct(ls);}else if(oo==2){rst=dBProvider.deleteIOPVProduct(ls);}else{rst=dBProvider.updateIOPVProduct(ls);}return rst;}
public MsgResult operateIOPVProductUnit(RequestBatchOperateDB ro){MsgResult rst;List<IOPVProductUnit> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(IOPVProductUnit.DeCode(o));} catch (IOException ex) {log.info("IOPVProductUnit DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertIOPVProductUnit(ls);}else if(oo==2){rst=dBProvider.deleteIOPVProductUnit(ls);}else{rst=dBProvider.updateIOPVProductUnit(ls);}return rst;}
public MsgResult operateLogLoginInfo(RequestBatchOperateDB ro){MsgResult rst;List<LogLoginInfo> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(LogLoginInfo.DeCode(o));} catch (IOException ex) {log.info("LogLoginInfo DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertLogLoginInfo(ls);}else if(oo==2){rst=dBProvider.deleteLogLoginInfo(ls);}else{rst=dBProvider.updateLogLoginInfo(ls);}return rst;}
public MsgResult operateNewOrder(RequestBatchOperateDB ro){MsgResult rst;List<NewOrder> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(NewOrder.DeCode(o));} catch (IOException ex) {log.info("NewOrder DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertNewOrder(ls);}else if(oo==2){rst=dBProvider.deleteNewOrder(ls);}else{rst=dBProvider.updateNewOrder(ls);}return rst;}
public MsgResult operateObjClassCode(RequestBatchOperateDB ro){MsgResult rst;List<ObjClassCode> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ObjClassCode.DeCode(o));} catch (IOException ex) {log.info("ObjClassCode DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertObjClassCode(ls);}else if(oo==2){rst=dBProvider.deleteObjClassCode(ls);}else{rst=dBProvider.updateObjClassCode(ls);}return rst;}
public MsgResult operateObjClassCodeAtrr(RequestBatchOperateDB ro){MsgResult rst;List<ObjClassCodeAtrr> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ObjClassCodeAtrr.DeCode(o));} catch (IOException ex) {log.info("ObjClassCodeAtrr DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertObjClassCodeAtrr(ls);}else if(oo==2){rst=dBProvider.deleteObjClassCodeAtrr(ls);}else{rst=dBProvider.updateObjClassCodeAtrr(ls);}return rst;}
public MsgResult operateObjETF(RequestBatchOperateDB ro){MsgResult rst;List<ObjETF> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ObjETF.DeCode(o));} catch (IOException ex) {log.info("ObjETF DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertObjETF(ls);}else if(oo==2){rst=dBProvider.deleteObjETF(ls);}else{rst=dBProvider.updateObjETF(ls);}return rst;}
public MsgResult operateObjETFList(RequestBatchOperateDB ro){MsgResult rst;List<ObjETFList> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ObjETFList.DeCode(o));} catch (IOException ex) {log.info("ObjETFList DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertObjETFList(ls);}else if(oo==2){rst=dBProvider.deleteObjETFList(ls);}else{rst=dBProvider.updateObjETFList(ls);}return rst;}
public MsgResult operateObjExRightInfo(RequestBatchOperateDB ro){MsgResult rst;List<ObjExRightInfo> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ObjExRightInfo.DeCode(o));} catch (IOException ex) {log.info("ObjExRightInfo DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertObjExRightInfo(ls);}else if(oo==2){rst=dBProvider.deleteObjExRightInfo(ls);}else{rst=dBProvider.updateObjExRightInfo(ls);}return rst;}
public MsgResult operateObjIndex(RequestBatchOperateDB ro){MsgResult rst;List<ObjIndex> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ObjIndex.DeCode(o));} catch (IOException ex) {log.info("ObjIndex DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertObjIndex(ls);}else if(oo==2){rst=dBProvider.deleteObjIndex(ls);}else{rst=dBProvider.updateObjIndex(ls);}return rst;}
public MsgResult operateObjIndexList(RequestBatchOperateDB ro){MsgResult rst;List<ObjIndexList> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ObjIndexList.DeCode(o));} catch (IOException ex) {log.info("ObjIndexList DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertObjIndexList(ls);}else if(oo==2){rst=dBProvider.deleteObjIndexList(ls);}else{rst=dBProvider.updateObjIndexList(ls);}return rst;}
public MsgResult operateObjInfo(RequestBatchOperateDB ro){MsgResult rst;List<ObjInfo> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ObjInfo.DeCode(o));} catch (IOException ex) {log.info("ObjInfo DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertObjInfo(ls);}else if(oo==2){rst=dBProvider.deleteObjInfo(ls);}else{rst=dBProvider.updateObjInfo(ls);}return rst;}
public MsgResult operateObjNewStock(RequestBatchOperateDB ro){MsgResult rst;List<ObjNewStock> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ObjNewStock.DeCode(o));} catch (IOException ex) {log.info("ObjNewStock DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertObjNewStock(ls);}else if(oo==2){rst=dBProvider.deleteObjNewStock(ls);}else{rst=dBProvider.updateObjNewStock(ls);}return rst;}
public MsgResult operateObjPriceTick(RequestBatchOperateDB ro){MsgResult rst;List<ObjPriceTick> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ObjPriceTick.DeCode(o));} catch (IOException ex) {log.info("ObjPriceTick DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertObjPriceTick(ls);}else if(oo==2){rst=dBProvider.deleteObjPriceTick(ls);}else{rst=dBProvider.updateObjPriceTick(ls);}return rst;}
public MsgResult operateObjProtfolio(RequestBatchOperateDB ro){MsgResult rst;List<ObjProtfolio> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ObjProtfolio.DeCode(o));} catch (IOException ex) {log.info("ObjProtfolio DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertObjProtfolio(ls);}else if(oo==2){rst=dBProvider.deleteObjProtfolio(ls);}else{rst=dBProvider.updateObjProtfolio(ls);}return rst;}
public MsgResult operateObjProtfolioList(RequestBatchOperateDB ro){MsgResult rst;List<ObjProtfolioList> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ObjProtfolioList.DeCode(o));} catch (IOException ex) {log.info("ObjProtfolioList DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertObjProtfolioList(ls);}else if(oo==2){rst=dBProvider.deleteObjProtfolioList(ls);}else{rst=dBProvider.updateObjProtfolioList(ls);}return rst;}
public MsgResult operateObjStructuredFundBase(RequestBatchOperateDB ro){MsgResult rst;List<ObjStructuredFundBase> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ObjStructuredFundBase.DeCode(o));} catch (IOException ex) {log.info("ObjStructuredFundBase DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertObjStructuredFundBase(ls);}else if(oo==2){rst=dBProvider.deleteObjStructuredFundBase(ls);}else{rst=dBProvider.updateObjStructuredFundBase(ls);}return rst;}
public MsgResult operateObjStructuredFundPart(RequestBatchOperateDB ro){MsgResult rst;List<ObjStructuredFundPart> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ObjStructuredFundPart.DeCode(o));} catch (IOException ex) {log.info("ObjStructuredFundPart DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertObjStructuredFundPart(ls);}else if(oo==2){rst=dBProvider.deleteObjStructuredFundPart(ls);}else{rst=dBProvider.updateObjStructuredFundPart(ls);}return rst;}
public MsgResult operateObjUnderlyingCode(RequestBatchOperateDB ro){MsgResult rst;List<ObjUnderlyingCode> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ObjUnderlyingCode.DeCode(o));} catch (IOException ex) {log.info("ObjUnderlyingCode DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertObjUnderlyingCode(ls);}else if(oo==2){rst=dBProvider.deleteObjUnderlyingCode(ls);}else{rst=dBProvider.updateObjUnderlyingCode(ls);}return rst;}
public MsgResult operateOrderExecuteProgress(RequestBatchOperateDB ro){MsgResult rst;List<OrderExecuteProgress> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(OrderExecuteProgress.DeCode(o));} catch (IOException ex) {log.info("OrderExecuteProgress DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertOrderExecuteProgress(ls);}else if(oo==2){rst=dBProvider.deleteOrderExecuteProgress(ls);}else{rst=dBProvider.updateOrderExecuteProgress(ls);}return rst;}
public MsgResult operatePosDetail(RequestBatchOperateDB ro){MsgResult rst;List<PosDetail> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(PosDetail.DeCode(o));} catch (IOException ex) {log.info("PosDetail DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertPosDetail(ls);}else if(oo==2){rst=dBProvider.deletePosDetail(ls);}else{rst=dBProvider.updatePosDetail(ls);}return rst;}
public MsgResult operateProductAccount(RequestBatchOperateDB ro){MsgResult rst;List<ProductAccount> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ProductAccount.DeCode(o));} catch (IOException ex) {log.info("ProductAccount DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertProductAccount(ls);}else if(oo==2){rst=dBProvider.deleteProductAccount(ls);}else{rst=dBProvider.updateProductAccount(ls);}return rst;}
public MsgResult operateProductInfo(RequestBatchOperateDB ro){MsgResult rst;List<ProductInfo> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ProductInfo.DeCode(o));} catch (IOException ex) {log.info("ProductInfo DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertProductInfo(ls);}else if(oo==2){rst=dBProvider.deleteProductInfo(ls);}else{rst=dBProvider.updateProductInfo(ls);}return rst;}
public MsgResult operateProductUnit(RequestBatchOperateDB ro){MsgResult rst;List<ProductUnit> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(ProductUnit.DeCode(o));} catch (IOException ex) {log.info("ProductUnit DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertProductUnit(ls);}else if(oo==2){rst=dBProvider.deleteProductUnit(ls);}else{rst=dBProvider.updateProductUnit(ls);}return rst;}
public MsgResult operateRiskCtrlAssets(RequestBatchOperateDB ro){MsgResult rst;List<RiskCtrlAssets> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(RiskCtrlAssets.DeCode(o));} catch (IOException ex) {log.info("RiskCtrlAssets DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertRiskCtrlAssets(ls);}else if(oo==2){rst=dBProvider.deleteRiskCtrlAssets(ls);}else{rst=dBProvider.updateRiskCtrlAssets(ls);}return rst;}
public MsgResult operateRiskIndex(RequestBatchOperateDB ro){MsgResult rst;List<RiskIndex> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(RiskIndex.DeCode(o));} catch (IOException ex) {log.info("RiskIndex DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertRiskIndex(ls);}else if(oo==2){rst=dBProvider.deleteRiskIndex(ls);}else{rst=dBProvider.updateRiskIndex(ls);}return rst;}
public MsgResult operateRiskSets(RequestBatchOperateDB ro){MsgResult rst;List<RiskSets> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(RiskSets.DeCode(o));} catch (IOException ex) {log.info("RiskSets DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertRiskSets(ls);}else if(oo==2){rst=dBProvider.deleteRiskSets(ls);}else{rst=dBProvider.updateRiskSets(ls);}return rst;}
public MsgResult operateSafeForbidLogin(RequestBatchOperateDB ro){MsgResult rst;List<SafeForbidLogin> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(SafeForbidLogin.DeCode(o));} catch (IOException ex) {log.info("SafeForbidLogin DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertSafeForbidLogin(ls);}else if(oo==2){rst=dBProvider.deleteSafeForbidLogin(ls);}else{rst=dBProvider.updateSafeForbidLogin(ls);}return rst;}
public MsgResult operateSafeOtherAccess(RequestBatchOperateDB ro){MsgResult rst;List<SafeOtherAccess> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(SafeOtherAccess.DeCode(o));} catch (IOException ex) {log.info("SafeOtherAccess DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertSafeOtherAccess(ls);}else if(oo==2){rst=dBProvider.deleteSafeOtherAccess(ls);}else{rst=dBProvider.updateSafeOtherAccess(ls);}return rst;}
public MsgResult operateTradeReportDetail(RequestBatchOperateDB ro){MsgResult rst;List<TradeReportDetail> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(TradeReportDetail.DeCode(o));} catch (IOException ex) {log.info("TradeReportDetail DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertTradeReportDetail(ls);}else if(oo==2){rst=dBProvider.deleteTradeReportDetail(ls);}else{rst=dBProvider.updateTradeReportDetail(ls);}return rst;}
public MsgResult operateTradesetBroker(RequestBatchOperateDB ro){MsgResult rst;List<TradesetBroker> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(TradesetBroker.DeCode(o));} catch (IOException ex) {log.info("TradesetBroker DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertTradesetBroker(ls);}else if(oo==2){rst=dBProvider.deleteTradesetBroker(ls);}else{rst=dBProvider.updateTradesetBroker(ls);}return rst;}
public MsgResult operateTradesetCalendar(RequestBatchOperateDB ro){MsgResult rst;List<TradesetCalendar> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(TradesetCalendar.DeCode(o));} catch (IOException ex) {log.info("TradesetCalendar DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertTradesetCalendar(ls);}else if(oo==2){rst=dBProvider.deleteTradesetCalendar(ls);}else{rst=dBProvider.updateTradesetCalendar(ls);}return rst;}
public MsgResult operateTradesetExchangeLimit(RequestBatchOperateDB ro){MsgResult rst;List<TradesetExchangeLimit> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(TradesetExchangeLimit.DeCode(o));} catch (IOException ex) {log.info("TradesetExchangeLimit DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertTradesetExchangeLimit(ls);}else if(oo==2){rst=dBProvider.deleteTradesetExchangeLimit(ls);}else{rst=dBProvider.updateTradesetExchangeLimit(ls);}return rst;}
public MsgResult operateTradesetPip(RequestBatchOperateDB ro){MsgResult rst;List<TradesetPip> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(TradesetPip.DeCode(o));} catch (IOException ex) {log.info("TradesetPip DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertTradesetPip(ls);}else if(oo==2){rst=dBProvider.deleteTradesetPip(ls);}else{rst=dBProvider.updateTradesetPip(ls);}return rst;}
public MsgResult operateTradeSimOrderQuene(RequestBatchOperateDB ro){MsgResult rst;List<TradeSimOrderQuene> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(TradeSimOrderQuene.DeCode(o));} catch (IOException ex) {log.info("TradeSimOrderQuene DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertTradeSimOrderQuene(ls);}else if(oo==2){rst=dBProvider.deleteTradeSimOrderQuene(ls);}else{rst=dBProvider.updateTradeSimOrderQuene(ls);}return rst;}
public MsgResult operateUserAccess(RequestBatchOperateDB ro){MsgResult rst;List<UserAccess> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(UserAccess.DeCode(o));} catch (IOException ex) {log.info("UserAccess DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertUserAccess(ls);}else if(oo==2){rst=dBProvider.deleteUserAccess(ls);}else{rst=dBProvider.updateUserAccess(ls);}return rst;}
public MsgResult operateUserAccount(RequestBatchOperateDB ro){MsgResult rst;List<UserAccount> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(UserAccount.DeCode(o));} catch (IOException ex) {log.info("UserAccount DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertUserAccount(ls);}else if(oo==2){rst=dBProvider.deleteUserAccount(ls);}else{rst=dBProvider.updateUserAccount(ls);}return rst;}
public MsgResult operateUserInfo(RequestBatchOperateDB ro){MsgResult rst;List<UserInfo> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(UserInfo.DeCode(o));} catch (IOException ex) {log.info("UserInfo DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertUserInfo(ls);}else if(oo==2){rst=dBProvider.deleteUserInfo(ls);}else{rst=dBProvider.updateUserInfo(ls);}return rst;}
public MsgResult operateUserPrivateInfo(RequestBatchOperateDB ro){MsgResult rst;List<UserPrivateInfo> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(UserPrivateInfo.DeCode(o));} catch (IOException ex) {log.info("UserPrivateInfo DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertUserPrivateInfo(ls);}else if(oo==2){rst=dBProvider.deleteUserPrivateInfo(ls);}else{rst=dBProvider.updateUserPrivateInfo(ls);}return rst;}
public MsgResult operateWorkFlowPath(RequestBatchOperateDB ro){MsgResult rst;List<WorkFlowPath> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(WorkFlowPath.DeCode(o));} catch (IOException ex) {log.info("WorkFlowPath DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertWorkFlowPath(ls);}else if(oo==2){rst=dBProvider.deleteWorkFlowPath(ls);}else{rst=dBProvider.updateWorkFlowPath(ls);}return rst;}
public MsgResult operateWorkGroup(RequestBatchOperateDB ro){MsgResult rst;List<WorkGroup> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(WorkGroup.DeCode(o));} catch (IOException ex) {log.info("WorkGroup DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertWorkGroup(ls);}else if(oo==2){rst=dBProvider.deleteWorkGroup(ls);}else{rst=dBProvider.updateWorkGroup(ls);}return rst;}
public MsgResult operateWorkGroupUser(RequestBatchOperateDB ro){MsgResult rst;List<WorkGroupUser> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(WorkGroupUser.DeCode(o));} catch (IOException ex) {log.info("WorkGroupUser DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertWorkGroupUser(ls);}else if(oo==2){rst=dBProvider.deleteWorkGroupUser(ls);}else{rst=dBProvider.updateWorkGroupUser(ls);}return rst;}
public MsgResult operateWorkProductFlow(RequestBatchOperateDB ro){MsgResult rst;List<WorkProductFlow> ls = new ArrayList<>(ro.getDataList().size());ro.getDataList().stream().forEach((byte[] o) -> {try {ls.add(WorkProductFlow.DeCode(o));} catch (IOException ex) {log.info("WorkProductFlow DeCode fail:"+ ex);}});int oo = ro.getInstructionID();if (00==1){rst=dBProvider.insertWorkProductFlow(ls);}else if(oo==2){rst=dBProvider.deleteWorkProductFlow(ls);}else{rst=dBProvider.updateWorkProductFlow(ls);}return rst;}
    

public MsgResult operateAccountBail(int flag, AccountBail data) {return dBProvider.operateAccountBail(flag,data);}
public MsgResult operateAccountFare(int flag, AccountFare data) {return dBProvider.operateAccountFare(flag,data);}
public MsgResult operateAccountIcode(int flag, AccountIcode data) {return dBProvider.operateAccountIcode(flag,data);}
public MsgResult operateAccountInfo(int flag, AccountInfo data) {return dBProvider.operateAccountInfo(flag,data);}
public MsgResult operateAccountInvest(int flag, AccountInvest data) {return dBProvider.operateAccountInvest(flag,data);}
public MsgResult operateAlterOrder(int flag, AlterOrder data) {return dBProvider.operateAlterOrder(flag,data);}
public MsgResult operateBailTemp(int flag, BailTemp data) {return dBProvider.operateBailTemp(flag,data);}
public MsgResult operateBailTempObj(int flag, BailTempObj data) {return dBProvider.operateBailTempObj(flag,data);}
public MsgResult operateCancelOrder(int flag, CancelOrder data) {return dBProvider.operateCancelOrder(flag,data);}
public MsgResult operateClearingCurrencyRates(int flag, ClearingCurrencyRates data) {return dBProvider.operateClearingCurrencyRates(flag,data);}
public MsgResult operateClearingDayBook(int flag, ClearingDayBook data) {return dBProvider.operateClearingDayBook(flag,data);}
public MsgResult operateClearingExRight(int flag, ClearingExRight data) {return dBProvider.operateClearingExRight(flag,data);}
public MsgResult operateClearingPrice(int flag, ClearingPrice data) {return dBProvider.operateClearingPrice(flag,data);}
public MsgResult operateClearingProtfoliopos(int flag, ClearingProtfoliopos data) {return dBProvider.operateClearingProtfoliopos(flag,data);}
public MsgResult operateFareTemp(int flag, FareTemp data) {return dBProvider.operateFareTemp(flag,data);}
public MsgResult operateFareTempeObj(int flag, FareTempeObj data) {return dBProvider.operateFareTempeObj(flag,data);}
public MsgResult operateFundState(int flag, FundState data) {return dBProvider.operateFundState(flag,data);}
public MsgResult operateFundTransfer(int flag, FundTransfer data) {return dBProvider.operateFundTransfer(flag,data);}
public MsgResult operateIOPVAccount(int flag, IOPVAccount data) {return dBProvider.operateIOPVAccount(flag,data);}
public MsgResult operateIOPVProduct(int flag, IOPVProduct data) {return dBProvider.operateIOPVProduct(flag,data);}
public MsgResult operateIOPVProductUnit(int flag, IOPVProductUnit data) {return dBProvider.operateIOPVProductUnit(flag,data);}
public MsgResult operateLogLoginInfo(int flag, LogLoginInfo data) {return dBProvider.operateLogLoginInfo(flag,data);}
public MsgResult operateNewOrder(int flag, NewOrder data) {return dBProvider.operateNewOrder(flag,data);}
public MsgResult operateObjClassCode(int flag, ObjClassCode data) {return dBProvider.operateObjClassCode(flag,data);}
public MsgResult operateObjClassCodeAtrr(int flag, ObjClassCodeAtrr data) {return dBProvider.operateObjClassCodeAtrr(flag,data);}
public MsgResult operateObjETF(int flag, ObjETF data) {return dBProvider.operateObjETF(flag,data);}
public MsgResult operateObjETFList(int flag, ObjETFList data) {return dBProvider.operateObjETFList(flag,data);}
public MsgResult operateObjExRightInfo(int flag, ObjExRightInfo data) {return dBProvider.operateObjExRightInfo(flag,data);}
public MsgResult operateObjIndex(int flag, ObjIndex data) {return dBProvider.operateObjIndex(flag,data);}
public MsgResult operateObjIndexList(int flag, ObjIndexList data) {return dBProvider.operateObjIndexList(flag,data);}
public MsgResult operateObjInfo(int flag, ObjInfo data) {return dBProvider.operateObjInfo(flag,data);}
public MsgResult operateObjNewStock(int flag, ObjNewStock data) {return dBProvider.operateObjNewStock(flag,data);}
public MsgResult operateObjPriceTick(int flag, ObjPriceTick data) {return dBProvider.operateObjPriceTick(flag,data);}
public MsgResult operateObjProtfolio(int flag, ObjProtfolio data) {return dBProvider.operateObjProtfolio(flag,data);}
public MsgResult operateObjProtfolioList(int flag, ObjProtfolioList data) {return dBProvider.operateObjProtfolioList(flag,data);}
public MsgResult operateObjStructuredFundBase(int flag, ObjStructuredFundBase data) {return dBProvider.operateObjStructuredFundBase(flag,data);}
public MsgResult operateObjStructuredFundPart(int flag, ObjStructuredFundPart data) {return dBProvider.operateObjStructuredFundPart(flag,data);}
public MsgResult operateObjUnderlyingCode(int flag, ObjUnderlyingCode data) {return dBProvider.operateObjUnderlyingCode(flag,data);}
public MsgResult operateOrderExecuteProgress(int flag, OrderExecuteProgress data) {return dBProvider.operateOrderExecuteProgress(flag,data);}
public MsgResult operatePosDetail(int flag, PosDetail data) {return dBProvider.operatePosDetail(flag,data);}
public MsgResult operateProductAccount(int flag, ProductAccount data) {return dBProvider.operateProductAccount(flag,data);}
public MsgResult operateProductInfo(int flag, ProductInfo data) {return dBProvider.operateProductInfo(flag,data);}
public MsgResult operateProductUnit(int flag, ProductUnit data) {return dBProvider.operateProductUnit(flag,data);}
public MsgResult operateRiskCtrlAssets(int flag, RiskCtrlAssets data) {return dBProvider.operateRiskCtrlAssets(flag,data);}
public MsgResult operateRiskIndex(int flag, RiskIndex data) {return dBProvider.operateRiskIndex(flag,data);}
public MsgResult operateRiskSets(int flag, RiskSets data) {return dBProvider.operateRiskSets(flag,data);}
public MsgResult operateSafeForbidLogin(int flag, SafeForbidLogin data) {return dBProvider.operateSafeForbidLogin(flag,data);}
public MsgResult operateSafeOtherAccess(int flag, SafeOtherAccess data) {return dBProvider.operateSafeOtherAccess(flag,data);}
public MsgResult operateTradeReportDetail(int flag, TradeReportDetail data) {return dBProvider.operateTradeReportDetail(flag,data);}
public MsgResult operateTradesetBroker(int flag, TradesetBroker data) {return dBProvider.operateTradesetBroker(flag,data);}
public MsgResult operateTradesetCalendar(int flag, TradesetCalendar data) {return dBProvider.operateTradesetCalendar(flag,data);}
public MsgResult operateTradesetExchangeLimit(int flag, TradesetExchangeLimit data) {return dBProvider.operateTradesetExchangeLimit(flag,data);}
public MsgResult operateTradesetPip(int flag, TradesetPip data) {return dBProvider.operateTradesetPip(flag,data);}
public MsgResult operateTradeSimOrderQuene(int flag, TradeSimOrderQuene data) {return dBProvider.operateTradeSimOrderQuene(flag,data);}
public MsgResult operateUserAccess(int flag, UserAccess data) {return dBProvider.operateUserAccess(flag,data);}
public MsgResult operateUserAccount(int flag, UserAccount data) {return dBProvider.operateUserAccount(flag,data);}
public MsgResult operateUserInfo(int flag, UserInfo data) {return dBProvider.operateUserInfo(flag,data);}
public MsgResult operateUserPrivateInfo(int flag, UserPrivateInfo data) {return dBProvider.operateUserPrivateInfo(flag,data);}
public MsgResult operateWorkFlowPath(int flag, WorkFlowPath data) {return dBProvider.operateWorkFlowPath(flag,data);}
public MsgResult operateWorkGroup(int flag, WorkGroup data) {return dBProvider.operateWorkGroup(flag,data);}
public MsgResult operateWorkGroupUser(int flag, WorkGroupUser data) {return dBProvider.operateWorkGroupUser(flag,data);}
public MsgResult operateWorkProductFlow(int flag, WorkProductFlow data) {return dBProvider.operateWorkProductFlow(flag,data);}

}
