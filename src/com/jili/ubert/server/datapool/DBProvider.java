/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.server.datapool;

import com.jili.ubert.code.ReturnIDEntity;
import com.jili.ubert.code.server2client.MsgResult;
import com.jili.ubert.dao.DaoBaseMysql;
import com.jili.ubert.dao.db.AccountBail;
import com.jili.ubert.dao.db.AccountFare;
import com.jili.ubert.dao.db.AccountIcode;
import com.jili.ubert.dao.db.AccountInfo;
import com.jili.ubert.dao.db.AccountInvest;
import com.jili.ubert.dao.db.AlterOrder;
import com.jili.ubert.dao.db.BailTemp;
import com.jili.ubert.dao.db.BailTempObj;
import com.jili.ubert.dao.db.CancelOrder;
import com.jili.ubert.dao.db.ClearingCurrencyRates;
import com.jili.ubert.dao.db.ClearingDayBook;
import com.jili.ubert.dao.db.ClearingExRight;
import com.jili.ubert.dao.db.ClearingPrice;
import com.jili.ubert.dao.db.ClearingProtfoliopos;
import com.jili.ubert.dao.db.FareTemp;
import com.jili.ubert.dao.db.FareTempeObj;
import com.jili.ubert.dao.db.FundState;
import com.jili.ubert.dao.db.FundTransfer;
import com.jili.ubert.dao.db.IOPVAccount;
import com.jili.ubert.dao.db.IOPVProduct;
import com.jili.ubert.dao.db.IOPVProductUnit;
import com.jili.ubert.dao.db.LogLoginInfo;
import com.jili.ubert.dao.db.NewOrder;
import com.jili.ubert.dao.db.ObjClassCode;
import com.jili.ubert.dao.db.ObjClassCodeAtrr;
import com.jili.ubert.dao.db.ObjETF;
import com.jili.ubert.dao.db.ObjETFList;
import com.jili.ubert.dao.db.ObjExRightInfo;
import com.jili.ubert.dao.db.ObjIndex;
import com.jili.ubert.dao.db.ObjIndexList;
import com.jili.ubert.dao.db.ObjInfo;
import com.jili.ubert.dao.db.ObjNewStock;
import com.jili.ubert.dao.db.ObjPriceTick;
import com.jili.ubert.dao.db.ObjProtfolio;
import com.jili.ubert.dao.db.ObjProtfolioList;
import com.jili.ubert.dao.db.ObjStructuredFundBase;
import com.jili.ubert.dao.db.ObjStructuredFundPart;
import com.jili.ubert.dao.db.ObjUnderlyingCode;
import com.jili.ubert.dao.db.OrderExecuteProgress;
import com.jili.ubert.dao.db.PosDetail;
import com.jili.ubert.dao.db.ProductAccount;
import com.jili.ubert.dao.db.ProductInfo;
import com.jili.ubert.dao.db.ProductUnit;
import com.jili.ubert.dao.db.RiskCtrlAssets;
import com.jili.ubert.dao.db.RiskIndex;
import com.jili.ubert.dao.db.RiskSets;
import com.jili.ubert.dao.db.SafeForbidLogin;
import com.jili.ubert.dao.db.SafeOtherAccess;
import com.jili.ubert.dao.db.TradeReportDetail;
import com.jili.ubert.dao.db.TradeSimOrderQuene;
import com.jili.ubert.dao.db.TradesetBroker;
import com.jili.ubert.dao.db.TradesetCalendar;
import com.jili.ubert.dao.db.TradesetExchangeLimit;
import com.jili.ubert.dao.db.TradesetPip;
import com.jili.ubert.dao.db.UserAccess;
import com.jili.ubert.dao.db.UserAccount;
import com.jili.ubert.dao.db.UserInfo;
import com.jili.ubert.dao.db.UserPrivateInfo;
import com.jili.ubert.dao.db.WorkFlowPath;
import com.jili.ubert.dao.db.WorkGroup;
import com.jili.ubert.dao.db.WorkGroupUser;
import com.jili.ubert.dao.db.WorkProductFlow;
import com.panemu.tiwulfx.common.TableCriteria;
import com.panemu.tiwulfx.common.TableData;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author ChengJiLi
 */
public class DBProvider {

    private final EntityManagerFactory factory = Persistence.createEntityManagerFactory("ubert_db");
    public EntityManagerFactory getFactory(){
        return factory;
    }
    private final DaoBaseMysql<AccountBail> tableAccountBail = new DaoBaseMysql(AccountBail.class, factory);
    private final DaoBaseMysql<AccountFare> tableAccountFare = new DaoBaseMysql(AccountFare.class, factory);
    private final DaoBaseMysql<AccountIcode> tableAccountIcode = new DaoBaseMysql(AccountIcode.class, factory);
    private final DaoBaseMysql<AccountInfo> tableAccountInfo = new DaoBaseMysql(AccountInfo.class, factory);
    private final DaoBaseMysql<AccountInvest> tableAccountInvest = new DaoBaseMysql(AccountInvest.class, factory);
    private final DaoBaseMysql<AlterOrder> tableAlterOrder = new DaoBaseMysql(AlterOrder.class, factory);
    private final DaoBaseMysql<BailTemp> tableBailTemp = new DaoBaseMysql(BailTemp.class, factory);
    private final DaoBaseMysql<BailTempObj> tableBailTempObj = new DaoBaseMysql(BailTempObj.class, factory);
    private final DaoBaseMysql<CancelOrder> tableCancelOrder = new DaoBaseMysql(CancelOrder.class, factory);
    private final DaoBaseMysql<ClearingCurrencyRates> tableClearingCurrencyRates = new DaoBaseMysql(ClearingCurrencyRates.class, factory);
    private final DaoBaseMysql<ClearingDayBook> tableClearingDayBook = new DaoBaseMysql(ClearingDayBook.class, factory);
    private final DaoBaseMysql<ClearingExRight> tableClearingExRight = new DaoBaseMysql(ClearingExRight.class, factory);
    private final DaoBaseMysql<ClearingPrice> tableClearingPrice = new DaoBaseMysql(ClearingPrice.class, factory);
    private final DaoBaseMysql<ClearingProtfoliopos> tableClearingProtfoliopos = new DaoBaseMysql(ClearingProtfoliopos.class, factory);
    private final DaoBaseMysql<FareTemp> tableFareTemp = new DaoBaseMysql(FareTemp.class, factory);
    private final DaoBaseMysql<FareTempeObj> tableFareTempeObj = new DaoBaseMysql(FareTempeObj.class, factory);
    private final DaoBaseMysql<FundState> tableFundState = new DaoBaseMysql(FundState.class, factory);
    private final DaoBaseMysql<FundTransfer> tableFundTransfer = new DaoBaseMysql(FundTransfer.class, factory);
    private final DaoBaseMysql<IOPVAccount> tableIOPVAccount = new DaoBaseMysql(IOPVAccount.class, factory);
    private final DaoBaseMysql<IOPVProduct> tableIOPVProduct = new DaoBaseMysql(IOPVProduct.class, factory);
    private final DaoBaseMysql<IOPVProductUnit> tableIOPVProductUnit = new DaoBaseMysql(IOPVProductUnit.class, factory);
    private final DaoBaseMysql<LogLoginInfo> tableLogLoginInfo = new DaoBaseMysql(LogLoginInfo.class, factory);
    private final DaoBaseMysql<NewOrder> tableNewOrder = new DaoBaseMysql(NewOrder.class, factory);
    private final DaoBaseMysql<ObjClassCode> tableObjClassCode = new DaoBaseMysql(ObjClassCode.class, factory);
    private final DaoBaseMysql<ObjClassCodeAtrr> tableObjClassCodeAtrr = new DaoBaseMysql(ObjClassCodeAtrr.class, factory);
    private final DaoBaseMysql<ObjETF> tableObjETF = new DaoBaseMysql(ObjETF.class, factory);
    private final DaoBaseMysql<ObjETFList> tableObjETFList = new DaoBaseMysql(ObjETFList.class, factory);
    private final DaoBaseMysql<ObjExRightInfo> tableObjExRightInfo = new DaoBaseMysql(ObjExRightInfo.class, factory);
    private final DaoBaseMysql<ObjIndex> tableObjIndex = new DaoBaseMysql(ObjIndex.class, factory);
    private final DaoBaseMysql<ObjIndexList> tableObjIndexList = new DaoBaseMysql(ObjIndexList.class, factory);
    private final DaoBaseMysql<ObjInfo> tableObjInfo = new DaoBaseMysql(ObjInfo.class, factory);
    private final DaoBaseMysql<ObjNewStock> tableObjNewStock = new DaoBaseMysql(ObjNewStock.class, factory);
    private final DaoBaseMysql<ObjPriceTick> tableObjPriceTick = new DaoBaseMysql(ObjPriceTick.class, factory);
    private final DaoBaseMysql<ObjProtfolio> tableObjProtfolio = new DaoBaseMysql(ObjProtfolio.class, factory);
    private final DaoBaseMysql<ObjProtfolioList> tableObjProtfolioList = new DaoBaseMysql(ObjProtfolioList.class, factory);
    private final DaoBaseMysql<ObjStructuredFundBase> tableObjStructuredFundBase = new DaoBaseMysql(ObjStructuredFundBase.class, factory);
    private final DaoBaseMysql<ObjStructuredFundPart> tableObjStructuredFundPart = new DaoBaseMysql(ObjStructuredFundPart.class, factory);
    private final DaoBaseMysql<ObjUnderlyingCode> tableObjUnderlyingCode = new DaoBaseMysql(ObjUnderlyingCode.class, factory);
    private final DaoBaseMysql<OrderExecuteProgress> tableOrderExecuteProgress = new DaoBaseMysql(OrderExecuteProgress.class, factory);
    private final DaoBaseMysql<PosDetail> tablePosDetail = new DaoBaseMysql(PosDetail.class, factory);
    private final DaoBaseMysql<ProductAccount> tableProductAccount = new DaoBaseMysql(ProductAccount.class, factory);
    private final DaoBaseMysql<ProductInfo> tableProductInfo = new DaoBaseMysql(ProductInfo.class, factory);
    private final DaoBaseMysql<ProductUnit> tableProductUnit = new DaoBaseMysql(ProductUnit.class, factory);
    private final DaoBaseMysql<RiskCtrlAssets> tableRiskCtrlAssets = new DaoBaseMysql(RiskCtrlAssets.class, factory);
    private final DaoBaseMysql<RiskIndex> tableRiskIndex = new DaoBaseMysql(RiskIndex.class, factory);
    private final DaoBaseMysql<RiskSets> tableRiskSets = new DaoBaseMysql(RiskSets.class, factory);
    private final DaoBaseMysql<SafeForbidLogin> tableSafeForbidLogin = new DaoBaseMysql(SafeForbidLogin.class, factory);
    private final DaoBaseMysql<SafeOtherAccess> tableSafeOtherAccess = new DaoBaseMysql(SafeOtherAccess.class, factory);
    private final DaoBaseMysql<TradeReportDetail> tableTradeReportDetail = new DaoBaseMysql(TradeReportDetail.class, factory);
    private final DaoBaseMysql<TradesetBroker> tableTradesetBroker = new DaoBaseMysql(TradesetBroker.class, factory);
    private final DaoBaseMysql<TradesetCalendar> tableTradesetCalendar = new DaoBaseMysql(TradesetCalendar.class, factory);
    private final DaoBaseMysql<TradesetExchangeLimit> tableTradesetExchangeLimit = new DaoBaseMysql(TradesetExchangeLimit.class, factory);
    private final DaoBaseMysql<TradesetPip> tableTradesetPip = new DaoBaseMysql(TradesetPip.class, factory);
    private final DaoBaseMysql<TradeSimOrderQuene> tableTradeSimOrderQuene = new DaoBaseMysql(TradeSimOrderQuene.class, factory);
    private final DaoBaseMysql<UserAccess> tableUserAccess = new DaoBaseMysql(UserAccess.class, factory);
    private final DaoBaseMysql<UserAccount> tableUserAccount = new DaoBaseMysql(UserAccount.class, factory);
    private final DaoBaseMysql<UserInfo> tableUserInfo = new DaoBaseMysql(UserInfo.class, factory);
    private final DaoBaseMysql<UserPrivateInfo> tableUserPrivateInfo = new DaoBaseMysql(UserPrivateInfo.class, factory);
    private final DaoBaseMysql<WorkFlowPath> tableWorkFlowPath = new DaoBaseMysql(WorkFlowPath.class, factory);
    private final DaoBaseMysql<WorkGroup> tableWorkGroup = new DaoBaseMysql(WorkGroup.class, factory);
    private final DaoBaseMysql<WorkGroupUser> tableWorkGroupUser = new DaoBaseMysql(WorkGroupUser.class, factory);
    private final DaoBaseMysql<WorkProductFlow> tableWorkProductFlow = new DaoBaseMysql(WorkProductFlow.class, factory);

    public TableData<AccountBail> queryAccountBail(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableAccountBail.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<AccountFare> queryAccountFare(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableAccountFare.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<AccountIcode> queryAccountIcode(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableAccountIcode.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<AccountInfo> queryAccountInfo(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableAccountInfo.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<AccountInvest> queryAccountInvest(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableAccountInvest.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<AlterOrder> queryAlterOrder(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableAlterOrder.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<BailTemp> queryBailTemp(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableBailTemp.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<BailTempObj> queryBailTempObj(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableBailTempObj.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<CancelOrder> queryCancelOrder(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableCancelOrder.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ClearingCurrencyRates> queryClearingCurrencyRates(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableClearingCurrencyRates.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ClearingDayBook> queryClearingDayBook(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableClearingDayBook.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ClearingExRight> queryClearingExRight(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableClearingExRight.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ClearingPrice> queryClearingPrice(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableClearingPrice.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ClearingProtfoliopos> queryClearingProtfoliopos(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableClearingProtfoliopos.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<FareTemp> queryFareTemp(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableFareTemp.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<FareTempeObj> queryFareTempeObj(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableFareTempeObj.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<FundState> queryFundState(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableFundState.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<FundTransfer> queryFundTransfer(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableFundTransfer.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<IOPVAccount> queryIOPVAccount(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableIOPVAccount.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<IOPVProduct> queryIOPVProduct(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableIOPVProduct.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<IOPVProductUnit> queryIOPVProductUnit(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableIOPVProductUnit.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<LogLoginInfo> queryLogLoginInfo(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableLogLoginInfo.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<NewOrder> queryNewOrder(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableNewOrder.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ObjClassCode> queryObjClassCode(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableObjClassCode.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ObjClassCodeAtrr> queryObjClassCodeAtrr(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableObjClassCodeAtrr.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ObjETF> queryObjETF(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableObjETF.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ObjETFList> queryObjETFList(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableObjETFList.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ObjExRightInfo> queryObjExRightInfo(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableObjExRightInfo.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ObjIndex> queryObjIndex(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableObjIndex.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ObjIndexList> queryObjIndexList(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableObjIndexList.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ObjInfo> queryObjInfo(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableObjInfo.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ObjNewStock> queryObjNewStock(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableObjNewStock.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ObjPriceTick> queryObjPriceTick(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableObjPriceTick.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ObjProtfolio> queryObjProtfolio(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableObjProtfolio.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ObjProtfolioList> queryObjProtfolioList(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableObjProtfolioList.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ObjStructuredFundBase> queryObjStructuredFundBase(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableObjStructuredFundBase.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ObjStructuredFundPart> queryObjStructuredFundPart(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableObjStructuredFundPart.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ObjUnderlyingCode> queryObjUnderlyingCode(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableObjUnderlyingCode.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<OrderExecuteProgress> queryOrderExecuteProgress(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableOrderExecuteProgress.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<PosDetail> queryPosDetail(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tablePosDetail.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ProductAccount> queryProductAccount(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableProductAccount.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ProductInfo> queryProductInfo(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableProductInfo.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<ProductUnit> queryProductUnit(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableProductUnit.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<RiskCtrlAssets> queryRiskCtrlAssets(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableRiskCtrlAssets.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<RiskIndex> queryRiskIndex(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableRiskIndex.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<RiskSets> queryRiskSets(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableRiskSets.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<SafeForbidLogin> querySafeForbidLogin(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableSafeForbidLogin.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<SafeOtherAccess> querySafeOtherAccess(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableSafeOtherAccess.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<TradeReportDetail> queryTradeReportDetail(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableTradeReportDetail.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<TradesetBroker> queryTradesetBroker(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableTradesetBroker.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<TradesetCalendar> queryTradesetCalendar(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableTradesetCalendar.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<TradesetExchangeLimit> queryTradesetExchangeLimit(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableTradesetExchangeLimit.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<TradesetPip> queryTradesetPip(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableTradesetPip.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<TradeSimOrderQuene> queryTradeSimOrderQuene(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableTradeSimOrderQuene.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<UserAccess> queryUserAccess(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableUserAccess.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<UserAccount> queryUserAccount(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableUserAccount.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<UserInfo> queryUserInfo(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableUserInfo.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<UserPrivateInfo> queryUserPrivateInfo(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableUserPrivateInfo.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<WorkFlowPath> queryWorkFlowPath(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableWorkFlowPath.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<WorkGroup> queryWorkGroup(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableWorkGroup.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<WorkGroupUser> queryWorkGroupUser(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableWorkGroupUser.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public TableData<WorkProductFlow> queryWorkProductFlow(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<Boolean> sortingVersus, int maxResult) {
        return tableWorkProductFlow.fetch(startIndex, filteredColumns, sortedColumns, sortingVersus, maxResult);
    }

    public MsgResult insertAccountBail(List<AccountBail> ls) {
        MsgResult rst;
        try {
            tableAccountBail.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertAccountFare(List<AccountFare> ls) {
        MsgResult rst;
        try {
            tableAccountFare.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertAccountIcode(List<AccountIcode> ls) {
        MsgResult rst;
        try {
            tableAccountIcode.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertAccountInfo(List<AccountInfo> ls) {
        MsgResult rst;
        try {
            tableAccountInfo.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertAccountInvest(List<AccountInvest> ls) {
        MsgResult rst;
        try {
            tableAccountInvest.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertAlterOrder(List<AlterOrder> ls) {
        MsgResult rst;
        try {
            tableAlterOrder.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertBailTemp(List<BailTemp> ls) {
        MsgResult rst;
        try {
            tableBailTemp.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertBailTempObj(List<BailTempObj> ls) {
        MsgResult rst;
        try {
            tableBailTempObj.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertCancelOrder(List<CancelOrder> ls) {
        MsgResult rst;
        try {
            tableCancelOrder.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertClearingCurrencyRates(List<ClearingCurrencyRates> ls) {
        MsgResult rst;
        try {
            tableClearingCurrencyRates.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertClearingDayBook(List<ClearingDayBook> ls) {
        MsgResult rst;
        try {
            tableClearingDayBook.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertClearingExRight(List<ClearingExRight> ls) {
        MsgResult rst;
        try {
            tableClearingExRight.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertClearingPrice(List<ClearingPrice> ls) {
        MsgResult rst;
        try {
            tableClearingPrice.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertClearingProtfoliopos(List<ClearingProtfoliopos> ls) {
        MsgResult rst;
        try {
            tableClearingProtfoliopos.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertFareTemp(List<FareTemp> ls) {
        MsgResult rst;
        try {
            tableFareTemp.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertFareTempeObj(List<FareTempeObj> ls) {
        MsgResult rst;
        try {
            tableFareTempeObj.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertFundState(List<FundState> ls) {
        MsgResult rst;
        try {
            tableFundState.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertFundTransfer(List<FundTransfer> ls) {
        MsgResult rst;
        try {
            tableFundTransfer.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertIOPVAccount(List<IOPVAccount> ls) {
        MsgResult rst;
        try {
            tableIOPVAccount.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertIOPVProduct(List<IOPVProduct> ls) {
        MsgResult rst;
        try {
            tableIOPVProduct.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertIOPVProductUnit(List<IOPVProductUnit> ls) {
        MsgResult rst;
        try {
            tableIOPVProductUnit.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertLogLoginInfo(List<LogLoginInfo> ls) {
        MsgResult rst;
        try {
            tableLogLoginInfo.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertNewOrder(List<NewOrder> ls) {
        MsgResult rst;
        try {
            tableNewOrder.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertObjClassCode(List<ObjClassCode> ls) {
        MsgResult rst;
        try {
            tableObjClassCode.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertObjClassCodeAtrr(List<ObjClassCodeAtrr> ls) {
        MsgResult rst;
        try {
            tableObjClassCodeAtrr.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertObjETF(List<ObjETF> ls) {
        MsgResult rst;
        try {
            tableObjETF.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertObjETFList(List<ObjETFList> ls) {
        MsgResult rst;
        try {
            tableObjETFList.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertObjExRightInfo(List<ObjExRightInfo> ls) {
        MsgResult rst;
        try {
            tableObjExRightInfo.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertObjIndex(List<ObjIndex> ls) {
        MsgResult rst;
        try {
            tableObjIndex.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertObjIndexList(List<ObjIndexList> ls) {
        MsgResult rst;
        try {
            tableObjIndexList.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertObjInfo(List<ObjInfo> ls) {
        MsgResult rst;
        try {
            tableObjInfo.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertObjNewStock(List<ObjNewStock> ls) {
        MsgResult rst;
        try {
            tableObjNewStock.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertObjPriceTick(List<ObjPriceTick> ls) {
        MsgResult rst;
        try {
            tableObjPriceTick.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertObjProtfolio(List<ObjProtfolio> ls) {
        MsgResult rst;
        try {
            tableObjProtfolio.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertObjProtfolioList(List<ObjProtfolioList> ls) {
        MsgResult rst;
        try {
            tableObjProtfolioList.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertObjStructuredFundBase(List<ObjStructuredFundBase> ls) {
        MsgResult rst;
        try {
            tableObjStructuredFundBase.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertObjStructuredFundPart(List<ObjStructuredFundPart> ls) {
        MsgResult rst;
        try {
            tableObjStructuredFundPart.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertObjUnderlyingCode(List<ObjUnderlyingCode> ls) {
        MsgResult rst;
        try {
            tableObjUnderlyingCode.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertOrderExecuteProgress(List<OrderExecuteProgress> ls) {
        MsgResult rst;
        try {
            tableOrderExecuteProgress.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertPosDetail(List<PosDetail> ls) {
        MsgResult rst;
        try {
            tablePosDetail.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertProductAccount(List<ProductAccount> ls) {
        MsgResult rst;
        try {
            tableProductAccount.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertProductInfo(List<ProductInfo> ls) {
        MsgResult rst;
        try {
            tableProductInfo.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertProductUnit(List<ProductUnit> ls) {
        MsgResult rst;
        try {
            tableProductUnit.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertRiskCtrlAssets(List<RiskCtrlAssets> ls) {
        MsgResult rst;
        try {
            tableRiskCtrlAssets.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertRiskIndex(List<RiskIndex> ls) {
        MsgResult rst;
        try {
            tableRiskIndex.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertRiskSets(List<RiskSets> ls) {
        MsgResult rst;
        try {
            tableRiskSets.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertSafeForbidLogin(List<SafeForbidLogin> ls) {
        MsgResult rst;
        try {
            tableSafeForbidLogin.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertSafeOtherAccess(List<SafeOtherAccess> ls) {
        MsgResult rst;
        try {
            tableSafeOtherAccess.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertTradeReportDetail(List<TradeReportDetail> ls) {
        MsgResult rst;
        try {
            tableTradeReportDetail.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertTradesetBroker(List<TradesetBroker> ls) {
        MsgResult rst;
        try {
            tableTradesetBroker.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertTradesetCalendar(List<TradesetCalendar> ls) {
        MsgResult rst;
        try {
            tableTradesetCalendar.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertTradesetExchangeLimit(List<TradesetExchangeLimit> ls) {
        MsgResult rst;
        try {
            tableTradesetExchangeLimit.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertTradesetPip(List<TradesetPip> ls) {
        MsgResult rst;
        try {
            tableTradesetPip.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertTradeSimOrderQuene(List<TradeSimOrderQuene> ls) {
        MsgResult rst;
        try {
            tableTradeSimOrderQuene.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertUserAccess(List<UserAccess> ls) {
        MsgResult rst;
        try {
            tableUserAccess.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertUserAccount(List<UserAccount> ls) {
        MsgResult rst;
        try {
            tableUserAccount.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertUserInfo(List<UserInfo> ls) {
        MsgResult rst;
        try {
            tableUserInfo.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertUserPrivateInfo(List<UserPrivateInfo> ls) {
        MsgResult rst;
        try {
            tableUserPrivateInfo.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertWorkFlowPath(List<WorkFlowPath> ls) {
        MsgResult rst;
        try {
            tableWorkFlowPath.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertWorkGroup(List<WorkGroup> ls) {
        MsgResult rst;
        try {
            tableWorkGroup.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertWorkGroupUser(List<WorkGroupUser> ls) {
        MsgResult rst;
        try {
            tableWorkGroupUser.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

    public MsgResult insertWorkProductFlow(List<WorkProductFlow> ls) {
        MsgResult rst;
        try {
            tableWorkProductFlow.insert(ls);
            rst = ReturnIDEntity.Success;
        } catch (Exception ex) {
            rst = new MsgResult(false, 202, ex.getLocalizedMessage());
        }
        return rst;
    }

public MsgResult updateAccountBail(List<AccountBail> ls){MsgResult rst;try{tableAccountBail.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateAccountFare(List<AccountFare> ls){MsgResult rst;try{tableAccountFare.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateAccountIcode(List<AccountIcode> ls){MsgResult rst;try{tableAccountIcode.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateAccountInfo(List<AccountInfo> ls){MsgResult rst;try{tableAccountInfo.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateAccountInvest(List<AccountInvest> ls){MsgResult rst;try{tableAccountInvest.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateAlterOrder(List<AlterOrder> ls){MsgResult rst;try{tableAlterOrder.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateBailTemp(List<BailTemp> ls){MsgResult rst;try{tableBailTemp.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateBailTempObj(List<BailTempObj> ls){MsgResult rst;try{tableBailTempObj.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateCancelOrder(List<CancelOrder> ls){MsgResult rst;try{tableCancelOrder.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateClearingCurrencyRates(List<ClearingCurrencyRates> ls){MsgResult rst;try{tableClearingCurrencyRates.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateClearingDayBook(List<ClearingDayBook> ls){MsgResult rst;try{tableClearingDayBook.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateClearingExRight(List<ClearingExRight> ls){MsgResult rst;try{tableClearingExRight.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateClearingPrice(List<ClearingPrice> ls){MsgResult rst;try{tableClearingPrice.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateClearingProtfoliopos(List<ClearingProtfoliopos> ls){MsgResult rst;try{tableClearingProtfoliopos.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateFareTemp(List<FareTemp> ls){MsgResult rst;try{tableFareTemp.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateFareTempeObj(List<FareTempeObj> ls){MsgResult rst;try{tableFareTempeObj.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateFundState(List<FundState> ls){MsgResult rst;try{tableFundState.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateFundTransfer(List<FundTransfer> ls){MsgResult rst;try{tableFundTransfer.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateIOPVAccount(List<IOPVAccount> ls){MsgResult rst;try{tableIOPVAccount.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateIOPVProduct(List<IOPVProduct> ls){MsgResult rst;try{tableIOPVProduct.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateIOPVProductUnit(List<IOPVProductUnit> ls){MsgResult rst;try{tableIOPVProductUnit.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateLogLoginInfo(List<LogLoginInfo> ls){MsgResult rst;try{tableLogLoginInfo.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateNewOrder(List<NewOrder> ls){MsgResult rst;try{tableNewOrder.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateObjClassCode(List<ObjClassCode> ls){MsgResult rst;try{tableObjClassCode.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateObjClassCodeAtrr(List<ObjClassCodeAtrr> ls){MsgResult rst;try{tableObjClassCodeAtrr.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateObjETF(List<ObjETF> ls){MsgResult rst;try{tableObjETF.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateObjETFList(List<ObjETFList> ls){MsgResult rst;try{tableObjETFList.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateObjExRightInfo(List<ObjExRightInfo> ls){MsgResult rst;try{tableObjExRightInfo.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateObjIndex(List<ObjIndex> ls){MsgResult rst;try{tableObjIndex.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateObjIndexList(List<ObjIndexList> ls){MsgResult rst;try{tableObjIndexList.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateObjInfo(List<ObjInfo> ls){MsgResult rst;try{tableObjInfo.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateObjNewStock(List<ObjNewStock> ls){MsgResult rst;try{tableObjNewStock.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateObjPriceTick(List<ObjPriceTick> ls){MsgResult rst;try{tableObjPriceTick.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateObjProtfolio(List<ObjProtfolio> ls){MsgResult rst;try{tableObjProtfolio.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateObjProtfolioList(List<ObjProtfolioList> ls){MsgResult rst;try{tableObjProtfolioList.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateObjStructuredFundBase(List<ObjStructuredFundBase> ls){MsgResult rst;try{tableObjStructuredFundBase.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateObjStructuredFundPart(List<ObjStructuredFundPart> ls){MsgResult rst;try{tableObjStructuredFundPart.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateObjUnderlyingCode(List<ObjUnderlyingCode> ls){MsgResult rst;try{tableObjUnderlyingCode.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateOrderExecuteProgress(List<OrderExecuteProgress> ls){MsgResult rst;try{tableOrderExecuteProgress.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updatePosDetail(List<PosDetail> ls){MsgResult rst;try{tablePosDetail.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateProductAccount(List<ProductAccount> ls){MsgResult rst;try{tableProductAccount.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateProductInfo(List<ProductInfo> ls){MsgResult rst;try{tableProductInfo.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateProductUnit(List<ProductUnit> ls){MsgResult rst;try{tableProductUnit.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateRiskCtrlAssets(List<RiskCtrlAssets> ls){MsgResult rst;try{tableRiskCtrlAssets.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateRiskIndex(List<RiskIndex> ls){MsgResult rst;try{tableRiskIndex.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateRiskSets(List<RiskSets> ls){MsgResult rst;try{tableRiskSets.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateSafeForbidLogin(List<SafeForbidLogin> ls){MsgResult rst;try{tableSafeForbidLogin.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateSafeOtherAccess(List<SafeOtherAccess> ls){MsgResult rst;try{tableSafeOtherAccess.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateTradeReportDetail(List<TradeReportDetail> ls){MsgResult rst;try{tableTradeReportDetail.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateTradesetBroker(List<TradesetBroker> ls){MsgResult rst;try{tableTradesetBroker.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateTradesetCalendar(List<TradesetCalendar> ls){MsgResult rst;try{tableTradesetCalendar.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateTradesetExchangeLimit(List<TradesetExchangeLimit> ls){MsgResult rst;try{tableTradesetExchangeLimit.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateTradesetPip(List<TradesetPip> ls){MsgResult rst;try{tableTradesetPip.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateTradeSimOrderQuene(List<TradeSimOrderQuene> ls){MsgResult rst;try{tableTradeSimOrderQuene.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateUserAccess(List<UserAccess> ls){MsgResult rst;try{tableUserAccess.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateUserAccount(List<UserAccount> ls){MsgResult rst;try{tableUserAccount.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateUserInfo(List<UserInfo> ls){MsgResult rst;try{tableUserInfo.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateUserPrivateInfo(List<UserPrivateInfo> ls){MsgResult rst;try{tableUserPrivateInfo.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateWorkFlowPath(List<WorkFlowPath> ls){MsgResult rst;try{tableWorkFlowPath.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateWorkGroup(List<WorkGroup> ls){MsgResult rst;try{tableWorkGroup.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateWorkGroupUser(List<WorkGroupUser> ls){MsgResult rst;try{tableWorkGroupUser.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}
public MsgResult updateWorkProductFlow(List<WorkProductFlow> ls){MsgResult rst;try{tableWorkProductFlow.update(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,203,ex.getLocalizedMessage());} return rst;}

public MsgResult deleteAccountBail(List<AccountBail> ls){MsgResult rst;try{tableAccountBail.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteAccountFare(List<AccountFare> ls){MsgResult rst;try{tableAccountFare.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteAccountIcode(List<AccountIcode> ls){MsgResult rst;try{tableAccountIcode.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteAccountInfo(List<AccountInfo> ls){MsgResult rst;try{tableAccountInfo.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteAccountInvest(List<AccountInvest> ls){MsgResult rst;try{tableAccountInvest.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteAlterOrder(List<AlterOrder> ls){MsgResult rst;try{tableAlterOrder.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteBailTemp(List<BailTemp> ls){MsgResult rst;try{tableBailTemp.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteBailTempObj(List<BailTempObj> ls){MsgResult rst;try{tableBailTempObj.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteCancelOrder(List<CancelOrder> ls){MsgResult rst;try{tableCancelOrder.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteClearingCurrencyRates(List<ClearingCurrencyRates> ls){MsgResult rst;try{tableClearingCurrencyRates.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteClearingDayBook(List<ClearingDayBook> ls){MsgResult rst;try{tableClearingDayBook.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteClearingExRight(List<ClearingExRight> ls){MsgResult rst;try{tableClearingExRight.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteClearingPrice(List<ClearingPrice> ls){MsgResult rst;try{tableClearingPrice.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteClearingProtfoliopos(List<ClearingProtfoliopos> ls){MsgResult rst;try{tableClearingProtfoliopos.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteFareTemp(List<FareTemp> ls){MsgResult rst;try{tableFareTemp.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteFareTempeObj(List<FareTempeObj> ls){MsgResult rst;try{tableFareTempeObj.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteFundState(List<FundState> ls){MsgResult rst;try{tableFundState.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteFundTransfer(List<FundTransfer> ls){MsgResult rst;try{tableFundTransfer.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteIOPVAccount(List<IOPVAccount> ls){MsgResult rst;try{tableIOPVAccount.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteIOPVProduct(List<IOPVProduct> ls){MsgResult rst;try{tableIOPVProduct.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteIOPVProductUnit(List<IOPVProductUnit> ls){MsgResult rst;try{tableIOPVProductUnit.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteLogLoginInfo(List<LogLoginInfo> ls){MsgResult rst;try{tableLogLoginInfo.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteNewOrder(List<NewOrder> ls){MsgResult rst;try{tableNewOrder.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteObjClassCode(List<ObjClassCode> ls){MsgResult rst;try{tableObjClassCode.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteObjClassCodeAtrr(List<ObjClassCodeAtrr> ls){MsgResult rst;try{tableObjClassCodeAtrr.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteObjETF(List<ObjETF> ls){MsgResult rst;try{tableObjETF.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteObjETFList(List<ObjETFList> ls){MsgResult rst;try{tableObjETFList.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteObjExRightInfo(List<ObjExRightInfo> ls){MsgResult rst;try{tableObjExRightInfo.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteObjIndex(List<ObjIndex> ls){MsgResult rst;try{tableObjIndex.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteObjIndexList(List<ObjIndexList> ls){MsgResult rst;try{tableObjIndexList.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteObjInfo(List<ObjInfo> ls){MsgResult rst;try{tableObjInfo.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteObjNewStock(List<ObjNewStock> ls){MsgResult rst;try{tableObjNewStock.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteObjPriceTick(List<ObjPriceTick> ls){MsgResult rst;try{tableObjPriceTick.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteObjProtfolio(List<ObjProtfolio> ls){MsgResult rst;try{tableObjProtfolio.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteObjProtfolioList(List<ObjProtfolioList> ls){MsgResult rst;try{tableObjProtfolioList.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteObjStructuredFundBase(List<ObjStructuredFundBase> ls){MsgResult rst;try{tableObjStructuredFundBase.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteObjStructuredFundPart(List<ObjStructuredFundPart> ls){MsgResult rst;try{tableObjStructuredFundPart.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteObjUnderlyingCode(List<ObjUnderlyingCode> ls){MsgResult rst;try{tableObjUnderlyingCode.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteOrderExecuteProgress(List<OrderExecuteProgress> ls){MsgResult rst;try{tableOrderExecuteProgress.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deletePosDetail(List<PosDetail> ls){MsgResult rst;try{tablePosDetail.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteProductAccount(List<ProductAccount> ls){MsgResult rst;try{tableProductAccount.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteProductInfo(List<ProductInfo> ls){MsgResult rst;try{tableProductInfo.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteProductUnit(List<ProductUnit> ls){MsgResult rst;try{tableProductUnit.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteRiskCtrlAssets(List<RiskCtrlAssets> ls){MsgResult rst;try{tableRiskCtrlAssets.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteRiskIndex(List<RiskIndex> ls){MsgResult rst;try{tableRiskIndex.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteRiskSets(List<RiskSets> ls){MsgResult rst;try{tableRiskSets.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteSafeForbidLogin(List<SafeForbidLogin> ls){MsgResult rst;try{tableSafeForbidLogin.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteSafeOtherAccess(List<SafeOtherAccess> ls){MsgResult rst;try{tableSafeOtherAccess.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteTradeReportDetail(List<TradeReportDetail> ls){MsgResult rst;try{tableTradeReportDetail.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteTradesetBroker(List<TradesetBroker> ls){MsgResult rst;try{tableTradesetBroker.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteTradesetCalendar(List<TradesetCalendar> ls){MsgResult rst;try{tableTradesetCalendar.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteTradesetExchangeLimit(List<TradesetExchangeLimit> ls){MsgResult rst;try{tableTradesetExchangeLimit.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteTradesetPip(List<TradesetPip> ls){MsgResult rst;try{tableTradesetPip.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteTradeSimOrderQuene(List<TradeSimOrderQuene> ls){MsgResult rst;try{tableTradeSimOrderQuene.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteUserAccess(List<UserAccess> ls){MsgResult rst;try{tableUserAccess.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteUserAccount(List<UserAccount> ls){MsgResult rst;try{tableUserAccount.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteUserInfo(List<UserInfo> ls){MsgResult rst;try{tableUserInfo.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteUserPrivateInfo(List<UserPrivateInfo> ls){MsgResult rst;try{tableUserPrivateInfo.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteWorkFlowPath(List<WorkFlowPath> ls){MsgResult rst;try{tableWorkFlowPath.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteWorkGroup(List<WorkGroup> ls){MsgResult rst;try{tableWorkGroup.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteWorkGroupUser(List<WorkGroupUser> ls){MsgResult rst;try{tableWorkGroupUser.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}
public MsgResult deleteWorkProductFlow(List<WorkProductFlow> ls){MsgResult rst;try{tableWorkProductFlow.delete(ls);rst = ReturnIDEntity.Success;}catch(Exception ex){rst = new MsgResult(false,204,ex.getLocalizedMessage());} return rst;}

public MsgResult operateAccountBail(int flag, AccountBail ls) {MsgResult rst;if (flag == 1) {try {tableAccountBail.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableAccountBail.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableAccountBail.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateAccountFare(int flag, AccountFare ls) {MsgResult rst;if (flag == 1) {try {tableAccountFare.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableAccountFare.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableAccountFare.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateAccountIcode(int flag, AccountIcode ls) {MsgResult rst;if (flag == 1) {try {tableAccountIcode.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableAccountIcode.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableAccountIcode.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateAccountInfo(int flag, AccountInfo ls) {MsgResult rst;if (flag == 1) {try {tableAccountInfo.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableAccountInfo.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableAccountInfo.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateAccountInvest(int flag, AccountInvest ls) {MsgResult rst;if (flag == 1) {try {tableAccountInvest.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableAccountInvest.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableAccountInvest.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateAlterOrder(int flag, AlterOrder ls) {MsgResult rst;if (flag == 1) {try {tableAlterOrder.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableAlterOrder.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableAlterOrder.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateBailTemp(int flag, BailTemp ls) {MsgResult rst;if (flag == 1) {try {tableBailTemp.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableBailTemp.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableBailTemp.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateBailTempObj(int flag, BailTempObj ls) {MsgResult rst;if (flag == 1) {try {tableBailTempObj.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableBailTempObj.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableBailTempObj.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateCancelOrder(int flag, CancelOrder ls) {MsgResult rst;if (flag == 1) {try {tableCancelOrder.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableCancelOrder.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableCancelOrder.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateClearingCurrencyRates(int flag, ClearingCurrencyRates ls) {MsgResult rst;if (flag == 1) {try {tableClearingCurrencyRates.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableClearingCurrencyRates.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableClearingCurrencyRates.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateClearingDayBook(int flag, ClearingDayBook ls) {MsgResult rst;if (flag == 1) {try {tableClearingDayBook.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableClearingDayBook.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableClearingDayBook.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateClearingExRight(int flag, ClearingExRight ls) {MsgResult rst;if (flag == 1) {try {tableClearingExRight.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableClearingExRight.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableClearingExRight.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateClearingPrice(int flag, ClearingPrice ls) {MsgResult rst;if (flag == 1) {try {tableClearingPrice.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableClearingPrice.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableClearingPrice.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateClearingProtfoliopos(int flag, ClearingProtfoliopos ls) {MsgResult rst;if (flag == 1) {try {tableClearingProtfoliopos.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableClearingProtfoliopos.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableClearingProtfoliopos.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateFareTemp(int flag, FareTemp ls) {MsgResult rst;if (flag == 1) {try {tableFareTemp.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableFareTemp.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableFareTemp.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateFareTempeObj(int flag, FareTempeObj ls) {MsgResult rst;if (flag == 1) {try {tableFareTempeObj.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableFareTempeObj.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableFareTempeObj.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateFundState(int flag, FundState ls) {MsgResult rst;if (flag == 1) {try {tableFundState.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableFundState.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableFundState.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateFundTransfer(int flag, FundTransfer ls) {MsgResult rst;if (flag == 1) {try {tableFundTransfer.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableFundTransfer.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableFundTransfer.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateIOPVAccount(int flag, IOPVAccount ls) {MsgResult rst;if (flag == 1) {try {tableIOPVAccount.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableIOPVAccount.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableIOPVAccount.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateIOPVProduct(int flag, IOPVProduct ls) {MsgResult rst;if (flag == 1) {try {tableIOPVProduct.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableIOPVProduct.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableIOPVProduct.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateIOPVProductUnit(int flag, IOPVProductUnit ls) {MsgResult rst;if (flag == 1) {try {tableIOPVProductUnit.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableIOPVProductUnit.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableIOPVProductUnit.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateLogLoginInfo(int flag, LogLoginInfo ls) {MsgResult rst;if (flag == 1) {try {tableLogLoginInfo.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableLogLoginInfo.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableLogLoginInfo.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateNewOrder(int flag, NewOrder ls) {MsgResult rst;if (flag == 1) {try {tableNewOrder.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableNewOrder.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableNewOrder.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateObjClassCode(int flag, ObjClassCode ls) {MsgResult rst;if (flag == 1) {try {tableObjClassCode.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableObjClassCode.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableObjClassCode.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateObjClassCodeAtrr(int flag, ObjClassCodeAtrr ls) {MsgResult rst;if (flag == 1) {try {tableObjClassCodeAtrr.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableObjClassCodeAtrr.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableObjClassCodeAtrr.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateObjETF(int flag, ObjETF ls) {MsgResult rst;if (flag == 1) {try {tableObjETF.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableObjETF.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableObjETF.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateObjETFList(int flag, ObjETFList ls) {MsgResult rst;if (flag == 1) {try {tableObjETFList.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableObjETFList.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableObjETFList.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateObjExRightInfo(int flag, ObjExRightInfo ls) {MsgResult rst;if (flag == 1) {try {tableObjExRightInfo.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableObjExRightInfo.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableObjExRightInfo.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateObjIndex(int flag, ObjIndex ls) {MsgResult rst;if (flag == 1) {try {tableObjIndex.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableObjIndex.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableObjIndex.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateObjIndexList(int flag, ObjIndexList ls) {MsgResult rst;if (flag == 1) {try {tableObjIndexList.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableObjIndexList.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableObjIndexList.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateObjInfo(int flag, ObjInfo ls) {MsgResult rst;if (flag == 1) {try {tableObjInfo.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableObjInfo.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableObjInfo.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateObjNewStock(int flag, ObjNewStock ls) {MsgResult rst;if (flag == 1) {try {tableObjNewStock.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableObjNewStock.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableObjNewStock.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateObjPriceTick(int flag, ObjPriceTick ls) {MsgResult rst;if (flag == 1) {try {tableObjPriceTick.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableObjPriceTick.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableObjPriceTick.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateObjProtfolio(int flag, ObjProtfolio ls) {MsgResult rst;if (flag == 1) {try {tableObjProtfolio.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableObjProtfolio.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableObjProtfolio.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateObjProtfolioList(int flag, ObjProtfolioList ls) {MsgResult rst;if (flag == 1) {try {tableObjProtfolioList.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableObjProtfolioList.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableObjProtfolioList.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateObjStructuredFundBase(int flag, ObjStructuredFundBase ls) {MsgResult rst;if (flag == 1) {try {tableObjStructuredFundBase.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableObjStructuredFundBase.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableObjStructuredFundBase.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateObjStructuredFundPart(int flag, ObjStructuredFundPart ls) {MsgResult rst;if (flag == 1) {try {tableObjStructuredFundPart.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableObjStructuredFundPart.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableObjStructuredFundPart.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateObjUnderlyingCode(int flag, ObjUnderlyingCode ls) {MsgResult rst;if (flag == 1) {try {tableObjUnderlyingCode.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableObjUnderlyingCode.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableObjUnderlyingCode.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateOrderExecuteProgress(int flag, OrderExecuteProgress ls) {MsgResult rst;if (flag == 1) {try {tableOrderExecuteProgress.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableOrderExecuteProgress.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableOrderExecuteProgress.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operatePosDetail(int flag, PosDetail ls) {MsgResult rst;if (flag == 1) {try {tablePosDetail.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tablePosDetail.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tablePosDetail.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateProductAccount(int flag, ProductAccount ls) {MsgResult rst;if (flag == 1) {try {tableProductAccount.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableProductAccount.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableProductAccount.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateProductInfo(int flag, ProductInfo ls) {MsgResult rst;if (flag == 1) {try {tableProductInfo.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableProductInfo.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableProductInfo.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateProductUnit(int flag, ProductUnit ls) {MsgResult rst;if (flag == 1) {try {tableProductUnit.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableProductUnit.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableProductUnit.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateRiskCtrlAssets(int flag, RiskCtrlAssets ls) {MsgResult rst;if (flag == 1) {try {tableRiskCtrlAssets.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableRiskCtrlAssets.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableRiskCtrlAssets.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateRiskIndex(int flag, RiskIndex ls) {MsgResult rst;if (flag == 1) {try {tableRiskIndex.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableRiskIndex.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableRiskIndex.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateRiskSets(int flag, RiskSets ls) {MsgResult rst;if (flag == 1) {try {tableRiskSets.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableRiskSets.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableRiskSets.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateSafeForbidLogin(int flag, SafeForbidLogin ls) {MsgResult rst;if (flag == 1) {try {tableSafeForbidLogin.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableSafeForbidLogin.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableSafeForbidLogin.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateSafeOtherAccess(int flag, SafeOtherAccess ls) {MsgResult rst;if (flag == 1) {try {tableSafeOtherAccess.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableSafeOtherAccess.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableSafeOtherAccess.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateTradeReportDetail(int flag, TradeReportDetail ls) {MsgResult rst;if (flag == 1) {try {tableTradeReportDetail.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableTradeReportDetail.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableTradeReportDetail.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateTradesetBroker(int flag, TradesetBroker ls) {MsgResult rst;if (flag == 1) {try {tableTradesetBroker.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableTradesetBroker.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableTradesetBroker.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateTradesetCalendar(int flag, TradesetCalendar ls) {MsgResult rst;if (flag == 1) {try {tableTradesetCalendar.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableTradesetCalendar.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableTradesetCalendar.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateTradesetExchangeLimit(int flag, TradesetExchangeLimit ls) {MsgResult rst;if (flag == 1) {try {tableTradesetExchangeLimit.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableTradesetExchangeLimit.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableTradesetExchangeLimit.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateTradesetPip(int flag, TradesetPip ls) {MsgResult rst;if (flag == 1) {try {tableTradesetPip.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableTradesetPip.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableTradesetPip.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateTradeSimOrderQuene(int flag, TradeSimOrderQuene ls) {MsgResult rst;if (flag == 1) {try {tableTradeSimOrderQuene.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableTradeSimOrderQuene.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableTradeSimOrderQuene.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateUserAccess(int flag, UserAccess ls) {MsgResult rst;if (flag == 1) {try {tableUserAccess.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableUserAccess.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableUserAccess.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateUserAccount(int flag, UserAccount ls) {MsgResult rst;if (flag == 1) {try {tableUserAccount.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableUserAccount.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableUserAccount.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateUserInfo(int flag, UserInfo ls) {MsgResult rst;if (flag == 1) {try {tableUserInfo.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableUserInfo.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableUserInfo.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateUserPrivateInfo(int flag, UserPrivateInfo ls) {MsgResult rst;if (flag == 1) {try {tableUserPrivateInfo.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableUserPrivateInfo.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableUserPrivateInfo.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateWorkFlowPath(int flag, WorkFlowPath ls) {MsgResult rst;if (flag == 1) {try {tableWorkFlowPath.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableWorkFlowPath.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableWorkFlowPath.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateWorkGroup(int flag, WorkGroup ls) {MsgResult rst;if (flag == 1) {try {tableWorkGroup.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableWorkGroup.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableWorkGroup.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateWorkGroupUser(int flag, WorkGroupUser ls) {MsgResult rst;if (flag == 1) {try {tableWorkGroupUser.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableWorkGroupUser.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableWorkGroupUser.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}
public MsgResult operateWorkProductFlow(int flag, WorkProductFlow ls) {MsgResult rst;if (flag == 1) {try {tableWorkProductFlow.insert(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else if (flag == 2) {try {tableWorkProductFlow.delete(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}} else {try {tableWorkProductFlow.update(ls);rst = ReturnIDEntity.Success;} catch (Exception ex) {rst = new MsgResult(false, 202, ex.getLocalizedMessage());}}return rst;}

}
