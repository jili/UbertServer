/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.netty.priceserver;

import com.jili.ubert.netty.MsgDecoder;
import com.jili.ubert.netty.MsgEncoder;
import com.jili.ubert.server.datapool.DataPoolPrice;
import com.jili.ubert.server.priceserver.PriceServer;
import com.jili.ubert.server.publicdata.PublicData;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;

/**
 *
 * @author ChengJiLi
 */
public class NettyServer2PriceInitializer  extends ChannelInitializer<SocketChannel> {
    private DataPoolPrice datapool;
    private PublicData publicData;
    private PriceServer priceServer;
    NettyServer2PriceInitializer(DataPoolPrice datapool, PublicData publicData,PriceServer priceServer) {
        this.publicData = publicData;
        this.datapool = datapool;
        this.priceServer = priceServer;
    }

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast(new MsgDecoder());
        pipeline.addLast(new MsgEncoder());

        // and then business logic.
        // Please note we create a handler for every new channel
        // because it has stateful properties.
        pipeline.addLast(new PriceHanlder(datapool,publicData,priceServer));
    }
    
}
