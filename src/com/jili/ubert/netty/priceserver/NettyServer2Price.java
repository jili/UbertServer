/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.netty.priceserver;

import com.jili.ubert.netty.traderserver.NettyServer2Trade;
import com.jili.ubert.server.datapool.DataPoolPrice;
import com.jili.ubert.server.priceserver.PriceServer;
import com.jili.ubert.server.publicdata.PublicData;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ChengJiLi
 */
public class NettyServer2Price implements Runnable {

    private int port;
    private DataPoolPrice datapool;
    private PublicData publicData;
    private PriceServer priceServer;

    public NettyServer2Price(int port) {
        this.port = port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setDatapool(DataPoolPrice datapool) {
        this.datapool = datapool;
    }

    public void setPublicData(PublicData publicData) {
        this.publicData = publicData;
    }

    @Override
    public void run() {
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(new NettyServer2PriceInitializer(datapool, publicData,priceServer));

            b.bind(port).sync().channel().closeFuture().sync();
            Logger.getLogger(NettyServer2Trade.class.getName()).log(Level.INFO, null, "测试版本-SecureChatServerInitializer1");
        } catch (InterruptedException ex) {
            Logger.getLogger(NettyServer2Trade.class.getName()).log(Level.INFO, null, ex);
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    public void setPriceServer(PriceServer priceServer) {
        this.priceServer = priceServer;
    }
}
