/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.netty;

import com.baidu.bjf.remoting.protobuf.Codec;
import com.baidu.bjf.remoting.protobuf.ProtobufProxy;
import com.jili.ubert.dao.DBOperation;
import com.jili.ubert.dao.DBTableName;
import java.io.IOException;

/**
 *
 * @author ChengJili
 */
public class DAOEntity {

    private DBTableName tableName;
    private DBOperation operation;
    private byte[] data;

    /**
     * @return the tableName
     */
    public DBTableName getTableName() {
        return tableName;
    }

    /**
     * @param tableName the tableName to set
     */
    public void setTableName(DBTableName tableName) {
        this.tableName = tableName;
    }

    /**
     * @return the operation
     */
    public DBOperation getOperation() {
        return operation;
    }

    /**
     * @param operation the operation to set
     */
    public void setOperation(DBOperation operation) {
        this.operation = operation;
    }

    /**
     * @return the data
     */
    public byte[] getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(byte[] data) {
        this.data = data;
    }

    public byte[] EnCode(DAOEntity msg) throws IOException {
        Codec<DAOEntity> Lcode = ProtobufProxy.create(DAOEntity.class);
        return Lcode.encode(msg);
    }

    public DAOEntity DeCode(byte[] data) throws IOException {
        Codec<DAOEntity> Lcode = ProtobufProxy.create(DAOEntity.class);
        return Lcode.decode(data);
    }
}
