/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.netty.traderserver;

import com.jili.ubert.netty.MsgDecoder;
import com.jili.ubert.netty.MsgEncoder;
import com.jili.ubert.server.datapool.DataPoolTrade;
import com.jili.ubert.server.manager.ManagerServer;
import com.jili.ubert.server.ordermanager.OrderManager;
import com.jili.ubert.server.publicdata.PublicData;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.compression.ZlibCodecFactory;
import io.netty.handler.codec.compression.ZlibWrapper;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.util.SelfSignedCertificate;

/**
 *
 * @author ChengJiLi
 */
class NettyServer2TradeInitializer extends ChannelInitializer<SocketChannel> {

    private final OrderManager orderManager;
    private DataPoolTrade datapool;
    private PublicData publicData;
    private ManagerServer managerServer;
    private boolean isSSL = false;
    private boolean isZlib = false;
    private SslContext sslCtx;

    public NettyServer2TradeInitializer(OrderManager orderManager,DataPoolTrade datapool,PublicData publicData,ManagerServer managerServer) {
        this.orderManager = orderManager;
        this.datapool = datapool;
        this.managerServer = managerServer;
        this.publicData=publicData;
    }

    NettyServer2TradeInitializer(OrderManager orderManager, DataPoolTrade datapool, PublicData publicData, ManagerServer managerServer, boolean ssl, boolean zlib, SslContext sslCtx) {
        this.orderManager = orderManager;
        this.datapool = datapool;
        this.managerServer = managerServer;
        this.publicData=publicData;
        this.isSSL = ssl;
        this.isZlib = zlib;
        this.sslCtx = sslCtx;
    }

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();

        if (isSSL) {
            SelfSignedCertificate ssc = new SelfSignedCertificate();
            sslCtx = SslContext.newServerContext(ssc.certificate(), ssc.privateKey());
            pipeline.addLast(sslCtx.newHandler(ch.alloc()));
        }

        // Enable stream compression (you can remove these two if unnecessary)
        if (isZlib) {
            pipeline.addLast(ZlibCodecFactory.newZlibEncoder(ZlibWrapper.GZIP));
            pipeline.addLast(ZlibCodecFactory.newZlibDecoder(ZlibWrapper.GZIP));
        }

    //    pipeline.addLast("idleStateHandler", new IdleStateHandler(10, 5, 0));
        // Add the number codec first,
        pipeline.addLast(new MsgDecoder());
        pipeline.addLast(new MsgEncoder());

        // and then business logic.
        // Please note we create a handler for every new channel
        // because it has stateful properties.
        pipeline.addLast(new TradeHanlder(orderManager,datapool,publicData,managerServer));

    }

}
