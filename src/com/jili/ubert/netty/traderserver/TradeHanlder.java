/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.netty.traderserver;

import com.jili.ubert.netty.VerifyCodeGenerator;
import com.jili.ubert.dao.db.AlterOrder;
import com.jili.ubert.code.client2server.BasketCancelOrders;
import com.jili.ubert.code.client2server.BasketOrders;
import com.jili.ubert.dao.db.CancelOrder;
import com.jili.ubert.code.client2server.Logout;
import com.jili.ubert.code.client2server.Login;
import com.jili.ubert.dao.db.NewOrder;
import com.jili.ubert.code.server2client.MsgResult;
import com.jili.ubert.code.server2client.MsgResultList;
import com.jili.ubert.netty.MsgEntity;
import com.jili.ubert.tool.IPv4Util;
import com.jili.ubert.code.ReturnIDEntity;
import com.jili.ubert.code.client2server.RequestBatchOperateDB;
import com.jili.ubert.code.client2server.RequestPageData;
import com.jili.ubert.code.server2client.PageDataProto;
import com.jili.ubert.server.datapool.DataPoolTrade;
import com.jili.ubert.server.manager.ManagerServer;
import com.jili.ubert.server.ordermanager.OrderManager;
import com.jili.ubert.server.publicdata.PublicData;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author ChengJiLi
 */
class TradeHanlder extends SimpleChannelInboundHandler<MsgEntity> {

    private static final Log log = LogFactory.getLog(TradeHanlder.class);
    private static final HashMap<Integer, String> vCode = new HashMap<>();
    private OrderManager orderManager;
    private DataPoolTrade datapool;
    private PublicData publicData;
    private ManagerServer managerServer;

    public TradeHanlder(OrderManager orderManager,DataPoolTrade datapool,PublicData publicData,ManagerServer managerServer) {
        this.orderManager = orderManager;
        this.datapool = datapool;
        this.managerServer = managerServer;
        this.publicData=publicData;
    }

    private boolean isBlackList(int ip, String mac) {
        if (this.publicData.getForbidUser().containsKey(String.valueOf(ip))) {
            return true;
        }
        if (this.publicData.getForbidUser().containsKey(mac)) {
            return true;
        }
        return this.publicData.getForbidUser().containsKey(ip + mac);
    }

    private boolean isBlackList(String user) {
        return this.publicData.getForbidUser().containsKey(user);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        if (ctx.channel().isActive()) {
            ctx.channel().writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
        } else {
            ctx.close();
        }
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt)
            throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent e = (IdleStateEvent) evt;
            if (e.state() == IdleState.READER_IDLE) {
                ctx.close();
                log.info("READER_IDLE 读超时");
            } else if (e.state() == IdleState.WRITER_IDLE) {
                log.info("WRITER_IDLE 写超时");
            }
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }
    @Override
    public void channelActive(final ChannelHandlerContext ctx) {
        log.info("新增登陆连接："+ctx.channel());
    }

    @Override
    protected void messageReceived(ChannelHandlerContext ctx, MsgEntity msg) throws Exception {
        if (msg == null) {
            return;
        }
        log.debug("messageReceived" + ctx);
        Channel channel = ctx.channel();
        InetSocketAddress insocket = (InetSocketAddress) ctx.channel().remoteAddress();
        int ip = IPv4Util.bytesToInt(insocket.getAddress().getAddress());
        log.debug("客户IP：" + insocket.getAddress().getHostAddress() + "-" + ip);
        //    String mac = IPv4Util.getMACAddress(insocket.getAddress());
        String mac = "WE-WE-WE-WE-WE";
        //防止攻击，过滤类（现在一个算法）
        if (this.isBlackList(ip, mac)) {
            MsgResult rst = ReturnIDEntity.forbidInblacklist;
            msg.Reply( 250, rst.EnCode(rst));
            channel.close();
            return;
        }
        int csCommondCode = msg.getCmdCode();
        byte[] data = msg.getData();
        switch (csCommondCode) {
            case 0: {//启动问候，请求验证码，回复验证码
                String str = VerifyCodeGenerator.GenerateVerifyCode(4);
                log.debug("0-回应" + ip + " 验证码：" + str);
                msg.Reply( 0, str.getBytes());
                log.debug("1-回应" + ip + " 验证码：" + str);
                vCode.put(ip, str);
                break;
            }
            case 1: {//登录  Login
                /*
                 1.验证密码，验证码；回复消息
                 2.维护用户登录列表
                
                 备注现在为了提升用户体验，用户在输入的时候验证有没有，验证码也在输入的时候验证正确不正确。
                 */
                //解码消息
                Login login = new Login();
                login = login.DeCode(data);
                MsgResult rst;
                if (vCode.containsKey(ip)) {
                    if (vCode.get(ip).equals(login.getCode())) {
                        rst = publicData.LoginCheck(login, ip, channel);
                    } else {
                        rst = ReturnIDEntity.incorrectVCode;
                    }
                } else {
                    rst = ReturnIDEntity.incorrectVCode;
                }

                msg.Reply( 1, rst.EnCode(rst));

                break;
            }
            case 2: {//登出 Logout
                /*
                 1.从用户登录列表删除
                 2.释放其他资源
                 */
                Logout lonout = new Logout();
                lonout = lonout.DeCode(data);
                MsgResult rst = publicData.Logout(lonout.getUserID(), ip);
                msg.Reply( 2, rst.EnCode(rst));
                break;
            }
            case 3: {//单笔委托 NewOrder
                /*
                 1.验资验券
                 2.是否过风控，否，直接下单
                 3.是，调用风控过程
                 备注：1.下单接口静态实例工厂模式；2.风控静态实例工厂模式
                 */
                NewOrder order = new NewOrder();
                order = order.DeCode(data);
                MsgResult rst = orderManager.Send(order);
                msg.Reply( 3, rst.EnCode(rst));
                break;
            }
            case 4: {//单笔撤单 CancelOrder
                /*
                 1.回复报单
                 */
                CancelOrder order = new CancelOrder();
                order = order.DeCode(data);
                MsgResult rst = orderManager.Send(order);
                break;
            }
            case 5: {//篮子委托 BasketOrder
                BasketOrders order = new BasketOrders();
                order = order.DeCode(data);
                MsgResultList rst = orderManager.Send(order);
                break;
            }
            case 6: {//篮子撤单 BasketCancelOrder
                BasketCancelOrders order = new BasketCancelOrders();
                order = order.DeCode(data);
                MsgResultList rst = orderManager.Send(order);
                break;
            }
            case 7: {//改价 AlterOrder
                /*
                 监听者模式
                 */
                AlterOrder order = new AlterOrder();
                order = order.DeCode(data);
                MsgResult rst = orderManager.Send(order);
                break;
            }
            case 8: {//指令委托 InstructionOrder
                //    DealInstructionOrder.deal(ctx, msg);
                break;
            }
            case 9: {//取消指令 CancelInstructionOrder
                //    DealCancelInstructionOrder.deal(ctx, msg);
                break;
            }
            case 10: {//自定义交易指令 CustomTradeOrder
                break;
            }
            case 11: {//验证验证码
                Login login = new Login();
                login = login.DeCode(data);
                MsgResult rst;
                if (vCode.containsKey(ip)) {
                    if (login.getCode().equals(vCode.get(ip))) {
                        rst = ReturnIDEntity.Success;
                    } else {
                        rst = ReturnIDEntity.incorrectVCode;
                    }
                } else {
                    rst = ReturnIDEntity.incorrectVCode;

                }
                msg.Reply( 11, rst.EnCode(rst));
                break;
            }
            case 12: {//验证是否有此用户
                Login login = new Login();
                login = login.DeCode(data);
                MsgResult rst;
                if (this.publicData.getUserInfo(login.getUserID())!=null) {
                    rst = ReturnIDEntity.Success;
                } else {
                    rst = ReturnIDEntity.noExistUser;
                }
                break;
            }
            case 13: {

                break;
            }
            case 14: {

                break;
            }
            case 15: {

                break;
            }
            case 16: {

                break;
            }
            case 17: {

                break;
            }
            case 18: {

                break;
            }
            case 19: {

                break;
            }
            case 20: {

                break;
            }
            case 21: {

                break;
            }
            case 22: {

                break;
            }
            case 23: {

                break;
            }
            case 24: {

                break;
            }
            case 25: {

                break;
            }
            case 26: {

                break;
            }
            case 27: {

                break;
            }
            case 28: {

                break;
            }
            case 29: {

                break;
            }
            case 30: {

                break;
            }
case 500: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryAccountBail(rpd);msg.Reply(500, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 500 IO decode encode fail");}}});break;}
case 501: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryAccountFare(rpd);msg.Reply(501, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 501 IO decode encode fail");}}});break;}
case 502: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryAccountIcode(rpd);msg.Reply(502, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 502 IO decode encode fail");}}});break;}
case 503: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryAccountInfo(rpd);msg.Reply(503, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 503 IO decode encode fail");}}});break;}
case 504: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryAccountInvest(rpd);msg.Reply(504, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 504 IO decode encode fail");}}});break;}
case 505: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryAlterOrder(rpd);msg.Reply(505, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 505 IO decode encode fail");}}});break;}
case 506: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryBailTemp(rpd);msg.Reply(506, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 506 IO decode encode fail");}}});break;}
case 507: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryBailTempObj(rpd);msg.Reply(507, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 507 IO decode encode fail");}}});break;}
case 508: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryCancelOrder(rpd);msg.Reply(508, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 508 IO decode encode fail");}}});break;}
case 509: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryClearingCurrencyRates(rpd);msg.Reply(509, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 509 IO decode encode fail");}}});break;}
case 510: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryClearingDayBook(rpd);msg.Reply(510, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 510 IO decode encode fail");}}});break;}
case 511: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryClearingExRight(rpd);msg.Reply(511, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 511 IO decode encode fail");}}});break;}
case 512: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryClearingPrice(rpd);msg.Reply(512, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 512 IO decode encode fail");}}});break;}
case 513: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryClearingProtfoliopos(rpd);msg.Reply(513, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 513 IO decode encode fail");}}});break;}
case 514: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryFareTemp(rpd);msg.Reply(514, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 514 IO decode encode fail");}}});break;}
case 515: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryFareTempeObj(rpd);msg.Reply(515, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 515 IO decode encode fail");}}});break;}
case 516: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryFundState(rpd);msg.Reply(516, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 516 IO decode encode fail");}}});break;}
case 517: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryFundTransfer(rpd);msg.Reply(517, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 517 IO decode encode fail");}}});break;}
case 518: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryIOPVAccount(rpd);msg.Reply(518, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 518 IO decode encode fail");}}});break;}
case 519: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryIOPVProduct(rpd);msg.Reply(519, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 519 IO decode encode fail");}}});break;}
case 520: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryIOPVProductUnit(rpd);msg.Reply(520, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 520 IO decode encode fail");}}});break;}
case 521: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryLogLoginInfo(rpd);msg.Reply(521, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 521 IO decode encode fail");}}});break;}
case 522: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryNewOrder(rpd);msg.Reply(522, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 522 IO decode encode fail");}}});break;}
case 523: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryObjClassCode(rpd);msg.Reply(523, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 523 IO decode encode fail");}}});break;}
case 524: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryObjClassCodeAtrr(rpd);msg.Reply(524, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 524 IO decode encode fail");}}});break;}
case 525: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryObjETF(rpd);msg.Reply(525, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 525 IO decode encode fail");}}});break;}
case 526: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryObjETFList(rpd);msg.Reply(526, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 526 IO decode encode fail");}}});break;}
case 527: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryObjExRightInfo(rpd);msg.Reply(527, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 527 IO decode encode fail");}}});break;}
case 528: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryObjIndex(rpd);msg.Reply(528, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 528 IO decode encode fail");}}});break;}
case 529: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryObjIndexList(rpd);msg.Reply(529, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 529 IO decode encode fail");}}});break;}
case 530: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryObjInfo(rpd);msg.Reply(530, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 530 IO decode encode fail");}}});break;}
case 531: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryObjNewStock(rpd);msg.Reply(531, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 531 IO decode encode fail");}}});break;}
case 532: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryObjPriceTick(rpd);msg.Reply(532, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 532 IO decode encode fail");}}});break;}
case 533: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryObjProtfolio(rpd);msg.Reply(533, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 533 IO decode encode fail");}}});break;}
case 534: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryObjProtfolioList(rpd);msg.Reply(534, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 534 IO decode encode fail");}}});break;}
case 535: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryObjStructuredFundBase(rpd);msg.Reply(535, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 535 IO decode encode fail");}}});break;}
case 536: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryObjStructuredFundPart(rpd);msg.Reply(536, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 536 IO decode encode fail");}}});break;}
case 537: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryObjUnderlyingCode(rpd);msg.Reply(537, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 537 IO decode encode fail");}}});break;}
case 538: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryOrderExecuteProgress(rpd);msg.Reply(538, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 538 IO decode encode fail");}}});break;}
case 539: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryPosDetail(rpd);msg.Reply(539, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 539 IO decode encode fail");}}});break;}
case 540: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryProductAccount(rpd);msg.Reply(540, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 540 IO decode encode fail");}}});break;}
case 541: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryProductInfo(rpd);msg.Reply(541, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 541 IO decode encode fail");}}});break;}
case 542: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryProductUnit(rpd);msg.Reply(542, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 542 IO decode encode fail");}}});break;}
case 543: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryRiskCtrlAssets(rpd);msg.Reply(543, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 543 IO decode encode fail");}}});break;}
case 544: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryRiskIndex(rpd);msg.Reply(544, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 544 IO decode encode fail");}}});break;}
case 545: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryRiskSets(rpd);msg.Reply(545, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 545 IO decode encode fail");}}});break;}
case 546: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.querySafeForbidLogin(rpd);msg.Reply(546, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 546 IO decode encode fail");}}});break;}
case 547: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.querySafeOtherAccess(rpd);msg.Reply(547, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 547 IO decode encode fail");}}});break;}
case 548: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTradeReportDetail(rpd);msg.Reply(548, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 548 IO decode encode fail");}}});break;}
case 549: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTradesetBroker(rpd);msg.Reply(549, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 549 IO decode encode fail");}}});break;}
case 550: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTradesetCalendar(rpd);msg.Reply(550, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 550 IO decode encode fail");}}});break;}
case 551: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTradesetExchangeLimit(rpd);msg.Reply(551, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 551 IO decode encode fail");}}});break;}
case 552: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTradesetPip(rpd);msg.Reply(552, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 552 IO decode encode fail");}}});break;}
case 553: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTradeSimOrderQuene(rpd);msg.Reply(553, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 553 IO decode encode fail");}}});break;}
case 554: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryUserAccess(rpd);msg.Reply(554, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 554 IO decode encode fail");}}});break;}
case 555: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryUserAccount(rpd);msg.Reply(555, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 555 IO decode encode fail");}}});break;}
case 556: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryUserInfo(rpd);msg.Reply(556, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 556 IO decode encode fail");}}});break;}
case 557: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryUserPrivateInfo(rpd);msg.Reply(557, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 557 IO decode encode fail");}}});break;}
case 558: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryWorkFlowPath(rpd);msg.Reply(558, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 558 IO decode encode fail");}}});break;}
case 559: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryWorkGroup(rpd);msg.Reply(559, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 559 IO decode encode fail");}}});break;}
case 560: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryWorkGroupUser(rpd);msg.Reply(560, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 560 IO decode encode fail");}}});break;}
case 561: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryWorkProductFlow(rpd);msg.Reply(561, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 561 IO decode encode fail");}}});break;}

//case 562: {publicData.WorkRun(new Runnable(){@Override public void run() {try {SelectPageData rpd = SelectPageData.DeCode(data);PageDataProto pdp = datapool.queryString(rpd);msg.Reply(562, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 562 IO decode encode fail");}}});break;}
case 563: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateAccountBail(rpd);msg.Reply(563, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 563 IO decode encode fail");}}});break;}
case 564: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateAccountFare(rpd);msg.Reply(564, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 564 IO decode encode fail");}}});break;}
case 565: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateAccountIcode(rpd);msg.Reply(565, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 565 IO decode encode fail");}}});break;}
case 566: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateAccountInfo(rpd);msg.Reply(566, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 566 IO decode encode fail");}}});break;}
case 567: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateAccountInvest(rpd);msg.Reply(567, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 567 IO decode encode fail");}}});break;}
case 568: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateAlterOrder(rpd);msg.Reply(568, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 568 IO decode encode fail");}}});break;}
case 569: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateBailTemp(rpd);msg.Reply(569, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 569 IO decode encode fail");}}});break;}
case 570: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateBailTempObj(rpd);msg.Reply(570, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 570 IO decode encode fail");}}});break;}
case 571: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateCancelOrder(rpd);msg.Reply(571, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 571 IO decode encode fail");}}});break;}
case 572: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateClearingCurrencyRates(rpd);msg.Reply(572, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 572 IO decode encode fail");}}});break;}
case 573: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateClearingDayBook(rpd);msg.Reply(573, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 573 IO decode encode fail");}}});break;}
case 574: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateClearingExRight(rpd);msg.Reply(574, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 574 IO decode encode fail");}}});break;}
case 575: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateClearingPrice(rpd);msg.Reply(575, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 575 IO decode encode fail");}}});break;}
case 576: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateClearingProtfoliopos(rpd);msg.Reply(576, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 576 IO decode encode fail");}}});break;}
case 577: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateFareTemp(rpd);msg.Reply(577, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 577 IO decode encode fail");}}});break;}
case 578: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateFareTempeObj(rpd);msg.Reply(578, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 578 IO decode encode fail");}}});break;}
case 579: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateFundState(rpd);msg.Reply(579, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 579 IO decode encode fail");}}});break;}
case 580: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateFundTransfer(rpd);msg.Reply(580, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 580 IO decode encode fail");}}});break;}
case 581: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateIOPVAccount(rpd);msg.Reply(581, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 581 IO decode encode fail");}}});break;}
case 582: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateIOPVProduct(rpd);msg.Reply(582, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 582 IO decode encode fail");}}});break;}
case 583: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateIOPVProductUnit(rpd);msg.Reply(583, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 583 IO decode encode fail");}}});break;}
case 584: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateLogLoginInfo(rpd);msg.Reply(584, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 584 IO decode encode fail");}}});break;}
case 585: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateNewOrder(rpd);msg.Reply(585, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 585 IO decode encode fail");}}});break;}
case 586: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateObjClassCode(rpd);msg.Reply(586, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 586 IO decode encode fail");}}});break;}
case 587: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateObjClassCodeAtrr(rpd);msg.Reply(587, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 587 IO decode encode fail");}}});break;}
case 588: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateObjETF(rpd);msg.Reply(588, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 588 IO decode encode fail");}}});break;}
case 589: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateObjETFList(rpd);msg.Reply(589, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 589 IO decode encode fail");}}});break;}
case 590: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateObjExRightInfo(rpd);msg.Reply(590, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 590 IO decode encode fail");}}});break;}
case 591: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateObjIndex(rpd);msg.Reply(591, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 591 IO decode encode fail");}}});break;}
case 592: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateObjIndexList(rpd);msg.Reply(592, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 592 IO decode encode fail");}}});break;}
case 593: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateObjInfo(rpd);msg.Reply(593, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 593 IO decode encode fail");}}});break;}
case 594: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateObjNewStock(rpd);msg.Reply(594, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 594 IO decode encode fail");}}});break;}
case 595: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateObjPriceTick(rpd);msg.Reply(595, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 595 IO decode encode fail");}}});break;}
case 596: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateObjProtfolio(rpd);msg.Reply(596, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 596 IO decode encode fail");}}});break;}
case 597: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateObjProtfolioList(rpd);msg.Reply(597, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 597 IO decode encode fail");}}});break;}
case 598: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateObjStructuredFundBase(rpd);msg.Reply(598, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 598 IO decode encode fail");}}});break;}
case 599: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateObjStructuredFundPart(rpd);msg.Reply(599, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 599 IO decode encode fail");}}});break;}
case 600: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateObjUnderlyingCode(rpd);msg.Reply(600, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 600 IO decode encode fail");}}});break;}
case 601: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateOrderExecuteProgress(rpd);msg.Reply(601, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 601 IO decode encode fail");}}});break;}
case 602: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operatePosDetail(rpd);msg.Reply(602, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 602 IO decode encode fail");}}});break;}
case 603: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateProductAccount(rpd);msg.Reply(603, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 603 IO decode encode fail");}}});break;}
case 604: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateProductInfo(rpd);msg.Reply(604, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 604 IO decode encode fail");}}});break;}
case 605: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateProductUnit(rpd);msg.Reply(605, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 605 IO decode encode fail");}}});break;}
case 606: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateRiskCtrlAssets(rpd);msg.Reply(606, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 606 IO decode encode fail");}}});break;}
case 607: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateRiskIndex(rpd);msg.Reply(607, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 607 IO decode encode fail");}}});break;}
case 608: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateRiskSets(rpd);msg.Reply(608, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 608 IO decode encode fail");}}});break;}
case 609: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateSafeForbidLogin(rpd);msg.Reply(609, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 609 IO decode encode fail");}}});break;}
case 610: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateSafeOtherAccess(rpd);msg.Reply(610, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 610 IO decode encode fail");}}});break;}
case 611: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateTradeReportDetail(rpd);msg.Reply(611, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 611 IO decode encode fail");}}});break;}
case 612: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateTradesetBroker(rpd);msg.Reply(612, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 612 IO decode encode fail");}}});break;}
case 613: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateTradesetCalendar(rpd);msg.Reply(613, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 613 IO decode encode fail");}}});break;}
case 614: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateTradesetExchangeLimit(rpd);msg.Reply(614, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 614 IO decode encode fail");}}});break;}
case 615: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateTradesetPip(rpd);msg.Reply(615, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 615 IO decode encode fail");}}});break;}
case 616: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateTradeSimOrderQuene(rpd);msg.Reply(616, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 616 IO decode encode fail");}}});break;}
case 617: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateUserAccess(rpd);msg.Reply(617, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 617 IO decode encode fail");}}});break;}
case 618: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateUserAccount(rpd);msg.Reply(618, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 618 IO decode encode fail");}}});break;}
case 619: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateUserInfo(rpd);msg.Reply(619, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 619 IO decode encode fail");}}});break;}
case 620: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateUserPrivateInfo(rpd);msg.Reply(620, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 620 IO decode encode fail");}}});break;}
case 621: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateWorkFlowPath(rpd);msg.Reply(621, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 621 IO decode encode fail");}}});break;}
case 622: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateWorkGroup(rpd);msg.Reply(622, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 622 IO decode encode fail");}}});break;}
case 623: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateWorkGroupUser(rpd);msg.Reply(623, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 623 IO decode encode fail");}}});break;}
case 624: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestBatchOperateDB rpd = RequestBatchOperateDB.DeCode(data);MsgResult pdp = datapool.operateWorkProductFlow(rpd);msg.Reply(624, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 624 IO decode encode fail");}}});break;}
//case 625: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestExecutSQL rpd = RequestExecutSQL.DeCode(data);MsgResult pdp = datapool.operateString(rpd);msg.Reply(625, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 625 IO decode encode fail");}}});break;}//case 625: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestExecutSQL rpd = RequestExecutSQL.DeCode(data);MsgResult pdp = datapool.operateString(rpd);msg.Reply(625, MsgResult.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 625 IO decode encode fail");}}});break;}

case 1000: {publicData.WorkRun(new Runnable() {
            @Override public void run() {

            }
        });
        break;
    }
            default: {//未知消息，错误回复
                MsgResult rst = ReturnIDEntity.noExistMSG;
                log.debug("非法消息");
                msg.Reply( 250, "非法消息".getBytes());
                break;
            }
        }
    }

    public void setOrderManager(OrderManager orderManager) {
        this.orderManager = orderManager;
    }

    public void setDatapool(DataPoolTrade datapool) {
        this.datapool = datapool;
    }

    public void setPublicData(PublicData publicData) {
        this.publicData = publicData;
    }

    public void setManagerServer(ManagerServer managerServer) {
        this.managerServer = managerServer;
    }
    
}
