/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.netty.traderserver;

import com.jili.ubert.server.datapool.DataPoolTrade;
import com.jili.ubert.server.manager.ManagerServer;
import com.jili.ubert.server.ordermanager.OrderManager;
import com.jili.ubert.server.publicdata.PublicData;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.util.SelfSignedCertificate;
import java.security.cert.CertificateException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLException;

/**
 *
 * @author ChengJiLi
 */
public class NettyServer2Trade implements Runnable {
//public class NettyServer2Trade {

    private int port;
    private OrderManager orderManager;
    private DataPoolTrade datapool;
    private PublicData publicData;
    private ManagerServer managerServer;
    private boolean isSSL = false;
    private boolean isZlib = false;

    public NettyServer2Trade(int port) {
        this.port = port;
    }

    public NettyServer2Trade(int port, boolean isSSL, boolean isZlib) {
        this.port = port;
        this.isSSL = isSSL;
        this.isZlib = isZlib;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public void run() {
        SelfSignedCertificate ssc;
        SslContext sslCtx;
        try {
            ssc = new SelfSignedCertificate();
            sslCtx = SslContext.newServerContext(ssc.certificate(), ssc.privateKey());
            EventLoopGroup bossGroup = new NioEventLoopGroup(1);
            EventLoopGroup workerGroup = new NioEventLoopGroup();
            try {
                ServerBootstrap b = new ServerBootstrap();
                b.group(bossGroup, workerGroup)
                        .channel(NioServerSocketChannel.class)
                        .handler(new LoggingHandler(LogLevel.INFO))
                        .childHandler(new NettyServer2TradeInitializer(orderManager,datapool,publicData,managerServer, isSSL, isZlib,sslCtx));

                b.bind(port).sync().channel().closeFuture().sync();
                Logger.getLogger(NettyServer2Trade.class.getName()).log(Level.INFO, null, "测试版本-SecureChatServerInitializer1");
            } catch (InterruptedException ex) {
                Logger.getLogger(NettyServer2Trade.class.getName()).log(Level.INFO, null, ex);
            } finally {
                bossGroup.shutdownGracefully();
                workerGroup.shutdownGracefully();
            }
        } catch (CertificateException | SSLException ex) {
            Logger.getLogger(NettyServer2Trade.class.getName()).log(Level.INFO, null, ex);
        }
    }
    /*
     public void run() {
     try {// netty支持3种reactor,netty推荐主从(boss和worker)
     EventLoopGroup bossGroup = new NioEventLoopGroup(1);// acceptor,负责tcp接入,接入认证,创立socketChannel等;
     EventLoopGroup workerGroup = new NioEventLoopGroup();// netty默认设置:Runtime.getRuntime().availableProcessors()
     // 负责io的读写和编码

     try {
     ServerBootstrap b = new ServerBootstrap();
     b.group(bossGroup, workerGroup)
                        
     .option(ChannelOption.TCP_NODELAY, true)
     .option(ChannelOption.SO_KEEPALIVE, true)
                         
     .channel(NioServerSocketChannel.class)
     .handler(new LoggingHandler(LogLevel.INFO))
     .childHandler(new NettyServer2TradeInitializer(rBridgeForTNS, isSSL, isZlib));

     ChannelFuture f = b.bind(port).sync();

     // Wait until the server socket is closed.
     // In this example, this does not happen, but you can do that to
     // gracefully
     // shut down your server.
     f.channel().closeFuture().sync();
     } finally {
     workerGroup.shutdownGracefully();
     bossGroup.shutdownGracefully();
     }
     } catch (InterruptedException e) {
     }
     }
     */

    public void setOrderManager(OrderManager orderManager) {
        this.orderManager = orderManager;
    }

    public void setDatapool(DataPoolTrade datapool) {
        this.datapool = datapool;
    }

    public void setPublicData(PublicData publicData) {
        this.publicData = publicData;
    }

    public void setManagerServer(ManagerServer managerServer) {
        this.managerServer = managerServer;
    }

    public void setIsSSL(boolean isSSL) {
        this.isSSL = isSSL;
    }

    public void setIsZlib(boolean isZlib) {
        this.isZlib = isZlib;
    }
}
