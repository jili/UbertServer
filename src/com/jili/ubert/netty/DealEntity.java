/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.netty;

import io.netty.channel.ChannelHandlerContext;

/**
 *
 * @author ChengJiLi
 */
public interface DealEntity {
    public void EnterMsg(ChannelHandlerContext ctx, MsgEntity msg,MsgVehicle data);
}
