/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.netty;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import java.util.Arrays;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
/**
 *
 * @author ChengJiLi
 */

/**
 * 后台处理逻辑的核心实体类
 */
public class MsgEntity {
    private static final Log log = LogFactory.getLog(MsgEntity.class);
    private int cmdCode;// 储存命令码
    private int sessionID;// 储存请求ID
    private byte[] data;// 存放实际数据,用于protobuf解码成对应message
    private Channel channel;// 当前玩家的channel

    public int getCmdCode() {
            return cmdCode;
    }

    public void setCmdCode(int cmdCode) {
            this.cmdCode = cmdCode;
    }
        public int getSessionID() {
            return sessionID;
    }

    public void setSessionID(int sessionID) {
            this.sessionID = sessionID;
    }

    public byte[] getData() {
            return data;
    }

    public void setData(byte[] data) {
            this.data = data;
    }

    public Channel getChannel() {
            return channel;
    }

    public void setChannel(Channel channel) {
            this.channel = channel;
    }

    public ChannelFuture SendMsg(Channel channel0, int cmd, int RequstID, byte[] data) {
        ChannelFuture lastWriteFuture;
        this.cmdCode = cmd;
        this.sessionID = RequstID;
        this.data = data;

        lastWriteFuture = channel0.writeAndFlush(this);
        log.debug("Send-RequstID:" + RequstID + Arrays.toString(data) + " to channel:" + channel0);
        return lastWriteFuture;
    }

    public ChannelFuture Reply(int cmd,byte[] data){
        ChannelFuture lastWriteFuture;
        this.cmdCode = cmd;
        this.data = data;
        lastWriteFuture=channel.writeAndFlush(this);
        log.debug("Reply-RequstID:"+sessionID+Arrays.toString(data)+" to channel:"+channel);
        return lastWriteFuture;
    }
    public ChannelFuture Reply(byte[] data){
        ChannelFuture lastWriteFuture;
        this.data = data;
        lastWriteFuture=channel.writeAndFlush(this);
        log.debug("Reply-RequstID:"+sessionID+Arrays.toString(data)+" to channel:"+channel);
        return lastWriteFuture;
    }
}