/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.netty.otherserver;

import com.jili.ubert.code.ReturnIDEntity;
import com.jili.ubert.code.client2server.Login;
import com.jili.ubert.code.client2server.Logout;
import com.jili.ubert.code.client2server.RequestPageData;
import com.jili.ubert.code.client2server.SelectPageData;
import com.jili.ubert.code.server2client.MsgResult;
import com.jili.ubert.code.server2client.PageDataProto;
import com.jili.ubert.netty.MsgEntity;
import com.jili.ubert.netty.VerifyCodeGenerator;
import com.jili.ubert.netty.priceserver.PriceHanlder;
import com.jili.ubert.server.datapool.DataPoolHistory;
import com.jili.ubert.server.historyserver.HistoryServer;
import com.jili.ubert.server.publicdata.PublicData;
import com.jili.ubert.tool.IPv4Util;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import java.io.IOException;
import java.net.InetSocketAddress;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author ChengJiLi
 */
public class OtherHanlder extends SimpleChannelInboundHandler<MsgEntity> {

    private static final Log log = LogFactory.getLog(PriceHanlder.class);
    private DataPoolHistory datapool;
    private PublicData publicData;
    private HistoryServer server;

    OtherHanlder(DataPoolHistory datapool, PublicData publicData,HistoryServer server) {
        this.publicData = publicData;
        this.datapool = datapool;
        this.server=server;
    }

    private boolean isBlackList(int ip, String mac) {
        if (this.publicData.getForbidUser().containsKey(String.valueOf(ip))) {
            return true;
        }
        if (this.publicData.getForbidUser().containsKey(mac)) {
            return true;
        }
        return this.publicData.getForbidUser().containsKey(ip + mac);
    }

    private boolean isBlackList(String user) {
        return this.publicData.getForbidUser().containsKey(user);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        if (ctx.channel().isActive()) {
            ctx.channel().writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
        } else {
            ctx.close();
        }
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt)
            throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent e = (IdleStateEvent) evt;
            if (e.state() == IdleState.READER_IDLE) {
                ctx.close();
                log.info("READER_IDLE 读超时");
            } else if (e.state() == IdleState.WRITER_IDLE) {
                log.info("WRITER_IDLE 写超时");
            }
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    public void channelActive(final ChannelHandlerContext ctx) {
        log.info("新增登陆连接：" + ctx.channel());
    }
    @Override
    protected void messageReceived(ChannelHandlerContext ctx, MsgEntity msg) throws Exception {
        if (msg == null) {
            return;
        }
        log.debug("messageReceived" + ctx);
        Channel channel = ctx.channel();
        InetSocketAddress insocket = (InetSocketAddress) ctx.channel().remoteAddress();
        int ip = IPv4Util.bytesToInt(insocket.getAddress().getAddress());
        log.debug("客户IP：" + insocket.getAddress().getHostAddress() + "-" + ip);
        //    String mac = IPv4Util.getMACAddress(insocket.getAddress());
        String mac = "WE-WE-WE-WE-WE";
        //防止攻击，过滤类（现在一个算法）
        if (this.isBlackList(ip, mac)) {
            MsgResult rst = ReturnIDEntity.forbidInblacklist;
            msg.Reply( 250, rst.EnCode(rst));
            channel.close();
            return;
        }
        int csCommondCode = msg.getCmdCode();
        byte[] data = msg.getData();
        switch (csCommondCode) {
            case 0: {//启动问候，请求验证码，回复验证码
                String str = VerifyCodeGenerator.GenerateVerifyCode(4);
                log.debug("0-回应" + ip + " 验证码：" + str);
                msg.Reply( 0, str.getBytes());
                log.debug("1-回应" + ip + " 验证码：" + str);
                break;
            }
            case 1: {//登录  Login
                /*
                 1.验证密码，验证码；回复消息
                 2.维护用户登录列表
                
                 备注现在为了提升用户体验，用户在输入的时候验证有没有，验证码也在输入的时候验证正确不正确。
                 */
                //解码消息
                Login login = new Login();
                login = login.DeCode(data);
                MsgResult rst = publicData.LoginCheck(login, ip, channel);
                msg.Reply( 1, rst.EnCode(rst));
                break;
            }
            case 2: {//登出 Logout
                /*
                 1.从用户登录列表删除
                 2.释放其他资源
                 */
                Logout lonout = new Logout();
                lonout = lonout.DeCode(data);
                MsgResult rst = publicData.Logout(lonout.getUserID(), ip);
                msg.Reply( 2, rst.EnCode(rst));
                break;
            }
            case 3: {
                
            }
            case 500: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryHistoryClearingCurrencyRates(rpd);msg.Reply(500, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 500 IO decode encode fail");}}});break;}
case 501: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryHistoryIOPVAccount(rpd);msg.Reply(501, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 501 IO decode encode fail");}}});break;}
case 502: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryHistoryIOPVProduct(rpd);msg.Reply(502, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 502 IO decode encode fail");}}});break;}
case 503: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryHistoryIOPVProductUnit(rpd);msg.Reply(503, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 503 IO decode encode fail");}}});break;}
case 504: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryHistoryLogLoginInfo(rpd);msg.Reply(504, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 504 IO decode encode fail");}}});break;}
case 505: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryHistoryObjExRightInfo(rpd);msg.Reply(505, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 505 IO decode encode fail");}}});break;}
case 506: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryHistoryTradeFundTransfer(rpd);msg.Reply(506, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 506 IO decode encode fail");}}});break;}
case 507: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryHistoryTradesetCalendar(rpd);msg.Reply(507, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 507 IO decode encode fail");}}});break;}
case 508: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryRecentClearingAccountBail(rpd);msg.Reply(508, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 508 IO decode encode fail");}}});break;}
case 509: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryRecentClearingAccountFare(rpd);msg.Reply(509, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 509 IO decode encode fail");}}});break;}
case 510: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryRecentClearingDayBook(rpd);msg.Reply(510, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 510 IO decode encode fail");}}});break;}
case 511: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryRecentClearingExRight(rpd);msg.Reply(511, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 511 IO decode encode fail");}}});break;}
case 512: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryRecentClearingFundState(rpd);msg.Reply(512, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 512 IO decode encode fail");}}});break;}
case 513: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryRecentClearingPosDetail(rpd);msg.Reply(513, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 513 IO decode encode fail");}}});break;}
case 514: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryRecentClearingPrice(rpd);msg.Reply(514, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 514 IO decode encode fail");}}});break;}
case 515: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryRecentClearingProtfoliopos(rpd);msg.Reply(515, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 515 IO decode encode fail");}}});break;}
case 516: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryRecentObjETF(rpd);msg.Reply(516, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 516 IO decode encode fail");}}});break;}
case 517: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryRecentObjETFList(rpd);msg.Reply(517, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 517 IO decode encode fail");}}});break;}
case 518: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryRecentObjIndex(rpd);msg.Reply(518, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 518 IO decode encode fail");}}});break;}
case 519: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryRecentObjIndexList(rpd);msg.Reply(519, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 519 IO decode encode fail");}}});break;}
case 520: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryRecentTradeAlterOrder(rpd);msg.Reply(520, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 520 IO decode encode fail");}}});break;}
case 521: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryRecentTradeCancelOrder(rpd);msg.Reply(521, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 521 IO decode encode fail");}}});break;}
case 522: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryRecentTradeExecuteOrder(rpd);msg.Reply(522, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 522 IO decode encode fail");}}});break;}
case 523: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryRecentTradeNewOrder(rpd);msg.Reply(523, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 523 IO decode encode fail");}}});break;}
case 524: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryRecentTradeReportDetail(rpd);msg.Reply(524, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 524 IO decode encode fail");}}});break;}
case 525: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceAccountBail(rpd);msg.Reply(525, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 525 IO decode encode fail");}}});break;}
case 526: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceAccountFare(rpd);msg.Reply(526, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 526 IO decode encode fail");}}});break;}
case 527: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceAccountIcode(rpd);msg.Reply(527, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 527 IO decode encode fail");}}});break;}
case 528: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceAccountInfo(rpd);msg.Reply(528, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 528 IO decode encode fail");}}});break;}
case 529: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceAccountInvest(rpd);msg.Reply(529, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 529 IO decode encode fail");}}});break;}
case 530: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceBailTemp(rpd);msg.Reply(530, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 530 IO decode encode fail");}}});break;}
case 531: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceBailTempObj(rpd);msg.Reply(531, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 531 IO decode encode fail");}}});break;}
case 532: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceFareTemp(rpd);msg.Reply(532, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 532 IO decode encode fail");}}});break;}
case 533: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceFareTempeObj(rpd);msg.Reply(533, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 533 IO decode encode fail");}}});break;}
case 534: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceObjInfo(rpd);msg.Reply(534, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 534 IO decode encode fail");}}});break;}
case 535: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceObjProtfolio(rpd);msg.Reply(535, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 535 IO decode encode fail");}}});break;}
case 536: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceObjProtfolioList(rpd);msg.Reply(536, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 536 IO decode encode fail");}}});break;}
case 537: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceObjUnderlying(rpd);msg.Reply(537, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 537 IO decode encode fail");}}});break;}
case 538: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceProductAccount(rpd);msg.Reply(538, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 538 IO decode encode fail");}}});break;}
case 539: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceProductInfo(rpd);msg.Reply(539, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 539 IO decode encode fail");}}});break;}
case 540: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceProductUnit(rpd);msg.Reply(540, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 540 IO decode encode fail");}}});break;}
case 541: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceRiskCtrlAssets(rpd);msg.Reply(541, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 541 IO decode encode fail");}}});break;}
case 542: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceRiskSets(rpd);msg.Reply(542, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 542 IO decode encode fail");}}});break;}
case 543: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceSafeForbidLogin(rpd);msg.Reply(543, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 543 IO decode encode fail");}}});break;}
case 544: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceSafeOtherAccess(rpd);msg.Reply(544, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 544 IO decode encode fail");}}});break;}
case 545: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceTradesetExchangeLimit(rpd);msg.Reply(545, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 545 IO decode encode fail");}}});break;}
case 546: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceUserAccess(rpd);msg.Reply(546, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 546 IO decode encode fail");}}});break;}
case 547: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceUserAccount(rpd);msg.Reply(547, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 547 IO decode encode fail");}}});break;}
case 548: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceUserInfo(rpd);msg.Reply(548, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 548 IO decode encode fail");}}});break;}
case 549: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceUserPrivateInfo(rpd);msg.Reply(549, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 549 IO decode encode fail");}}});break;}
case 550: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceWorkFlowPath(rpd);msg.Reply(550, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 550 IO decode encode fail");}}});break;}
case 551: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceWorkGroup(rpd);msg.Reply(551, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 551 IO decode encode fail");}}});break;}
case 552: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceWorkGroupUser(rpd);msg.Reply(552, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 552 IO decode encode fail");}}});break;}
case 553: {publicData.WorkRun(new Runnable(){@Override public void run() {try {RequestPageData rpd = RequestPageData.DeCode(data);PageDataProto pdp = datapool.queryTraceWorkProductFlow(rpd);msg.Reply(553, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 553 IO decode encode fail");}}});break;}
case 554: {publicData.WorkRun(new Runnable(){@Override public void run() {try {SelectPageData rpd = SelectPageData.DeCode(data);PageDataProto pdp = datapool.queryString(rpd);msg.Reply(554, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 554 IO decode encode fail");}}});break;}
case 555: {publicData.WorkRun(new Runnable(){@Override public void run() {try {SelectPageData rpd = SelectPageData.DeCode(data);PageDataProto pdp = datapool.queryString(rpd);msg.Reply(555, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 555 IO decode encode fail");}}});break;}
case 556: {publicData.WorkRun(new Runnable(){@Override public void run() {try {SelectPageData rpd = SelectPageData.DeCode(data);PageDataProto pdp = datapool.queryString(rpd);msg.Reply(556, PageDataProto.EnCode(pdp));} catch (IOException ex) {try{msg.Reply(250,MsgResult.EnCode(ReturnIDEntity.failIODeCode));} catch (IOException ex0) {} log.info("CMD 556 IO decode encode fail");}}});break;}

            default:{
                
            }
        }
    }
    
}
/*
*/