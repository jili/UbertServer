/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.netty.otherserver;

import com.jili.ubert.netty.traderserver.NettyServer2Trade;
import com.jili.ubert.server.datapool.DataPoolHistory;
import com.jili.ubert.server.historyserver.HistoryServer;
import com.jili.ubert.server.publicdata.PublicData;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.util.SelfSignedCertificate;
import java.security.cert.CertificateException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLException;

/**
 *
 * @author ChengJiLi
 */
public class NettyServer2Other  implements Runnable {
    
    private int port;
    private DataPoolHistory datapool;
    private PublicData publicData;
    private HistoryServer server;
    private boolean isSSL = false;
    private boolean isZlib = false;

    public NettyServer2Other(int port) {
        this.port = port;
    }

    public NettyServer2Other(int port, boolean isSSL, boolean isZlib) {
        this.port = port;
        this.isSSL = isSSL;
        this.isZlib = isZlib;
    }
    @Override
    public void run() {
        SelfSignedCertificate ssc;
        SslContext sslCtx;
        try {
            ssc = new SelfSignedCertificate();
            sslCtx = SslContext.newServerContext(ssc.certificate(), ssc.privateKey());
            EventLoopGroup bossGroup = new NioEventLoopGroup(1);
            EventLoopGroup workerGroup = new NioEventLoopGroup();
            try {
                ServerBootstrap b = new ServerBootstrap();
                b.group(bossGroup, workerGroup)
                        .channel(NioServerSocketChannel.class)
                        .handler(new LoggingHandler(LogLevel.INFO))
                        .childHandler(new NettyServer2OtherInitializer(datapool,publicData,server, isSSL, isZlib,sslCtx));

                b.bind(port).sync().channel().closeFuture().sync();
                Logger.getLogger(NettyServer2Trade.class.getName()).log(Level.INFO, null, "测试版本-SecureChatServerInitializer1");
            } catch (InterruptedException ex) {
                Logger.getLogger(NettyServer2Trade.class.getName()).log(Level.INFO, null, ex);
            } finally {
                bossGroup.shutdownGracefully();
                workerGroup.shutdownGracefully();
            }
        } catch (CertificateException | SSLException ex) {
            Logger.getLogger(NettyServer2Trade.class.getName()).log(Level.INFO, null, ex);
        }
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setDatapool(DataPoolHistory datapool) {
        this.datapool = datapool;
    }

    public void setPublicData(PublicData publicData) {
        this.publicData = publicData;
    }

    public void setServer(HistoryServer server) {
        this.server = server;
    }
    
}
