/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.netty;

import io.netty.channel.ChannelHandlerContext;
import java.io.IOException;

/**
 *
 * @author ChengJiLi
 */
public class DealVehicleMsg {
    public void Deal(ChannelHandlerContext ctx, MsgEntity msg,byte[] data) throws ClassNotFoundException, InstantiationException, IOException, IllegalAccessException{
        short cmd = 22;
        try {
            MsgVehicle msgvia = new MsgVehicle();
            msgvia = msgvia.DeCode(data);
            DealEntity dealentity = (DealEntity) Class.forName(msgvia.donename).newInstance();
            dealentity.EnterMsg(ctx, msg, msgvia);
            msg.Reply(cmd, data);
        }
        catch (ClassNotFoundException e){
            msg.Reply(cmd, data);
        }
        catch (InstantiationException e){
            msg.Reply(cmd, data);
        }
        catch (IOException e){
            msg.Reply(cmd, data);
        }
        /*
        finally{
            cmd = null;
        }
        */
    }
}
