/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.netty;

import com.baidu.bjf.remoting.protobuf.Codec;
import com.baidu.bjf.remoting.protobuf.FieldType;
import com.baidu.bjf.remoting.protobuf.ProtobufProxy;
import com.baidu.bjf.remoting.protobuf.annotation.Protobuf;
import io.netty.channel.Channel;
import java.io.IOException;

/**
 *
 * @author ChengJiLi
 * @param <T>
 */
public class MsgVehicle<T> {
    @Protobuf(fieldType = FieldType.STRING, order=1, required = false)
    public String donename;
    @Protobuf(fieldType = FieldType.OBJECT, order=1, required = false)
    public T data;
    public MsgVehicle(){}
    public MsgVehicle(String donename,T data){
        this.donename = donename;
        this.data = data;
    }
    public void setData(T data){
        this.data = data;
    }
    public void setDoneName(String name){
        this.donename = name;
    }
    public String getDoneName(){
        return donename;
    }
    public T getData(){
        return data;
    }
    public int SendToServer(Channel sendchannel) throws IOException{
        MsgEntity msg = new MsgEntity();
        msg.SendMsg(sendchannel, (short)22, 1, EnCode(this));
        return 10;
    } 
    public int SendToClient(Channel sendchannel) throws IOException{
        MsgEntity msg = new MsgEntity();
        msg.SendMsg(sendchannel, (short)22, 1, EnCode(this));
        return 10;
    } 
    public byte[] EnCode(MsgVehicle msg) throws IOException{
        Codec<MsgVehicle> Lcode = ProtobufProxy.create(MsgVehicle.class);
        return Lcode.encode(msg);
    }
    public MsgVehicle DeCode(byte[] data) throws IOException{
        Codec<MsgVehicle> Lcode = ProtobufProxy.create(MsgVehicle.class);
        return Lcode.decode(data);
        
    }
}
