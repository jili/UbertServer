/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jili.ubert.code;

import com.jili.ubert.code.server2client.MsgResult;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
//import static org.junit.Assert.*;

/**
 *
 * @author ChengJili
 */
public class NewJUnitTestCode {
    
    public NewJUnitTestCode() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void  codeMsgResult() throws IOException {
        MsgResult data = new MsgResult();
        data.setReturnID(1);
        data.setSuccess(true);
        data.setWord("I am ChengJili");
        byte[] a = data.EnCode(data);
        MsgResult data0 = data.DeCode(a);
        Assert.assertEquals(data.getWord(), data0.getWord());
        System.out.println(data0.getWord());
    }
    @Test
    public void  codeMsgXX() throws IOException {
        MsgResult data = new MsgResult();
    }
}
